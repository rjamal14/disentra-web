/* eslint-disable react/prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import { Card, CardContent, Typography, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { Page } from 'components';
import { Form } from './components';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  results: {
    marginTop: theme.spacing(3)
  },
  breadcrumb:{
    margin: theme.spacing(3),
    marginBottom: -theme.spacing(6),
  },
  title: {
    fontSize: '18px',
    fontStyle: 'normal',
    fontWeight: 600,
    lineHeight: '22px',
    letterSpacing: '0em',
    textAlign: 'left',
    color: '#000000',
    marginBottom: theme.spacing(3),
  }
}))

const Profile = ({ 
  title,
  dataName,
  FormInput,
  desc
}) => {
  const classes = useStyles();
  
  return (
    <Page
      className={classes.root}
      title={title}
    >
      <Card>
        
        <div>
          <CardContent>
            <div className={classes.root}>
              <Grid
                container
                spacing={3}
              >
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <Typography className={classes.title}>{dataName}</Typography>
                  <Typography
                    style={{marginTop:'5px', marginBottom: '25px'}}
                    variant="h6"
                  >
                    {desc && desc}
                  </Typography>
                  <Form FormInput={FormInput} />
                </Grid>
              </Grid>
            </div>
          </CardContent>
        </div>
      </Card>
    </Page>
  );
};

Profile.propTypes = {
  addApi: PropTypes.func,
  dataName: PropTypes.string,
  deleteApi: PropTypes.func,
  editApi: PropTypes.func,
  getApi: PropTypes.func,
  path: PropTypes.array,
  refreshState: PropTypes.func,
  stateName: PropTypes.string,
  tableColumn: PropTypes.array,
  title: PropTypes.string
};

export default Profile;