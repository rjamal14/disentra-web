import React from 'react'

import Profile from '../../Profile'
import FormInput from './FormInput'

const Setting = () => {
  return (
    <Profile
      dataName="Ubah Profil"
      FormInput={FormInput}
      stateName="village"
      title="Ubah Profil"
    />
  )
}

export default Setting