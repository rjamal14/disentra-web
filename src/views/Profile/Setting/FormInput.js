/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react'
import { useForm } from 'react-hook-form';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { 
  FormControl,
  Typography, 
  useMediaQuery,
  Button,
  CircularProgress,
  Divider,
} from '@material-ui/core';
import { useTheme, withStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import { editUserLoginDetail, getRoles, getJobPositions } from 'actions';
import validation from '../validation';
import useStyles from '../form-jss';
import palette from 'theme/palette';
import { RoundedInput, RoundedSelect } from 'components';
import { apiWithToken } from '../../../config/apiWithToken';

function FormInput() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { loading } = useSelector(state => state.user);
  const { data: listRoles } = useSelector(state => state.role);
  const { data: listJobPositions } = useSelector(state => state.jobPosition);
  const theme = useTheme();
  const { register, handleSubmit, control, errors } = useForm();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const [fileHandler, setFileHandler] = useState(null);
  const [data, setData] = useState({});

  const PhotoErrorText = withStyles(() => ({
    root: {
      color: palette.error.main,
      marginBottom: 10,
      marginLeft: 10,
      marginTop: 10
    }
  }))(Typography);

  useEffect(() => {
    dispatch(getRoles());
    dispatch(getJobPositions());
    apiWithToken.get('/user')
      .then(res => {
        const { status, data: { data } } = res;
        if (status === 200) {
          setData(data)
        }
      })
  }, []);

  const onSubmit = (data) => {
    dispatch(editUserLoginDetail({ 
      payload: data
    }));
  };

  const setPhoto = (event) => {
    setFileHandler(event.target.files);
  };
  
  return(
    <div className={clsx(classes.root, !fullScreen && classes.desktop)}>
      {
        (data?.name) && (
          <>
            <div className={classes.uploadContainer}>
              <Typography
                component="p"
                variant="body1"
              >
              Foto User
              </Typography>
              <div className={clsx(classes.uploadArea, fullScreen && classes.fullWidthRel)}>
                <div className={classes.uploadInfo}>
                  {
                    fileHandler &&
                  <img
                    alt="user"
                    className={classes.userImage}
                    src={URL.createObjectURL(fileHandler[0])}
                  />
                  }
                  <img 
                    alt="photo-profile"
                    className={classes.userImage}
                    src={data.photo?.url || '/svg/profile.svg'}
                  />
                  <Typography
                    component="p"
                    variant="caption"
                  >
                  Tambah Foto (Maks. ukuran 2MB)
                  </Typography>
                </div>
                <input 
                  accept="image/*" 
                  className={classes.file}
                  defaultValue={''} 
                  name="photo"
                  onChange={setPhoto}
                  ref={register()}
                  type="file"
                />
              </div>
              {
                errors.photo &&
              <PhotoErrorText
                classes={classes.photoErrorText}
                component="p"
                variant="caption"
              >
                {errors.photo.message}
              </PhotoErrorText>
              }
            </div>
            <Divider
              flexItem
              orientation="vertical"
            />
            <div className={clsx(classes.formContent, fullScreen && classes.fullWidth)}>
              <FormControl fullWidth>
                <RoundedInput 
                  defaultValue={data?.name}
                  errorMessage={errors?.name?.message}
                  errors={errors.name}
                  label="Nama"
                  name="name"
                  register={register(validation.name)}
                />
              </FormControl>
              <FormControl fullWidth>
                <RoundedSelect
                  control={control}
                  defaultValue={data?.role}
                  disabled
                  errorMessage={errors?.role_ids?.message}
                  errors={errors.role_ids}
                  label="Role"
                  name="role_ids"
                  options={listRoles}
                  validation={validation.role_ids}
                />
              </FormControl>
              <FormControl fullWidth>
                <RoundedInput
                  defaultValue={data?.nip}
                  disabled
                  label="NIP"
                  name="nip"
                  placeholder="Contoh: 18111210"
                  type="number"
                />
              </FormControl>
              <FormControl fullWidth>
                <RoundedSelect
                  control={control}
                  defaultValue={listJobPositions[0]?.id || data?.job}
                  disabled
                  errorMessage={errors?.job_position_id?.message}
                  errors={errors.job_position_id}
                  label="Jabatan"
                  name="job_position_id"
                  options={listJobPositions}
                  validation={validation.job_position_id}
                />
              </FormControl>
              <FormControl fullWidth>
                <RoundedInput
                  defaultValue={data?.email}
                  disabled
                  errorMessage="Format email tidak valid"
                  errors={errors.email}
                  label="Email"
                  name="email"
                  register={register(validation.email)}
                  type="email"
                />
              </FormControl>
              <FormControl fullWidth>
                <RoundedInput
                  defaultValue={data?.phone_number}
                  errorMessage={
                  errors.phone_number?.type === 'pattern'
                    ? 'Format nomor handphone salah'
                    : 'Nomor telepon hanya terdiri dari 10 sampai 14 nomor'
                  }
                  errors={errors.phone_number}
                  label="Nomor Handphone"
                  name="phone_number"
                  placeholder="Contoh: 081xxxxxxx"
                  register={register(validation.phone)}
                  type="number"
                />
              </FormControl>
              <div className={classes.buttonAction}>
                <Button 
                  classes={{
                    root: classes.buttonRounded
                  }}
                  color="secondary"
                  disabled={loading}
                  endIcon={
                    loading ? (
                      <CircularProgress
                        color="secondary"
                        size={20} 
                      /> 
                    ) : null
                  }
                  onClick={handleSubmit(onSubmit)}
                  variant="contained"
                >
                Simpan
                </Button>
              </div>
            </div>
          </>
        )
      }
    </div>
  )
}

FormInput.propTypes = {
  toggleModal: PropTypes.func
};

export default FormInput;