/* eslint-disable react/prop-types */
import React, { useRef, useEffect} from 'react'
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { 
  useMediaQuery,
} from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';
import useStyles from './form-jss';

function Form(props) {
  const { FormInput } = props;
  const classes = useStyles();
  const { success, error, loading } = useSelector(state => state.user);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const initialization = useRef(true);

  useEffect(() => {
    if (initialization.current) {
      initialization.current = false;
    }
  }, [success, error, loading]);
  
  return(
    <div className={clsx(classes.root, !fullScreen && classes.desktop)}>
      <div className={clsx(classes.formContent, fullScreen && classes.fullWidth)}>
        <FormInput />
      </div>
    </div>
  )
}

Form.propTypes = {
  toggleModal: PropTypes.func
};

export default Form;