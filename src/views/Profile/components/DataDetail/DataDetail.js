/* eslint-disable react/prop-types */
import React, { useState, useEffect ,useRef } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import { useForm } from 'react-hook-form';
import { 
  useMediaQuery,
  Button,
  CircularProgress,
} from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import useStyles from './data-detail-jss';
import { refreshStateUser } from 'actions';
import FieldDisplay from '../FieldDisplay';

function DataDetail(props) {
  const { 
    isEdit, 
    setIsEdit, 
    hideDetailModal, 
    showDeleteConfirmation,
    editApi,
    FormInput,
  } = props;
  const classes = useStyles();
  const theme = useTheme();
  const dispatch = useDispatch();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const { data: dataUser, dataDetail, success, loading } = useSelector(state => state.user);
  const { register, handleSubmit, control, errors, setValue } = useForm();
  const initialization = useRef(true);

  useEffect(() => {
    if (initialization.current) {
      initialization.current = false;
    }
  }, [success, loading]);

  const closeModal = () => {
    hideDetailModal();
  };

  const switchToEdit = () => {
    setIsEdit(true);
  };

  const onSubmit = (data) => {
    dispatch(editApi({ 
      payload: data,
      targetId: dataDetail.id
    }));
  };

  const showConfirmation = () => {
    const index = dataUser.map(user => user.id).indexOf(dataDetail.id);
    showDeleteConfirmation(index);
  };

  return(
    <div className={clsx(classes.root, !fullScreen && classes.desktop)}>
      <div className={clsx(classes.formContent, fullScreen && classes.fullWidth)}>
        {
          isEdit ? (
            
            <FormInput 
              control={control} 
              dataDetail={dataDetail}
              errors={errors}
              register={register}
              setValue={setValue}
            />
          ) : (
            <FieldDisplay 
              data={dataDetail?.name} 
              label="Nama"
            />
          )
        }
        {
          isEdit ? (
            <div className={classes.buttonAction}>
              <Button
                classes={{
                  root: clsx(classes.buttonRounded, classes.expandButton)
                }}
                color="secondary"
                onClick={closeModal}
                variant="outlined"
              >
                Batal
              </Button>
              <Button 
                classes={{
                  root: classes.buttonRounded
                }}
                color="secondary"
                disabled={loading}
                endIcon={
                  loading ? (
                    <CircularProgress
                      color="secondary"
                      size={20} 
                    /> 
                  ) : null
                }
                onClick={handleSubmit(onSubmit)}
                variant="contained"
              >
                Simpan
              </Button>
            </div>
          ) : (
            <div className={classes.buttonAction}>
              <Button
                classes={{
                  root: clsx(classes.buttonRounded, classes.expandButton)
                }}
                onClick={showConfirmation}
              >
                Hapus
              </Button>
              <Button 
                classes={{
                  root: classes.buttonRounded
                }}
                color="secondary"
                onClick={switchToEdit}
                variant="contained"
              >
                Ubah User
              </Button>
            </div>
          )
        }
      </div>
    </div>
  )
}

DataDetail.propTypes = {
  hideDetailModal: PropTypes.func,
  isEdit: PropTypes.bool,
  setIsEdit: PropTypes.func,
  showChangeStatusConfirmation: PropTypes.func,
  showDeleteConfirmation: PropTypes.func,
};

export default DataDetail;