import React from 'react'

import Profile from '../../Profile'
import FormInput from './FormInput'

const ChangePassword = () => {
  return (
    <Profile
      dataName="Ubah Kata Sandi"
      desc="Buat kata sandi baru minimal 8 karakter yang terdiri dari kombinasi huruf, angka, dan simbol"
      FormInput={FormInput}
      stateName="village"
      title="Ubah Kata Sandi"
    />
  )
}

export default ChangePassword