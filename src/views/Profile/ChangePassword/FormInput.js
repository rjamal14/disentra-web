import React, { useEffect } from 'react'
import { useForm } from 'react-hook-form';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { 
  FormControl,
  useMediaQuery,
  Button,
  CircularProgress
} from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import { updateUserPassword, logout } from 'actions';
import useStyles from '../form-jss';
import { RoundedInput } from 'components';
import useRouter from 'utils/useRouter';

function FormInput() {
  const { history } = useRouter();
  const classes = useStyles();
  const dispatch = useDispatch();
  const { loading, success } = useSelector(state => state.user);
  const theme = useTheme();
  const { register, handleSubmit, errors, getValues } = useForm();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const password = getValues('password');

  useEffect(() => {
    if (success) {
      history.push('/auth/login');
      localStorage.clear();
      dispatch(logout());
    }
  }, [success])

  const onSubmit = (data) => {
    dispatch(updateUserPassword({ 
      payload: data
    }));
  };

  const validate = (value, confirm) => {
    const rgx = new RegExp('^(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-zA-Z])[a-zA-Z0-9!@#$%^&*]{8,}$');
    if (!value) return 'Tidak boleh kosong';
    else if (!(rgx.test(value))) return 'Minimal 8 karakter yang terdiri dari kombinasi huruf, angka, dan simbol';
    else if (confirm && value !== password) return 'Kata sandi konfirmasi tidak sama dengan yang baru'; 
    else return true;
  }

  return(
    <div className={clsx(classes.root, !fullScreen && classes.desktop)}>
      <div className={clsx(classes.formContent, fullScreen && classes.fullWidth)}>
        <div>
          <FormControl>
            <RoundedInput
              errorMessage={errors?.current_password?.message}
              errors={errors.current_password}
              label="Kata Sandi Lama"
              name="current_password"
              register={register({
                validate: {
                  required: value => { 
                    if (!value) return 'Tidak boleh kosong';
                    else return true;
                  },
                },
              })}
              type="password"
            />
          </FormControl>
        </div>
        <div>
          <FormControl>
            <RoundedInput
              errorMessage={errors?.password?.message}
              errors={errors.password}
              label="Kata Sandi Baru"
              name="password"
              register={register({
                validate: {
                  required: value => validate(value, ''),
                },
              })}
              type="password"
            />
          </FormControl>
        </div>
        <div>
          <FormControl>
            <RoundedInput
              errorMessage={errors?.password_confirmation?.message}
              errors={errors.password_confirmation}
              label="Konfirmasi Kata Sandi Baru"
              name="password_confirmation"
              register={register({
                validate: {
                  required: value => validate(value, 'confirm'),
                },
              })}
              type="password"
            />
          </FormControl>
        </div>
        <div className={classes.buttonAction}>
          <Button 
            classes={{
              root: classes.buttonRounded
            }}
            color="secondary"
            disabled={loading}
            endIcon={
              loading ? (
                <CircularProgress
                  color="secondary"
                  size={20} 
                /> 
              ) : null
            }
            onClick={handleSubmit(onSubmit)}
            variant="contained"
          >
            Simpan
          </Button>
        </div>
      </div>
    </div>
  )
}

FormInput.propTypes = {
  toggleModal: PropTypes.func
};

export default FormInput;