/* eslint-disable react/prop-types */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
import React, { useState } from 'react';
import CardContent from '@material-ui/core/CardContent';
import Card from '@material-ui/core/Card';
import useStyles from './reset-jss.js';
import ResetForm from './ResetForm';
import Updated from './Updated';
import baseUrl from '../../services/baseUrl';
import env from '../../config/env';
import { AlertMessage } from 'components';

const uri = '/api/v1/users/password';

const Reset = (props) => {
  const classes = useStyles();
  const initialValues = { password: '', confirmation_password: '' };
  const [isSuccess, setIsSuccess] = useState(true);
  const [error, setError] = useState({ message: '', type: 'error' });
  const [isError, setIsError] = useState(false);

  const handleSubmit = async (value) => {
    if(value.password !== value.confirmation_password){
      setIsError(true);
      setError({ message: 'konfirmasi kata sandi tidak sama ! ', type: 'error' });
    }else{
      try {
        const str = props.location.search;
        let form = { 
          reset_password_token: str.split('=')[1].split('&')[0],
          scope: str.split('&scope=')[1] || 'admin',
          user: {
            password: value.password,
            confirmation_password: value.confirmation_password,
          },
        }; 
        const { status, data } = await baseUrl.put(`${env.baseUrl + uri}`, form);
        if (status === 200) {
          if(data.data.success === 'reset_password_success'){
            setIsSuccess(false);
          }else if (data.data.error === 'unknown_token'){
            setIsError(true);
            setError({ message: 'Sesi sudah berakhir, Lakukan kembali lupa kata sandi untuk mendapatkan link baru', type: 'error' });
          }else{
            setIsError(true);
            setError({ message: 'Kata Sandi harus terdiri dari huruf, angka dan minimal 6 karakter !', type: 'error' });
          }
        }
      } catch (err) {
        setIsError(true);
        if (error.response.status === 401){
          setError({ message: 'Sesi sudah berakhir, Lakukan kembali lupa kata sandi untuk mendapatkan link baru', type: 'error' });
        }else{
          setError({ message: 'Kesalahan tidak diketahui', type: 'error' });
        }
      }
    }
  }
  const triggerSetError = (value) => {
    setIsError(value);
  };
  return (
    <div className={classes.mainContainer}>
      <Card className={classes.card}>
        <CardContent className={classes.cardContent}>
          <img
            alt="logo"
            className={classes.img}
            src={'/icon/logo.png'}
          />
          <AlertMessage
            message={error.message}
            open={isError}
            severity={error.type}
            triggerSetError={triggerSetError}
          />
          { 
            isSuccess ?
              <ResetForm
                initialValues={initialValues}
                onSubmit={handleSubmit}
              />
              : 
              <Updated />
          }
        </CardContent>
      </Card>
    </div>
  );
};
export default Reset;
