import React  from 'react';
import PropTypes from 'prop-types';
import { Typography, Link } from '@material-ui/core';
import useStyles from './reset-jss';

const Updated = () => {
  const classes = useStyles();
  return (
    <div className={classes.container}>
      <img
        alt="logo"
        className={classes.img2}
        src={'/icon/updated-email.png'}
      />
      <Typography
        className={classes.textTitle}
        component="h1"
        variant="h5"
      >
        Kata Sandi Berhasil Diubah!
      </Typography>
      <Typography
        className={classes.textDesc}
        component="h6"
        variant="h6"
      >
          Silahkan masuk kembali dengan menggunakan kata sandi baru Anda
      </Typography>
      <div className={classes.buttonContainer2}>
        <div className={classes.rememberMe} />
          Punya akun? 
        <Link
          className={classes.linksML}
          color="secondary"
          href="/"
        >
          Masuk
        </Link>
      </div>
    </div>
  );
};

Updated.propTypes = ({
  initialValues: PropTypes.object.isRequired,
  onSubmit: PropTypes.func.isRequired
});

export default Updated;
