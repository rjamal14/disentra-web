import React from 'react';
import PropTypes from 'prop-types';
import { Formik, Form, Field } from 'formik';
import { Button, CircularProgress, Typography } from '@material-ui/core';
import { TextFieldPassword,TextFieldConfirmationPassword } from 'components';
import useStyles from './reset-jss';

const ResetForm = ({ initialValues, onSubmit }) => {
  const classes = useStyles();
  
  const validatePassword = (value) => {
    let error;
    if (!value) {
      error = 'Kata Sandi tidak boleh kosong !';
    }

    return error;
  };

  const validateConfirmationPassword = (value) => {
    let error;
    if (!value) {
      error = 'Konfirmasi Kata Sandi tidak boleh kosong !';
    }
    return error;
  };

  return (
    <Formik
      component={({ submitForm, isSubmitting, errors, touched }) => (
        <Form
          autoComplete="off"
          className={classes.container}
        >
          <Typography
            className={classes.textTitle}
            variant="h3"
          >
              Ubah Kata Sandi
          </Typography>
          <Typography
            className={classes.textDesc}
            component="h6"
            variant="h6"
          >
            Buat kata sandi baru minimal 8 karakter yang terdiri dari kombinasi huruf, angka, dan simbol
          </Typography>
          <div className={classes.form}>
            <Field
              autoComplete="off"
              component={TextFieldPassword}
              fullWidth
              helperText={touched.password && errors.password}
              margin="normal"
              name="password"
              placeholder="Kata Sandi"
              validate={validatePassword}
              variant="outlined"
            />
            <Field
              autoComplete="off"
              component={TextFieldConfirmationPassword}
              fullWidth
              helperText={touched.confirmation_password && errors.confirmation_password}
              margin="normal"
              name="confirmation_password"
              placeholder="Konfirmasi Kata Sandi"
              validate={validateConfirmationPassword}
              variant="outlined"
            />
            <div className={classes.buttonContainer}>
              <Button
                className={classes.btn}
                color="primary"
                disabled={isSubmitting}
                fullWidth
                onClick={submitForm}
                type="submit"
                variant="contained"
              >
                {
                  isSubmitting ? 
                    <CircularProgress
                      color="secondary"
                      size={24}
                    /> : 'Kirim'
                }
              </Button>
            </div>
          </div>
        </Form>
      )}
      initialValues={initialValues}
      onSubmit={async (value) => {
        await onSubmit(value);
      }}
    />
  );
};

ResetForm.propTypes = ({
  initialValues: PropTypes.object.isRequired,
  onSubmit: PropTypes.func.isRequired
});

export default ResetForm;
