/* eslint-disable react/prop-types */
import React from 'react'
import { RoundedInput } from 'components';
import validation from '../validation';

const FormInput = (props) => {
  const { dataDetail, register, errors } = props;

  return (
    <>
      <RoundedInput 
        defaultValue={dataDetail?.name || ''}
        errorMessage={errors?.name?.message}
        errors={errors.name}
        label="Nama Provinsi"
        name="name"
        register={register(validation.name)}
      />
    </>
  )
}

export default FormInput;