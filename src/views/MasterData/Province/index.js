import React from 'react';
import MasterData from '../MasterData';
import { getProvince, deleteProvince, addProvince, editProvince, refreshState } from 'actions';
import FormInput from './FormInput';

const path = [
  ['Master Data', '#!'],
  ['Provinsi', '/master-data/province'],
];

const tableColumn = [
  {
    name: 'name',
    label: 'Nama Provinsi',
  },
  {
    name: '',
    label: 'Aksi',
  }
];

const Province = () => {
  return (
    <MasterData
      addApi={addProvince}
      dataName="Provinsi"
      deleteApi={deleteProvince}
      editApi={editProvince}
      FormInput={FormInput}
      getApi={getProvince}
      path={path}
      refreshState={refreshState}
      stateName="province"
      tableColumn={tableColumn}
      title="Provinsi"
    />
  );
};

export default Province;