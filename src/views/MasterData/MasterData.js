/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import swal from 'sweetalert';
import { Card } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { Page, Breadcrumb, Modal } from 'components';
import { Header, Results, DataDetail, Form } from './components';
import { getAllProvince, getAllCity, getAllDistrict } from 'actions';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  results: {
    marginTop: theme.spacing(3)
  },
  breadcrumb:{
    margin: theme.spacing(3),
    marginBottom: -theme.spacing(6),
  }
}));

const MasterData = ({ 
  title,
  dataName,
  path,
  getApi,
  deleteApi,
  tableColumn,
  stateName,
  addApi,
  editApi,
  refreshState,
  FormInput,
}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [showModal, setShowModal] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [showDataDetail, setShowDataDetail] = useState(false);
  const [dataPerPage, setDataPerPage] = useState(10);
  const [searchVal, setSearch] = useState('');
  const { data, pagination } = useSelector(state => state[stateName]);

  useEffect(() => {
    dispatch(getAllProvince());
    dispatch(getAllCity());
    dispatch(getAllDistrict());
  }, [])

  useEffect(() => {
    dispatch(getApi({
      payload: {
        dataPerPage,
        search: searchVal
      }
    }));
  }, [dataPerPage]);

  useEffect(() => {
    dispatch(getApi({
      payload: {
        dataPerPage,
        search: searchVal
      }
    }));
  }, [searchVal]);

  function resultSearch(search){
    setSearch(search)    
  }

  const showDeleteConfirmation = async (index) => {
    const confirmation = await swal({
      title: `Hapus ${dataName}`,
      text: 'Setelah dihapus, Anda tidak akan dapat memulihkan data ini!',
      icon: 'warning',
      buttons: true,
      dangerMode: true,
    })
    if (confirmation) {
      deleteExecution(index);
    }
  };

  const deleteExecution = (index) => {
    dispatch(deleteApi({
      payload: {
        id: data[index].id
      }
    }))
  };

  const hideDetailModal = () => {
    setShowDataDetail(false);
    setIsEdit(false);
  };

  const onChangePage = (direction) => {
    let currentPage = pagination?.current_page ?? 1;
    const totalPages = pagination?.total_pages ?? 1;
    if (direction === 'next') {
      if (currentPage < totalPages) {
        dispatch(getApi({
          payload: {
            dataPerPage,
            page: pagination?.current_page + 1 ?? 1,
            search: searchVal
          }
        }))
      }
    } else {
      if (currentPage > 1) {
        dispatch(getApi({
          payload: {
            dataPerPage,
            page: pagination?.current_page - 1 ?? 1,
            search: searchVal
          }
        }))
      }
    }
  }

  return (
    <Page
      className={classes.root}
      title={title}
    >
      <Card>
        <div className={classes.breadcrumb}>
          <Breadcrumb route={path} />
        </div>
        <Header 
          dataName={dataName}
          dataPerPage={dataPerPage}
          resultSearch={resultSearch} 
          rowsPerPage={dataPerPage}
          setDataPerPage={setDataPerPage}
          setRowsPerPage={(val)=> setDataPerPage(val)}
          setShowModal={setShowModal}
          showModal={showModal}
        />
        <Results
          className={classes.results}
          onChangePage={onChangePage}
          setIsEdit={setIsEdit}
          setShowDataDetail={setShowDataDetail}
          showDeleteConfirmation={showDeleteConfirmation}
          stateName={stateName}
          tableColumn={tableColumn}
        />
      </Card>
      <Modal
        children={
          <Form
            addApi={addApi}
            FormInput={FormInput}
            toggleModal={setShowModal}
          />
        }
        isOpen={showModal}
        title={`Tambah ${dataName}`}
        toggleModal={setShowModal}
      />
      <Modal
        children={
          <DataDetail 
            dataName={dataName}
            editApi={editApi}
            FormInput={FormInput}
            hideDetailModal={hideDetailModal}
            isEdit={isEdit}
            refreshState={refreshState}
            setIsEdit={setIsEdit}
            showDeleteConfirmation={showDeleteConfirmation}
            stateName={stateName}
            tableColumn={tableColumn}
          />
        }
        hideDetailModal={hideDetailModal}
        isOpen={showDataDetail}
        title={`Ubah ${dataName}`}
        toggleModal={setShowDataDetail}
      />
    </Page>
  );
};

MasterData.propTypes = {
  addApi: PropTypes.func,
  dataName: PropTypes.string,
  deleteApi: PropTypes.func,
  editApi: PropTypes.func,
  getApi: PropTypes.func,
  path: PropTypes.array,
  refreshState: PropTypes.func,
  stateName: PropTypes.string,
  tableColumn: PropTypes.array,
  title: PropTypes.string
};

export default MasterData;