import React from 'react'

// import Form from './Form'
import MasterData from '../MasterData'
import { getBusinessField, deleteBusinessField, addBusinessField, editBusinessField, refreshState } from 'actions'
import FormInput from './FormInput'

const path = [
  ['Master Data', '/master-data'],
  ['Bidang Usaha', '/master-data/business-field'],
];

const tableColumn = [
  {
    name: 'name',
    label: 'Nama Bidang Usaha',
  },
  {
    name: '',
    label: 'Aksi',
  }
]

const BusinessField = () => {
  return (
    <MasterData
      addApi={addBusinessField}
      dataName="Bidang Usaha"
      deleteApi={deleteBusinessField}
      FormInput={FormInput}
      editApi={editBusinessField}
      getApi={getBusinessField}
      path={path}
      refreshState={refreshState}
      stateName="businessField"
      tableColumn={tableColumn}
      title="Bidang Usaha"
    />
  )
}

export default BusinessField