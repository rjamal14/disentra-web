/* eslint-disable react/prop-types */
import React, { useRef, useEffect} from 'react'
import { useForm } from 'react-hook-form';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { 
  useMediaQuery,
  Button,
  CircularProgress,
} from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import { refreshStateUser } from 'actions';
import useStyles from './form-jss';

function Form(props) {
  const { toggleModal, addApi, FormInput } = props;
  const classes = useStyles();
  const dispatch = useDispatch();
  const { success, error, loading } = useSelector(state => state.user);
  const theme = useTheme();
  const { register, handleSubmit, control, errors, setValue } = useForm();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const initialization = useRef(true);

  useEffect(() => {
    dispatch(refreshStateUser());
  }, []);

  const onSubmit = (data) => {
    dispatch(addApi({ 
      payload: data 
    }));
  };

  const closeModal = () => {
    toggleModal(false);
  };

  useEffect(() => {
    if (initialization.current) {
      initialization.current = false;
    } else {
      if (success && !loading) {
        toggleModal(false);
        dispatch(refreshStateUser());
      }
      if (error) {
        dispatch(refreshStateUser());
      }
    }
  }, [success, error, loading]);
  
  return(
    <div className={clsx(classes.root, !fullScreen && classes.desktop)}>
      <div className={clsx(classes.formContent, fullScreen && classes.fullWidth)}>
        <FormInput
          control={control}
          errors={errors}
          register={register}
          setValue={setValue}
        />
        <div className={classes.buttonAction}>
          <Button
            classes={{
              root: clsx(classes.buttonRounded, classes.expandButton)
            }}
            color="secondary"
            onClick={closeModal}
            variant="outlined"
          >
            Batal
          </Button>
          <Button 
            classes={{
              root: classes.buttonRounded
            }}
            color="secondary"
            disabled={loading}
            endIcon={
              loading ? (
                <CircularProgress
                  color="secondary"
                  size={20} 
                /> 
              ) : null
            }
            onClick={handleSubmit(onSubmit)}
            variant="contained"
          >
            Simpan
          </Button>
        </div>
      </div>
    </div>
  )
}

Form.propTypes = {
  toggleModal: PropTypes.func
};

export default Form;