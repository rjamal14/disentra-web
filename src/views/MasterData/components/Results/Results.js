import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PerfectScrollbar from 'react-perfect-scrollbar';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { setUserDataDetail } from 'actions';
import useStyles from './result-jss';
import { LinearProgress } from '@material-ui/core';
import {
  Card,
  CardActions,
  CardContent,
  Divider,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
  Button,
} from '@material-ui/core';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { MoreButton } from '../index';

const CardFooter = withStyles(() => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    padding: '30px 0',
  }
}))(CardActions);

const Results = props => {
  const { 
    className, 
    onChangePage,
    setShowDataDetail, 
    setIsEdit, 
    showDeleteConfirmation,
    tableColumn,
    stateName
  } = props;

  const classes = useStyles();
  const dispatch = useDispatch();
  const { data, pagination } = useSelector(state => state[stateName]);

  const showDataDetailModal = (index, noEdit) => {
    if (!noEdit) {
      setIsEdit(true);
    }
    dispatch(setUserDataDetail({
      payload: data[index]
    }))
    setShowDataDetail(true);
  }

  return (
    <div className={clsx(classes.root, className)}>
      <Card>
        <Divider />
        <CardContent className={classes.content}>
          <PerfectScrollbar>
            <div className={classes.inner}>
              <Table>
                <TableHead>
                  <TableRow>
                    {
                      (tableColumn.length >= 1) && tableColumn.map((col, index) => (
                        <TableCell align={(index + 1) === tableColumn.length ? 'right' : 'left'}>
                          {col.label}
                        </TableCell>
                      )
                      )
                    }
                  </TableRow>
                </TableHead>
                <TableBody>
                  {data.map((result, index) => (
                    <TableRow
                      hover
                      key={index}
                    >
                      {
                        (tableColumn.length >= 1) && tableColumn.map((bodyCol) => {
                          if (bodyCol.name) {
                            return (
                              <TableCell>
                                {
                                  bodyCol?.customLabel
                                    ? bodyCol?.customLabel(result[bodyCol.name])
                                    : result[bodyCol.name]
                                }
                              </TableCell>
                            )
                          }
                        })
                      }
                      <TableCell align="right">
                        <MoreButton
                          color="secondary"
                          index={index}
                          showDataDetailModal={showDataDetailModal}
                          showDeleteConfirmation={showDeleteConfirmation}
                          status={result.status}
                        />
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </div>
            {(data.length < 1) && (<LinearProgress />)}
          </PerfectScrollbar>
        </CardContent>
        <CardFooter className={classes.actions}>
          <div className={classes.paginationButton}>
            <Button
              color="secondary"
              onClick={() => onChangePage('prev')}
              variant="contained"
            >
              <ChevronLeftIcon />
            </Button>
            <Typography
              component="h1"
              variant="subtitle1"
            >
              {pagination?.current_page || 1}
            </Typography>
            <Button
              color="secondary"
              onClick={() => onChangePage('next')}
              variant="contained"
            >
              <ChevronRightIcon />
            </Button>
          </div>
          <div className={classes.paginationInfo}>
            <Typography
              color="secondary"
            >
              Halaman {pagination?.current_page || 1} dari {pagination?.total_pages || 1}
            </Typography>
          </div>
        </CardFooter>
      </Card>
    </div>
  );
};

Results.propTypes = {
  className: PropTypes.string,
  onChangePage: PropTypes.func,
  setIsEdit: PropTypes.func,
  setShowDataDetail: PropTypes.func,
  showDeleteConfirmation: PropTypes.func,
  stateName: PropTypes.string,
  tableColumn: PropTypes.array
};

export default Results;