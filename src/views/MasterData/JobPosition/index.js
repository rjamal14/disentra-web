import React from 'react'
import MasterData from '../MasterData';
import { getJobPositionPerPage, deleteJobPosition, addJobPosition, editJobPosition, refreshState } from 'actions';
import FormInput from './FormInput'

const path = [
  ['Master Data', '#!'],
  ['Jabatan', '/master-data/job-position'],
];

const tableColumn = [
  {
    name: 'name',
    label: 'Nama Jabatan',
  },
  {
    name: '',
    label: 'Aksi',
  }
];

const Role = () => {
  return (
    <MasterData
      addApi={addJobPosition}
      dataName="Jabatan"
      deleteApi={deleteJobPosition}
      editApi={editJobPosition}
      FormInput={FormInput}
      getApi={getJobPositionPerPage}
      path={path}
      refreshState={refreshState}
      stateName="jobPosition"
      tableColumn={tableColumn}
      title="Jabatan"
    />
  )
}

export default Role