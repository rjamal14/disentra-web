import React from 'react'

import MasterData from '../MasterData'
import { getVillage, deleteVillage, addVillage, editVillage, refreshState } from 'actions'
import FormInput from './FormInput'

const path = [
  ['Master Data', '/master-data'],
  ['Kelurahan', '/master-data/village'],
];

const tableColumn = [
  {
    name: 'name',
    label: 'Nama Kelurahan',
  },
  {
    name: 'district_name',
    label: 'Nama Kecamatan',
  },
  {
    name: 'city_name',
    label: 'Nama Kota/Kab.',
  },
  {
    name: '',
    label: 'Aksi',
  }
]

const Village = () => {
  return (
    <MasterData
      addApi={addVillage}
      dataName="Kelurahan"
      deleteApi={deleteVillage}
      FormInput={FormInput}
      editApi={editVillage}
      getApi={getVillage}
      path={path}
      refreshState={refreshState}
      stateName="village"
      tableColumn={tableColumn}
      title="Kelurahan"
    />
  )
}

export default Village