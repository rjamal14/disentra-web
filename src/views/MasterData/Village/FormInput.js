/* eslint-disable react/prop-types */
import React from 'react'
import { useSelector } from 'react-redux';
import { RoundedInput } from 'components';
import { RoundedAutocomplete } from 'components';
import validation from '../validation';
import { getCity, getDistrict } from 'actions';

const FormInput = (props) => {
  const { dataDetail, control, register, errors, setValue } = props;
  const { data: cityList } = useSelector(state => state.city);
  const { data: districtList } = useSelector(state => state.district);

  return (
    <>
      <RoundedInput 
        defaultValue={dataDetail?.name || ''}
        errorMessage={errors?.name?.message}
        errors={errors.name}
        label="Nama Kelurahan"
        name="name"
        register={register(validation.name)}
      />
      <RoundedAutocomplete
        control={control}
        defaultValue={{ id: dataDetail?.city_id, name: dataDetail?.city_name }}
        errorMessage={errors?.city_id?.message}
        errors={errors.city_id}
        label="Nama Kota/Kabupaten"
        name="city_id"
        options={cityList}
        register={register(validation.city_id)}
        searchApi={getCity}
        setValue={setValue}
        validation={validation.city_id}
      />
      <RoundedAutocomplete
        control={control}
        defaultValue={{ id: dataDetail?.district_id, name: dataDetail?.district_name }}
        errorMessage={errors?.district_id?.message}
        errors={errors.district_id}
        label="Nama Kecamatan"
        name="district_id"
        options={districtList}
        register={register(validation.district_id)}
        searchApi={getDistrict}
        setValue={setValue}
        validation={validation.district_id}
      />
    </>
  )
}

export default FormInput;