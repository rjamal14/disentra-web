import React from 'react'

import MasterData from '../MasterData'
import { getDistrict, deleteDistrict, addDistrict, editDistrict, refreshState } from 'actions'
import FormInput from './FormInput'

const path = [
  
  ['Master Data', '/master-data'],
  ['Kecamatan', '/master-data/district'],
];

const tableColumn = [
  {
    name: 'name',
    label: 'Nama Kecamatan',
  },
  {
    name: 'city_name',
    label: 'Nama Kota/Kab.',
  },
  {
    name: '',
    label: 'Aksi',
  }
]

const District = () => {
  return (
    <MasterData
      addApi={addDistrict}
      dataName="Kecamatan"
      deleteApi={deleteDistrict}
      FormInput={FormInput}
      editApi={editDistrict}
      getApi={getDistrict}
      path={path}
      refreshState={refreshState}
      stateName="district"
      tableColumn={tableColumn}
      title="Kecamatan"
    />)
}

export default District