/* eslint-disable react/prop-types */
import React from 'react'
import { useSelector } from 'react-redux';
import { RoundedInput } from 'components';
import { RoundedAutocomplete } from 'components';
import validation from '../validation';
import { getCity } from 'actions';

const FormInput = (props) => {
  const { dataDetail, control, register, errors, setValue } = props;
  const { data: cityList } = useSelector(state => state.city);

  return (
    <>
      <RoundedInput 
        defaultValue={dataDetail?.name || ''}
        errorMessage={errors?.name?.message}
        errors={errors.name}
        label="Nama Kecamatan"
        name="name"
        register={register(validation.name)}
      />
      <RoundedAutocomplete
        control={control}
        defaultValue={{ id: dataDetail?.city_id, name: dataDetail?.city_name }}
        errorMessage={errors?.city_id?.message}
        errors={errors.city_id}
        label="Nama Kota/Kabupaten"
        name="city_id"
        options={cityList}
        register={register(validation.city_id)}
        searchApi={getCity}
        setValue={setValue}
        validation={validation.city_id}
      />
    </>
  )
}

export default FormInput;