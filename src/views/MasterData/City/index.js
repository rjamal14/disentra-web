import React from 'react'

import MasterData from '../MasterData'
import { getCity, deleteCity, addCity, editCity, refreshState } from 'actions'
import FormInput from './FormInput'

const setKategori = (val) => {
  if (val === 'regency') return 'Kabupaten'
  else return 'Kota'
}

const path = [
  ['Master Data', '/master-data'],
  ['Kota/Kabupaten', '/master-data/city'],
];

const tableColumn = [
  {
    name: 'name',
    label: 'Nama Kota/Kab.',
  },
  {
    name: 'city_type',
    label: 'Kategori',
    customLabel: (val) => setKategori(val)
  },
  {
    name: 'province_name',
    label: 'Nama Provinsi',
  },
  {
    name: '',
    label: 'Aksi',
  }
]

const City = () => {
  return (
    <MasterData
      addApi={addCity}
      dataName="Kota/Kabupaten"
      deleteApi={deleteCity}
      editApi={editCity}
      FormInput={FormInput}
      getApi={getCity}
      path={path}
      refreshState={refreshState}
      stateName="city"
      tableColumn={tableColumn}
      title="Kota/Kabupaten"
    />
  )
}

export default City