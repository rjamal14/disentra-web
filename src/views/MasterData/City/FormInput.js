/* eslint-disable react/prop-types */
import React from 'react'
import { useSelector } from 'react-redux';
import { RoundedInput } from 'components';
import { RoundedSelect } from 'components';
import { RoundedAutocomplete } from 'components';
import validation from '../validation';
import { getProvince } from 'actions';

const FormInput = (props) => {
  const { dataDetail, control, register, errors, setValue } = props;
  const { data: provinceList } = useSelector(state => state.province);

  return (
    <>
      <RoundedInput 
        defaultValue={dataDetail?.name || ''}
        errorMessage={errors?.name?.message}
        errors={errors.name}
        label="Nama Kota/Kabupaten"
        name="name"
        register={register(validation.name)}
      />
      <RoundedSelect
        control={control}
        defaultValue={dataDetail?.city_type || 'city'}
        errorMessage={errors?.city_type?.message}
        errors={errors.city_type}
        label="Kategori"
        name="city_type"
        options={[
          {
            id: 'regency',
            name: 'Kabupaten'
          },
          {
            id: 'city',
            name: 'Kota'
          }
        ]}
        validation={validation.city_type}
      />
      <RoundedAutocomplete
        control={control}
        defaultValue={{ id: dataDetail?.province_id, name: dataDetail?.province_name }}
        errorMessage={errors?.province_id?.message}
        errors={errors.province_id}
        label="Nama Provinsi"
        name="province_id"
        options={provinceList}
        register={register(validation.province_id)}
        searchApi={getProvince}
        setValue={setValue}
        validation={validation.province_id}
      />
    </>
  )
}

export default FormInput;