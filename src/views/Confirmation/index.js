/* eslint-disable react/prop-types */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import Card from '@material-ui/core/Card';
import useStyles from './confirmation-jss.js';
import Updated from './Updated';
import Failed from './Failed';
import baseUrl from '../../services/baseUrl';
import env from '../../config/env';

const Confirmation = (props) => {
  const classes = useStyles();
  const [isError, setIsError] = useState(false);
  const [loading, setLoading] = useState(true);
  
  const confirmationRegistrasion = async () => {
    const str = props.location.search;
    try {
      const response = await baseUrl.get(`${env.baseUrl}/api/v1/users/confirmation?confirmation_token=`+str.split('=')[1]);
      setIsError(false);
      setTimeout(() => {
        setLoading(false);
      }, 1000);
    } catch(error) {
      setTimeout(() => {
        setLoading(false);
      }, 1000);
      setIsError(true);
    }
  }

  useEffect(() => {
    confirmationRegistrasion()
  }, [])
  
  return (
    <div className={classes.mainContainer}>
      <Card className={classes.card}>
        <CardContent className={classes.cardContent}>
          <img
            alt="logo"
            className={classes.img}
            src={'/icon/logo.png'}
          />
          {
            loading ? (
              <>
                <CircularProgress color="secondary" />
                <Typography
                  className={classes.textTitle}
                  component="h1"
                  variant="h5"
                >
                  Menunggu Konfirmasi..
                </Typography>
              </>
            ) : (
              isError ? (
                <Failed />
              ) : (
                <Updated />  
              )
            ) 
          }
        </CardContent>
      </Card>
    </div>
  );
};
export default Confirmation;
