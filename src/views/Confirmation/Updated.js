import React  from 'react';
import { Typography } from '@material-ui/core';
import useStyles from './confirmation-jss';

const Updated = () => {
  const classes = useStyles();
  return (
    <div className={classes.container}>
      <img
        alt="logo"
        className={classes.img2}
        src={'/icon/updated-email.png'}
      />
      <Typography
        className={classes.textTitle}
        component="h1"
        variant="h5"
      >
        Registrasi Berhasil!
      </Typography>
      <Typography
        className={classes.textDesc}
        component="h6"
        variant="h6"
      >
        Selamat! Anda kini terdaftar sebagai pengguna aplikasi BJB DiSentra.
      </Typography>
    </div>
  );
};

export default Updated;
