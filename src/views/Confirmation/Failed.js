import React  from 'react';
import { Typography } from '@material-ui/core';
import useStyles from './confirmation-jss';

const Updated = () => {
  const classes = useStyles();
  return (
    <div className={classes.container}>
      <img
        alt="logo"
        className={classes.img2}
        src={'/icon/failed.svg'}
      />
      <Typography
        className={classes.textTitle}
        component="h1"
        variant="h5"
      >
        Registrasi Gagal!
      </Typography>
      <Typography
        className={classes.textDesc}
        component="h6"
        variant="h6"
      >
        Token url tidak valid.
      </Typography>
    </div>
  );
};

export default Updated;
