import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0
  },
  inner: {
    minWidth: 700
  },
  nameCell: {
    display: 'flex',
    alignItems: 'center'
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1)
  },
  actions: {
    padding: theme.spacing(1),
    justifyContent: 'flex-end'
  },
  paginationButton: {
    border: '1px solid #cfcfcf',
    width: 200,
    borderRadius: 5,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  paginationInfo: {
    position: 'absolute',
    right: theme.spacing(4),
  },
  status: {
    display: 'flex',
    alignItems: 'center'
  },
  circleStatus: {
    width: 7,
    height: 7,
    borderRadius: '50%',
    marginRight: '0.5rem'
  },
  success: {
    background: '#43A047'
  },
  suspend: {
    background: '#D50000'
  }
}));

export default useStyles;
