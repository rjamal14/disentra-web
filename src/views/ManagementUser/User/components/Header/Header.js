/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-no-undef */
import React, { /*useState*/ } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {
  CardContent,
  Typography,
  Grid,
  Button,
  Box,
  TextField,
  Select,
  MenuItem,
  Hidden
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import useStyles from '../components-jss';
import { useSelector } from 'react-redux';

const CustomTextField = withStyles((theme) => ({
  root: {
    '& .MuiOutlinedInput-root': {
      paddingLeft: 5,
      paddingRight: 5,
      paddingTop:0,
      paddingBottom:0,
      marginLeft:5,
      height:40,
      '& fieldset': {
        borderColor: '#C4C4C4',
        borderRadius:50,
      },
      '&:hover fieldset': {
        borderColor: theme.palette.primary,
        borderRadius:50,
      },
      '&.Mui-focused fieldset': {
        borderColor: theme.palette.primary,
        borderRadius:50,
      },
    },
  },
}))(TextField);

const Header = props => {
  const { 
    className, 
    setShowModal, 
    dataPerPage, 
    setDataPerPage,
    executeFilter,
    jobPosition,
    resetFilter,
    roleName,
    searchQuery,
    setJobPosition,
    setQuerySearch,
    setRole,
  } = props;
  // const [action, setAction] = useState('Suspend');
  const { data: listRoles } = useSelector(state => state.role);
  const { data: listJobPositions } = useSelector(state => state.jobPosition);

  const classes = useStyles();

  const renderPerPage = () => (
    <Select
      className={clsx(classes.select, classes.space)}
      name="perPage"
      onChange={(e) => setDataPerPage(e.target.value)}
      value={dataPerPage}
      variant="outlined"
    >
      <MenuItem value={5}>5 Data</MenuItem>
      <MenuItem value={10}>10 Data</MenuItem>
      <MenuItem value={15}>15 Data</MenuItem>
      <MenuItem value={25}>25 Data</MenuItem>
    </Select>
  );

  // const renderAction = () => (
  //   <Select
  //     className={classes.select}
  //     name="perPage"
  //     onChange={(event)=>{
  //       setAction(event.target.value)
  //     }}
  //     value={action}
  //     variant="outlined"
  //   >
  //     <MenuItem value={'Suspend'}>Suspend</MenuItem>
  //     <MenuItem value={'Lift Ban'}>Lift Ban</MenuItem>
  //     <MenuItem value={'Hapus'}>Hapus</MenuItem>
  //   </Select>
  // );

  const renderJabatan = () => (
    <Select
      className={classes.select}
      defaultValue="$"
      name="perPage"
      onChange={(event) => {
        setJobPosition(event.target.value)
      }}
      value={jobPosition}
      variant="outlined"
    >
      <MenuItem value="$">Semua Jabatan</MenuItem>
      {
        listJobPositions.map((item, index) => (
          <MenuItem 
            key={index}
            value={item.id}
          >
            {item.name}
          </MenuItem>
        ))
      }
    </Select>
  );

  const renderRole = () => (
    <Select
      className={classes.select}
      name="perPage"
      onChange={(event) => {
        setRole(event.target.value)
      }}
      value={roleName}
      variant="outlined"
    >
      <MenuItem value="$">Semua Role</MenuItem>
      {
        listRoles.map((item, index) => (
          <MenuItem 
            key={index}
            value={item.name}
          >
            {item.name}
          </MenuItem>
        ))
      }
    </Select>
  );

  const renderSearch = () => (
    <CustomTextField
      className={classes.textField}
      name="perPage"
      onChange={(event) => {
        setQuerySearch(event.target.value)
      }}
      placeholder="Cari nama/NIP"
      value={searchQuery}
      variant="outlined"
    />
  );
  
  const addUser = () => {
    setShowModal(true);
  };

  return (
    <div
      className={clsx(classes.root, className)}
    >
      <CardContent>
        <div className={classes.root}>
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              md={6}
              xs={12}
            >
              <Typography variant="h3">Daftar Admin</Typography>
              <Typography
                style={{marginTop:'5px'}}
                variant="h6"
              >Atur tipe level akun yang dapat mengakses halaman dashboard BJB DiSentra.</Typography>
            </Grid>
            <Hidden smDown>
              <Grid
                item
                md={6}
                xs={12}
              >
                <Box
                  display="flex"
                  flexDirection="row-reverse"
                >
                  <Button
                    className={classes.leftToolbar}
                    color="primary"
                    onClick={addUser}
                    type="submit"
                    variant="contained"
                  >
                    <Typography variant="button">Tambah Admin</Typography>
                  </Button>
                  <div className={classes.leftToolbar}>
                    <Typography variant="h6">Tampilkan: </Typography>
                    {renderPerPage()}
                  </div>
                </Box>
              </Grid>
              <Grid
                container
                spacing={3}
              >
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  {/* <div className={classes.leftToolbar}>
                    <Typography variant="h6">Tindakan: </Typography> 
                    {renderAction()}
                  </div> */}
                </Grid>
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <Box
                    display="flex"
                    flexDirection="row-reverse"
                  >
                    <Hidden mdDown>
                      <Box
                        display="flex"
                        flexDirection="row-reverse"
                      >
                        <Hidden mdDown>
                          <Button
                            className={classes.btn}
                            onClick={resetFilter}
                            type="button"
                            variant="outlined"
                          >
                            <Typography variant="button">Reset</Typography>
                          </Button>
                          <Button
                            className={classes.btn}
                            color="secondary"
                            onClick={executeFilter}
                            type="button"
                            variant="contained"
                          >
                            <Typography variant="buttonSecondary">Terapkan</Typography>
                          </Button>
                        </Hidden>
                      </Box>
                      <div className={classes.leftToolbar}>
                        {renderSearch()}
                        {renderJabatan()}
                        {renderRole()}
                      </div>
                      <div className={classes.leftToolbar} />
                    </Hidden>
                  </Box>
                </Grid>
                <Hidden mdUp>
                  <Grid
                    container
                    md={12}
                    xs={12}
                  />
                </Hidden>
              </Grid>
            </Hidden>
            <Hidden mdUp>
              <Grid
                item
                md={6}
                xs={12}
              >
                <div className={classes.leftToolbar}>
                  <Typography variant="h6">Tampilkan:</Typography>
                  {renderPerPage()}
                </div>
                <Button
                  className={classes.leftToolbar}
                  color="primary"
                  onClick={addUser}
                  style={{ marginTop: '1rem' }}
                  type="submit"
                  variant="contained"
                >
                  <Typography variant="button">Tambah Admin</Typography>
                </Button>
              </Grid>
            </Hidden>
          </Grid>
        </div>
      </CardContent>
    </div>
  );
};

Header.propTypes = {
  className: PropTypes.string,
  dataPerPage: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  executeFilter: PropTypes.func,
  jobPosition: PropTypes.string,
  onSearch: PropTypes.func,
  resetFilter: PropTypes.func,
  roleName: PropTypes.string,
  searchQuery: PropTypes.string,
  setDataPerPage: PropTypes.func,
  setJobPosition: PropTypes.func,
  setQuerySearch: PropTypes.func,
  setRole: PropTypes.func,
  setShowModal: PropTypes.func,
};

export default Header;
