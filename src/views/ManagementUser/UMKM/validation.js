const defaultMessage = 'Tidak boleh kosong';
const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const phoneID = /^(^\+62|62|^08)(\d{3,4}-?){2}\d{3,4}$/;

const validation = {
  name: {
    required: defaultMessage
  },
  email: {
    pattern: re,
    required: defaultMessage
  },
  nip: {
    required: defaultMessage
  },
  photo: {
    required: defaultMessage
  },
  phone: {
    required: defaultMessage,
    minLength: 10,
    maxLength: 14,
    pattern: phoneID
  },
  role_ids: {
    required: defaultMessage
  },
  job_position_id: {
    required: defaultMessage
  },
  branch_office_id: {
    required: defaultMessage
  },
  account_type: {
    required: defaultMessage
  },
  consultant_profession: {
    required: defaultMessage
  },
  account_description: {
    required: defaultMessage
  }
}

export default validation;