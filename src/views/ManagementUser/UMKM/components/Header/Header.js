/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-no-undef */
import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {
  CardContent,
  Typography,
  Grid,
  Button,
  Box,
  TextField,
  Select,
  MenuItem,
  Hidden
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import useStyles from '../components-jss';

const CustomTextField = withStyles((theme) => ({
  root: {
    '& .MuiOutlinedInput-root': {
      paddingLeft: 5,
      paddingRight: 5,
      paddingTop:0,
      paddingBottom:0,
      marginLeft:5,
      height:40,
      '& fieldset': {
        borderColor: '#C4C4C4',
        borderRadius:50,
      },
      '&:hover fieldset': {
        borderColor: theme.palette.primary,
        borderRadius:50,
      },
      '&.Mui-focused fieldset': {
        borderColor: theme.palette.primary,
        borderRadius:50,
      },
    },
  },
}))(TextField);

const Header = props => {
  const { 
    className, 
    dataPerPage, 
    setDataPerPage,
    executeFilter,
    resetFilter,
    searchQuery,
    setQuerySearch,
  } = props;
  // const [action, setAction] = useState('Suspend');
  const classes = useStyles();

  const renderPerPage = () => (
    <Select
      className={clsx(classes.select, classes.space)}
      name="perPage"
      onChange={(e) => setDataPerPage(e.target.value)}
      value={dataPerPage}
      variant="outlined"
    >
      <MenuItem value={5}>5 Data</MenuItem>
      <MenuItem value={10}>10 Data</MenuItem>
      <MenuItem value={15}>15 Data</MenuItem>
      <MenuItem value={25}>25 Data</MenuItem>
    </Select>
  );

  const renderSearch = () => (
    <CustomTextField
      className={classes.textField}
      name="perPage"
      onChange={(event) => {
        setQuerySearch(event.target.value)
      }}
      placeholder="Cari nama"
      value={searchQuery}
      variant="outlined"
    />
  );
  
  return (
    <div
      className={clsx(classes.root, className)}
    >
      <CardContent>
        <div className={classes.root}>
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              md={6}
              xs={12}
            >
              <Typography variant="h3">Daftar UMKM</Typography>
              <Typography
                style={{marginTop:'5px'}}
                variant="h6"
              >
                Atur tipe level akun yang dapat mengakses halaman dashboard BJB DiSentra.
              </Typography>
            </Grid>
            <Hidden smDown>
              <Grid
                item
                md={6}
                xs={12}
              >
                <Box
                  display="flex"
                  flexDirection="row-reverse"
                >
                  <div className={classes.leftToolbar}>
                    <Typography variant="h6">Tampilkan: </Typography>
                    {renderPerPage()}
                  </div>
                </Box>
              </Grid>
              <Grid
                container
                spacing={3}
              >
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  {/* <div className={classes.leftToolbar}>
                    <Typography variant="h6">Tindakan: </Typography> 
                    {renderAction()}
                  </div> */}
                </Grid>
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <Box
                    display="flex"
                    flexDirection="row-reverse"
                  >
                    <Hidden mdDown>
                      <Box
                        display="flex"
                        flexDirection="row-reverse"
                      >
                        <Hidden mdDown>
                          <Button
                            className={classes.btn}
                            onClick={resetFilter}
                            type="button"
                            variant="outlined"
                          >
                            <Typography variant="button">Reset</Typography>
                          </Button>
                          <Button
                            className={classes.btn}
                            color="secondary"
                            onClick={executeFilter}
                            type="button"
                            variant="contained"
                          >
                            <Typography variant="buttonSecondary">Terapkan</Typography>
                          </Button>
                        </Hidden>
                      </Box>
                      <div className={classes.leftToolbar}>
                        {renderSearch()}
                      </div>
                      <div className={classes.leftToolbar} />
                    </Hidden>
                  </Box>
                </Grid>
                <Hidden mdUp>
                  <Grid
                    container
                    md={12}
                    xs={12}
                  />
                </Hidden>
              </Grid>
            </Hidden>
            <Hidden mdUp>
              <Grid
                item
                md={6}
                xs={12}
              >
                <div className={classes.leftToolbar}>
                  <Typography variant="h6">Tampilkan:</Typography>
                  {renderPerPage()}
                </div>
              </Grid>
            </Hidden>
          </Grid>
        </div>
      </CardContent>
    </div>
  );
};

Header.propTypes = {
  className: PropTypes.string,
  dataPerPage: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  executeFilter: PropTypes.func,
  jobPosition: PropTypes.string,
  onSearch: PropTypes.func,
  resetFilter: PropTypes.func,
  roleName: PropTypes.string,
  searchQuery: PropTypes.string,
  setDataPerPage: PropTypes.func,
  setJobPosition: PropTypes.func,
  setQuerySearch: PropTypes.func,
  setRole: PropTypes.func,
  setShowModal: PropTypes.func,
};

export default Header;
