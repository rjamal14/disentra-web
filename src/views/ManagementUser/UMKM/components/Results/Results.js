import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import useStyles from './result-jss';
import { withStyles } from '@material-ui/core/styles';
import {
  Card,
  CardActions,
  CardContent,
  Divider,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
  Button,
} from '@material-ui/core';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { MoreButton } from '../index';
import { useDispatch, useSelector } from 'react-redux';
import { setUserDataDetail } from 'actions';

const CardFooter = withStyles(() => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    padding: '30px 0',
  }
}))(CardActions);

const Results = props => {
  const { 
    className, 
    onChangePage,
    setShowDataDetail, 
    setIsEdit, 
    showDeleteConfirmation, 
    showChangeStatusConfirmation,
  } = props;

  const classes = useStyles();
  const dispatch = useDispatch();

  const { data, pagination } = useSelector(state => state.userUmkm);

  const showDataDetailModal = (index, noEdit) => {
    if (!noEdit) {
      setIsEdit(true); 
    }
    dispatch(setUserDataDetail({
      payload: data[index]
    }))
    setShowDataDetail(true);
  }

  return (
    <div className={clsx(classes.root, className)}>
      <Card>
        <Divider />
        <CardContent className={classes.content}>
          <PerfectScrollbar>
            <div className={classes.inner}>
              <Table>
                <TableHead>
                  <TableRow>
                    {/* <TableCell padding="checkbox">
                      <Checkbox
                        checked={selectedCustomers.length === data.length}
                        color="primary"
                        indeterminate={
                          selectedCustomers.length > 0 &&
                          selectedCustomers.length < data.length
                        }
                        onChange={handleSelectAll}
                      />
                    </TableCell> */}
                    <TableCell>Nama</TableCell>
                    <TableCell>Nomor HP</TableCell>
                    <TableCell>Email</TableCell>
                    <TableCell>Status</TableCell>
                    <TableCell align="right" />
                  </TableRow>
                </TableHead>
                <TableBody>
                  {data.map((customer, index) => (
                    <TableRow
                      hover
                      key={index}
                    >
                      {/* <TableCell padding="checkbox">
                        <Checkbox
                          checked={
                            selectedCustomers.indexOf(customer.id) !== -1
                          }
                          color="primary"
                          onChange={event =>
                            handleSelectOne(event, customer.id)
                          }
                          value={selectedCustomers.indexOf(customer.id) !== -1}
                        />
                      </TableCell> */}
                      <TableCell>
                        <div className={classes.nameCell}>
                          <div>
                            <Typography
                              color="inherit"
                              component="a"
                              onClick={() => showDataDetailModal(index, 'noEdit')}
                              variant="h6"
                            >
                              {customer.name}
                            </Typography>
                          </div>
                        </div>
                      </TableCell>
                      <TableCell>
                        {customer.phone_number}
                      </TableCell>
                      <TableCell>{customer?.email}</TableCell>
                      <TableCell>
                        {customer.status === 'active' ? (
                          <div className={classes.status}>
                            <span className={`${classes.circleStatus} ${classes.success}`} />
                            <p>Aktif</p>
                          </div>
                        ) : (
                          <div className={classes.status}>
                            <span className={`${classes.circleStatus} ${classes.suspend}`} />
                            <p>Suspend</p>
                          </div>
                        )}
                      </TableCell>
                      <TableCell align="right">
                        <MoreButton 
                          color="secondary" 
                          customer={customer}
                          index={index}
                          showChangeStatusConfirmation={showChangeStatusConfirmation}
                          showDataDetailModal={showDataDetailModal}
                          showDeleteConfirmation={showDeleteConfirmation}
                        />
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </div>
          </PerfectScrollbar>
        </CardContent>
        <CardFooter className={classes.actions}>
          {
            // eslint-disable-next-line no-extra-boolean-cast
            Boolean(data?.length) ? (
              <>
                <div className={classes.paginationButton}>
                  <Button
                    color="secondary"
                    onClick={() => onChangePage('prev')}
                    variant="contained"
                  >
                    <ChevronLeftIcon />
                  </Button>
                  <Typography
                    component="h1"
                    variant="subtitle1"
                  >
                    {pagination?.current_page || 1}
                  </Typography>
                  <Button
                    color="secondary"
                    onClick={() => onChangePage('next')}
                    variant="contained"
                  >
                    <ChevronRightIcon />
                  </Button>
                </div>
              </>
            ) : (
              <Typography
                component="h2"
                varian="body1"
              >
                Data Tidak Ditemukan
              </Typography>
            )
          }
          <div className={classes.paginationInfo}>
            <Typography
              color="secondary"
            >
              Halaman {pagination?.current_page || 1} dari {pagination?.total_pages || 1}
            </Typography>
          </div>
        </CardFooter>
      </Card>
    </div>
  );
};

Results.propTypes = {
  className: PropTypes.string,
  onChangePage: PropTypes.func,
  setIsEdit: PropTypes.func,
  setShowDataDetail: PropTypes.func,
  showChangeStatusConfirmation: PropTypes.func,
  showDeleteConfirmation: PropTypes.func,
};

Results.defaultProps = {
  customers: []
};

export default Results;