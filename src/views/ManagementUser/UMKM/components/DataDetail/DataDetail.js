import React, { useEffect, useRef } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import { useForm } from 'react-hook-form';
import { 
  Typography, 
  useMediaQuery,
} from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import useStyles from './data-detail-jss';
import { refreshStateUser } from 'actions';
import FieldDisplay from '../FieldDisplay';

function DataDetail(props) {
  const { 
    hideDetailModal, 
  } = props;
  const classes = useStyles();
  const theme = useTheme();
  const dispatch = useDispatch();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const { dataDetail, success, loading } = useSelector(state => state.user);
  const { register } = useForm();
  const initialization = useRef(true);

  useEffect(() => {
    if (initialization.current) {
      initialization.current = false;
    } else {
      if (success && !loading) {
        closeModal(false);
        dispatch(refreshStateUser());
      }
    }
  }, [success, loading]);

  const closeModal = () => {
    hideDetailModal();
  };

  return(
    <div className={clsx(classes.root, !fullScreen && classes.desktop)}>
      <input
        defaultValue={dataDetail.user_profile?.id}
        name="user_profile_id"
        ref={register}
        type="hidden"
      />
      <div className={classes.uploadContainer}>
        <Typography
          component="p"
          variant="body1"
        >
          Foto User
        </Typography>
        <div className={clsx(classes.uploadArea, fullScreen && classes.fullWidthRel)}>
          {dataDetail.photo && (
            <img 
              alt="user"
              className={classes.userImage} 
              src={dataDetail.photo?.url} 
            />
          )}
        </div>
      </div>
      <div className={clsx(classes.formContent, fullScreen && classes.fullWidth)}>
        <FieldDisplay 
          data={dataDetail?.name} 
          label="Nama"
        />
        <FieldDisplay 
          data={dataDetail.email} 
          label="Email"
        />
        <FieldDisplay 
          data={dataDetail.phone_number} 
          label="Nomor Handphone"
        />
      </div>
    </div>
  )
}

DataDetail.propTypes = {
  hideDetailModal: PropTypes.func,
  isEdit: PropTypes.bool,
  setIsEdit: PropTypes.func,
  showChangeStatusConfirmation: PropTypes.func,
  showDeleteConfirmation: PropTypes.func,
};

export default DataDetail;