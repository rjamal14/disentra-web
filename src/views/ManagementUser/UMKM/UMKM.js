/*eslint-disable react/jsx-boolean-value*/
import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';

import { Page, Breadcrumb, Modal } from 'components';
import { Header, Results, Form, DataDetail } from './components';
import { Card } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { getUserUmkm, deleteUserUmkm, changeStatusUserUmkm } from 'actions';
import { getRoles } from 'actions';
import { getJobPositions } from 'actions';
import swal from 'sweetalert';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  results: {
    marginTop: theme.spacing(3)
  },
  breadcrumb:{
    margin: theme.spacing(3),
    marginBottom: -theme.spacing(6),
  }
}));

const path = [
  ['Manajemen User', '/management-user/umkm'],
  ['UMKM', '/management-user/umkm']
];

const UMKM = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const [querySearch, setQuerySearch] = useState('');
  const [user_profile_job_position_id_eq, setJobPosition] = useState('$');
  const [role_name, setRole] = useState('$');
  
  const [showModal, setShowModal] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [showDataDetail, setShowDataDetail] = useState(false);
  const [dataPerPage, setDataPerPage] = useState(10);
  const { data, pagination } = useSelector(state => state.userUmkm);

  useEffect(() => {
    dispatch(getUserUmkm({
      payload: {
        dataPerPage
      }
    })); // Get users to display in management user
    dispatch(getRoles()); // Get roles for options in add user
    dispatch(getJobPositions()); // Get job position for options in add user
  }, []);

  useEffect(() => {
    dispatch(getUserUmkm({
      payload: {
        dataPerPage
      }
    })); 
  }, [dataPerPage]);

  const refetchUser = (perPage, additionalFilter) => {
    dispatch(getUserUmkm({
      payload: {
        dataPerPage: perPage || dataPerPage,
        params: additionalFilter
      }
    }))
  };

  const showDeleteConfirmation = async (index) => {
    const confirmation = await swal({
      title: 'Hapus UMKM',
      text: 'Setelah dihapus, Anda tidak akan dapat memulihkan data ini!',
      icon: 'warning',
      buttons: true,
      dangerMode: true,
    })
    if (confirmation) {
      deleteExecution(index);
    }
  };

  const deleteExecution = (index) => {
    dispatch(deleteUserUmkm({
      payload: {
        id: data[index].id
      }
    }))
  };

  const showChangeStatusConfirmation = async (index, status) => {
    const confirmation = await swal({
      title: status === 'suspend' ? 'Suspend UMKM' : 'Aktifkan UMKM',
      text: `Apakah Anda yakin akan ${status === 'suspend' ? 'mensuspend' : 'mengaktifkan'} UMKM ini?`,
      icon: 'warning',
      buttons: true,
      dangerMode: true,
    })
    if (confirmation) {
      suspendExecution(index, status);
    }
  };

  const suspendExecution = (index, status) => {
    dispatch(changeStatusUserUmkm({
      payload: {
        id: data[index].id,
        status
      }
    }))
  };

  const hideDetailModal = () => {
    setShowDataDetail(false);
    setIsEdit(false);
  };

  const onChangePage = (direction) => {
    let currentPage = pagination?.current_page ?? 1;
    const totalPages = pagination?.total_pages ?? 1;
    if (direction === 'next') {
      if (currentPage < totalPages) {
        dispatch(getUserUmkm({
          payload: {
            dataPerPage,
            page: pagination?.current_page + 1 ?? 1
          }
        }))
      }
    } else {
      if (currentPage > 1) {
        dispatch(getUserUmkm({
          payload: {
            dataPerPage,
            page: pagination?.current_page - 1 ?? 1
          }
        }))
      }
    }
  }

  const executeFilter = () => {
    refetchUser(null, {
      querySearch,
      user_profile_job_position_id_eq,
      role_name,
    })
  };

  const resetFilter = () => {
    setQuerySearch('');
    setJobPosition('$');
    setRole('$')
    refetchUser(dataPerPage)
  };

  return (
    <Page
      className={classes.root}
      title="UMKM"
    >
      <Modal 
        children={<Form toggleModal={setShowModal} />}
        isOpen={showModal}
        title="Tambah UMKM"
        toggleModal={setShowModal}
      />
      <Modal
        children={
          <DataDetail 
            hideDetailModal={hideDetailModal}
            isEdit={isEdit}
            setIsEdit={setIsEdit}
            showChangeStatusConfirmation={showChangeStatusConfirmation}
            showDeleteConfirmation={showDeleteConfirmation}
          />
        }
        hideDetailModal={hideDetailModal}
        isOpen={showDataDetail}
        title="Detail UMKM"
        toggleModal={setShowDataDetail}
      />
      <Card>
        <div className={classes.breadcrumb}>
          <Breadcrumb route={path} />
        </div>
        <Header 
          dataPerPage={dataPerPage}
          executeFilter={executeFilter}
          jobPosition={user_profile_job_position_id_eq}
          resetFilter={resetFilter}
          roleName={role_name}
          searchQuery={querySearch}
          setDataPerPage={setDataPerPage}
          setJobPosition={setJobPosition}
          setQuerySearch={setQuerySearch}
          setRole={setRole}
          setShowModal={setShowModal} 
          showModal={showModal}
        />
        <Results
          className={classes.results}
          onChangePage={onChangePage}
          setIsEdit={setIsEdit}
          setShowDataDetail={setShowDataDetail}
          showChangeStatusConfirmation={showChangeStatusConfirmation}
          showDeleteConfirmation={showDeleteConfirmation}
        />
      </Card>
    </Page>
  );
};

export default UMKM;