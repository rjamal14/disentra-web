/* eslint-disable react/no-multi-comp */
/* eslint-disable react/sort-prop-types */
import React, { useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  CardActions,
  CardContent,
  Checkbox,
  Divider,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
  Button
} from '@material-ui/core';
import { TableEditBar } from 'components';
import { MoreButton } from '../index';
import { withStyles } from '@material-ui/core/styles';
import useStyles from '../components-jss';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import useStyles2 from './tables-jss';

const CustomTableCell = withStyles((theme) => ({
  root: {
    ...theme.typography.titleTable,
    borderBottom: `1px solid ${theme.palette.divider}`
  },
}))(TableCell);

const CardFooter = withStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    padding: '30px 0',
  },
  actions: {
    padding: theme.spacing(1),
    justifyContent: 'flex-end'
  },
}))(CardActions);


const Results = props => {
  const { className, Roles, getValueAction, onChangePage, pagination } = props;
  
  const classes = useStyles();
  const classes2 = useStyles2();
  
  const [selectedRoles, setSelectedRoles] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
 
  
  const handleSelectAll = event => {
    const selectedRoles = event.target.checked
      ? Roles.map(role => role.id)
      : [];
    
    setSelectedRoles(selectedRoles);
  };

  const handleSelectOne = (event, id) => {
    const selectedIndex = selectedRoles.indexOf(id);
    let newSelectedRoles = [];

    if (selectedIndex === -1) {
      newSelectedRoles = newSelectedRoles.concat(selectedRoles, id);
    } else if (selectedIndex === 0) {
      newSelectedRoles = newSelectedRoles.concat(
        selectedRoles.slice(1)
      );
    } else if (selectedIndex === selectedRoles.length - 1) {
      newSelectedRoles = newSelectedRoles.concat(
        selectedRoles.slice(0, -1)
      );
    } else if (selectedIndex > 0) {
      newSelectedRoles = newSelectedRoles.concat(
        selectedRoles.slice(0, selectedIndex),
        selectedRoles.slice(selectedIndex + 1)
      );
    }

    setSelectedRoles(newSelectedRoles);
  };

  const handleChangePage = (event, page) => {
    setPage(page);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(event.target.value);
  };

  return (
    <div
      className={clsx(classes.root, className)}
    >
      <Typography
        color="textSecondary"
        gutterBottom
        variant="body2"
      />
      <Divider />
      <CardContent className={classes.content}>
        <PerfectScrollbar>
          <div className={classes.inner}>
            <Table>
              <TableHead>
                <TableRow>
                  {/* <TableCell padding="checkbox">
                    <Checkbox
                      checked={selectedRoles.length === Roles.length}
                      color="primary"
                      indeterminate={
                        selectedRoles.length > 0 &&
                          selectedRoles.length < Roles.length
                      }
                      onChange={handleSelectAll}
                    />
                  </TableCell> */}
                  <CustomTableCell>Nama Role</CustomTableCell>
                  <CustomTableCell>Deskripsi</CustomTableCell>
                  <TableCell align="right" />
                </TableRow>
              </TableHead>
              <TableBody>
                {Roles.slice(0, rowsPerPage).map(role => (
                  <TableRow
                    hover
                    key={role.id}
                    selected={selectedRoles.indexOf(role.id) !== -1}
                  >
                    {/* <TableCell padding="checkbox">
                      <Checkbox
                        checked={
                          selectedRoles.indexOf(role.id) !== -1
                        }
                        color="primary"
                        onChange={event =>
                          handleSelectOne(event, role.id)
                        }
                        value={selectedRoles.indexOf(role.id) !== -1}
                      />
                    </TableCell> */}
                    <TableCell>{role.name}</TableCell>
                    <TableCell width="70%">{role.description}</TableCell>
                    <TableCell align="right">
                      <MoreButton
                        color="secondary"
                        getValue={(type,val)=>{
                          if (val !== null){
                            getValueAction(type,val)
                          }
                        }}
                        id={role.id}
                        role={role}
                      />
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </div>
        </PerfectScrollbar>
      </CardContent>
      <CardFooter className={classes2.actions}>
        <div className={classes2.paginationButton}>
          <Button
            color="secondary"
            onClick={() => onChangePage('prev')}
            variant="contained"
          >
            <ChevronLeftIcon />
          </Button>
          <Typography
            component="h1"
            variant="subtitle1"
          >
            {pagination?.current_page || 1}
          </Typography>
          <Button
            color="secondary"
            onClick={() => onChangePage('next')}
            variant="contained"
          >
            <ChevronRightIcon />
          </Button>
        </div>
        <div className={classes2.paginationInfo}>
          <Typography
            color="secondary"
          >
              Halaman {pagination?.current_page || 1} dari {pagination?.total_pages || 1}
          </Typography>
        </div>
      </CardFooter>
      <TableEditBar selected={selectedRoles} />
    </div>
  );
};

Results.propTypes = {
  className: PropTypes.string,
  getValueAction: PropTypes.func,
  Roles: PropTypes.array.isRequired,
  setShowModal: PropTypes.func,
  afterSubmit: PropTypes.func,
  onChangePage: PropTypes.func,
  pagination:PropTypes.array
};

Results.defaultProps = {
  Roles: []
};

export default Results;
