/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
import React, { useState } from 'react';
import Card from '@material-ui/core/Card';
import useStyles from './forgot-jss';
import SentEmail from './SentEmail';
import { CardContent, Link, Typography } from '@material-ui/core';
import ForgotForm from './ForgotPassword';
import baseUrl from '../../services/baseUrl';
import env from '../../config/env';
import { AlertMessage } from 'components';
import swal from 'sweetalert';

const uri = '/api/v1/users/password';

const Forgot = (props) => {
  const classes = useStyles();
  const initialValues = { email: '', scope: 'admin' };
  const [error, setError] = useState({ message: '', type: 'error' });
  const [isError, setIsError] = useState(false);
  const [email, setEmail] = useState(true);
  const [isSent, setIsSent] = useState(true);
  const handleSubmit = async (value) => {
    try {
      setEmail(value.email)
      const { status, data } = await baseUrl.post(`${env.baseUrl + uri }`, value);
      if (status === 200) {
        if(data.data.error === 'user_not_found'){
          setIsError(true);
          setError({ message: 'Email tidak terdaftar !', type: 'error' });
        }else{
          setIsSent(false);
        }
      }
    } catch (error) {
      if (error.response.status === 401){
        swal('Sesi sudah berakhir', 'Lakukan kembali lupa kata sandi untuk mendapatkan link baru', 'error');
      }else{
        swal('Kesalahan tidak diketahui', 'Server Internal Error', 'error');
      }
    }
  };
  const triggerSetError = (value) => {
    setIsError(value);
  };
  return (
    <div className={classes.mainContainer}>
      <Card className={classes.card}>
        <CardContent className={classes.cardContent}>
          <img
            alt="logo"
            className={classes.img}
            src={'/icon/logo.png'}
          />
          <AlertMessage
            message={error.message}
            open={isError}
            severity={error.type}
            triggerSetError={triggerSetError}
          />
          { 
            isSent ?
              <ForgotForm
                initialValues={initialValues}
                onSubmit={handleSubmit}
                props={props}
              />
              : 
              <SentEmail initialValues={email} />
          }

          <div className={classes.buttonContainer2}>
            <div className={classes.rememberMe} />
            <Typography 
              variant="h6"
            >Punya akun?</Typography> 
            <Link
              className={classes.linkIn}
              color="secondary"
              href="/"
              variant="h6"
            >
              Masuk
            </Link>
          </div>
        </CardContent>
      </Card>
    </div>
  );
};
export default Forgot;
