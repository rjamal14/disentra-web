import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Formik, Form, Field } from 'formik';
import { Button, CircularProgress, InputAdornment, Typography } from '@material-ui/core';
import { TextFieldLogin } from 'components';
import useStyles from './forgot-jss.js';

const ForgotForm = ({ initialValues, onSubmit }) => {
  const classes = useStyles();
  const [check, setCheck] = useState(false);

  const validateEmail = (value) => {
    let error;
    
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!re.test(value)){
      error = 'Format email salah !';
      setTimeout(function(){ setCheck(false) }, 500);
    }

    if (!value) {
      error = 'Email tidak boleh kosong !';
    }

    if (re.test(value)){
      setTimeout(function(){ setCheck(true) }, 500);
    }

    return error;
    
  };

  return (
    <Formik
      component={({ submitForm, isSubmitting, errors, touched }) => (
        <Form
          autoComplete="off"
          className={classes.container}
        >
          <Typography
            className={classes.textForgot}
            component="h4"
            variant="h4"
          >
            Lupa Kata Sandi?
          </Typography>
          <Typography
            className={classes.textDesc}
            component="h6"
            variant="h6"
          >
            Mohon masukkan alamat email Anda. Kami akan mengirimkan tautan untuk mengatur ulang kata sandi Anda.
          </Typography>
          <div className={classes.form}>
            <Field
              autoComplete="off"
              component={TextFieldLogin}
              fullWidth
              helperText={touched.email && errors.email}
              InputProps={{
                endAdornment: check ? (
                  <InputAdornment position="start">
                    <img src={'/icon/check.png'} />
                  </InputAdornment>
                ) : (
                  <div />
                ),
              }}
              margin="normal"
              name="email"
              placeholder="Email"
              validate={validateEmail}
              variant="outlined"
            />
            <div className={classes.buttonContainer}>
              <Button
                className={classes.btn}
                color="primary"
                disabled={isSubmitting}
                fullWidth
                onClick={submitForm}
                type="submit"
                variant="contained"
              >
                {
                  isSubmitting ? 
                    <CircularProgress
                      color="secondary"
                      size={24}
                    /> : 'Kirim'
                }
              </Button>
            </div>
          </div>
        </Form>
      )}
      initialValues={initialValues}
      onSubmit={async (value) => {
        await onSubmit(value);
      }}
    />
  );
};

ForgotForm.propTypes = ({
  initialValues: PropTypes.object.isRequired,
  onSubmit: PropTypes.func.isRequired
});

export default ForgotForm;
