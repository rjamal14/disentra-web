import React  from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import useStyles from './forgot-jss.js';

const SentEmail = ({ initialValues }) => {
  const classes = useStyles();
  return (
    <div className={classes.container}>
      <img
        alt="logo"
        className={classes.img2}
        src={'/icon/sent-email.png'}
      />
      <Typography
        className={classes.emailSend}
        component="h4"
        variant="h4"
      >
        Email Terkirim
      </Typography>
      <Typography
        className={classes.emailSendDesc}
        component="h6"
        variant="h6"
      >
        Kami telah mengirimkan tautan ke alamat email {initialValues} untuk mengatur ulang kata sandi Anda.
      </Typography>
    </div>
  );
};

SentEmail.propTypes = ({
  initialValues: PropTypes.object.isRequired,
  onSubmit: PropTypes.func.isRequired
});

export default SentEmail;
