import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { CardContent } from '@material-ui/core';
import { Page } from 'components';
import DashboardImg from 'assets/images/dashboard.png';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  container: {
    marginTop: theme.spacing(3)
  },
  dashboard: {
    background: '#FFFFFF',
    boxShadow: '0px 0px 1px rgba(0, 0, 0, 0.25)',
    borderRadius: 4,
    padding: '6rem',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column'
  },
  title: {
    fontWeight: 600,
    margin: '2rem 1.5rem'
  },
  description: {
    maxWidth: 600,
    lineHeight: '2rem',
    textAlign: 'center'
  }
}));

const DashboardDefault = () => {
  const classes = useStyles();
  return (
    <Page
      className={classes.root}
      title="Default Dashboard"
    >
      <CardContent style={{ padding: 0 }}>
        <div className={classes.dashboard}>
          <img
            alt="dashboard"
            src={DashboardImg} 
          />
          <h1 className={classes.title}>Selamat Datang di Dashboard BJB DiSentra</h1>
          <p className={classes.description}>Lakukan pengaturan terhadap user, konten, dan master data untuk aplikasi mobile DiSentra.</p>
        </div>
      </CardContent>
    </Page>
  );
};

export default DashboardDefault;
