const defaultMessage = 'Tidak boleh kosong';

const validation = {
  image: {
    required: defaultMessage
  },
  nama: {
    required: defaultMessage
  },
}

export default validation;