/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-no-undef */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {
  CardContent,
  Typography,
  Grid,
  Button,
  Box,
  Select,
  MenuItem,
  Hidden
} from '@material-ui/core';
import useStyles from '../components-jss';

const Header = props => {
  const { 
    businessField,
    executeFilter,
    resetFilter,
    // setBannerType,
    setStatus,
    status,
    className, 
    setShowModal, 
    dataPerPage, 
    setDataPerPage,
  } = props;
  const [action, setAction] = useState('Hapus');

  const classes = useStyles();

  const renderPerPage = () => (
    <Select
      className={clsx(classes.select, classes.space)}
      name="perPage"
      onChange={(e) => setDataPerPage(e.target.value)}
      value={dataPerPage}
      variant="outlined"
    >
      <MenuItem value={5}>5 Data</MenuItem>
      <MenuItem value={10}>10 Data</MenuItem>
      <MenuItem value={15}>15 Data</MenuItem>
      <MenuItem value={25}>25 Data</MenuItem>
    </Select>
  );

  const renderAction = () => (
    <Select
      className={classes.select}
      name="perPage"
      onChange={(event)=>{
        setAction(event.target.value)
      }}
      value={action}
      variant="outlined"
    >
      <MenuItem value={'Hapus'}>Hapus</MenuItem>
    </Select>
  );

  const renderCategory = () => (
    <Select
      className={classes.select}
      name="perPage"
      onChange={(event) => {
        setStatus(event.target.value)
      }}
      value={status}
      variant="outlined"
    >
      <MenuItem value={'$'}>Status</MenuItem>
      <MenuItem value={'active'}>Aktif</MenuItem>
      <MenuItem value={'inactive'}>Tidak Aktif</MenuItem>
    </Select>
  );


  // const renderSearch = () => (
  //   <Select
  //     className={classes.select}
  //     name="perPage"
  //     onChange={(event) => {
  //       setBannerType(event.target.value)
  //     }}
  //     value={businessField}
  //     variant="outlined"
  //   >
  //     <MenuItem value={'$'}>Kategori</MenuItem>
  //     <MenuItem value={'home'}>Homepage</MenuItem>
  //     <MenuItem value={'talk'}>Bincang Bisnis</MenuItem>
  //     <MenuItem value={'learning'}>Sentra Belajar</MenuItem>
  //     <MenuItem value={'umkm'}>Pasar UMKM</MenuItem>
  //     <MenuItem value={'clinic'}>Klinik Bisnis</MenuItem>
  //   </Select>
  // );
  
  const addContent = () => {
    setShowModal(true);
  };

  return (
    <div
      className={clsx(classes.root, className)}
    >
      <CardContent>
        <div className={classes.root}>
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              md={6}
              xs={12}
            >
              <Typography variant="h3">Room</Typography>
              <Typography
                style={{marginTop:'5px'}}
                variant="h6"
              >Atur konten Room untuk aplikasi mobile DiSentra</Typography>
            </Grid>
            <Hidden mdDown>
              <Grid
                item
                md={6}
                xs={12}
              >
                <Box
                  display="flex"
                  flexDirection="row-reverse"
                >
                  <Button
                    className={classes.leftToolbar}
                    color="primary"
                    onClick={addContent}
                    type="submit"
                    variant="contained"
                  >
                    <Typography variant="button">Tambah Konten</Typography>
                  </Button>
                  <div className={classes.leftToolbar}>
                    <Typography variant="h6">Tampilkan: </Typography>
                    {renderPerPage()}
                  </div>
                </Box>
              </Grid>
            </Hidden>
            <Hidden mdUp>
              <Grid
                item
                md={6}
                xs={12}
              >
                <div className={classes.leftToolbar}>
                  <Typography variant="h6">Tampilkan:</Typography>
                  {renderPerPage()}
                </div>
              </Grid>
            </Hidden>
            <Hidden mdUp>
              <Grid
                item
                md={6}
                xs={12}
              >
                <div className={classes.leftToolbar}>
                  <Typography variant="h6">Tampilkan:</Typography>
                  {renderPerPage()}
                </div>
              </Grid>
            </Hidden>
          </Grid>
        </div>
      </CardContent>
    </div>
  );
};

Header.propTypes = {
  businessField: PropTypes.string,
  className: PropTypes.string,
  dataPerPage: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  executeFilter: PropTypes.func,
  onSearch: PropTypes.func,
  resetFilter: PropTypes.func,
  // setBannerType: PropTypes.string,
  setDataPerPage: PropTypes.func,
  setShowModal: PropTypes.func,
  setStatus: PropTypes.func,
  status: PropTypes.string,
};

export default Header;
