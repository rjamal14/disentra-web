import React, { useState, useEffect, useRef, Fragment } from 'react';
import clsx from 'clsx';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import { useForm } from 'react-hook-form';
import {
  Typography,
  useMediaQuery,
  Button,
  CircularProgress,
  Link,
  FormControl
} from '@material-ui/core';
import { useTheme, withStyles } from '@material-ui/core/styles';
import useStyles from './data-detail-jss';
import { editRoom, refreshStateRoom } from 'actions';
import validation from '../../validation';
import { RoundedInput } from 'components';
import FieldDisplay from '../FieldDisplay';
import { RoundedSelect } from 'components';
import RadioGroup from '../RadioGroup';

const TextTriggerFile = withStyles(() => ({
  root: {
    position: 'relative',
    cursor: 'pointer',
    fontWeight: 900
  }
}))(Typography);

function DataDetail(props) {
  const {
    isEdit,
    setIsEdit,
    hideDetailModal,
    showDeleteConfirmation,
  } = props;
  const classes = useStyles();
  const theme = useTheme();
  const dispatch = useDispatch();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const { data: dataRoom, dataDetail, success, loading } = useSelector(state => state.room);
  const { register, handleSubmit, control, errors, watch } = useForm({
    defaultValues: {
      category_type: dataDetail?.banner_type !== 'home' ? 'menudisentra' : 'homepage'
    }
  });
  const [fileHandler, setFileHandler] = useState(null);
  const initialization = useRef(true);
  const [category, setCategory] = useState('homepage');

  useEffect(() => {
    if (initialization.current) {
      initialization.current = false;
    } else {
      if (success && !loading) {
        closeModal(false);
        dispatch(refreshStateRoom());
      }
    }
  }, [success, loading]);

  const closeModal = () => {
    hideDetailModal();
  };

  const switchToEdit = () => {
    setIsEdit(true);
  }; 

  const onSubmit = (data) => {
    dispatch(editRoom({
      payload: fileHandler ? Object.assign({}, data, { image: fileHandler }) : data,
      targetId: dataDetail.id
    }));
  };

  const setPhoto = (event) => {
    setFileHandler(event.target.files);
  };

  const showConfirmation = () => {
    const index = dataRoom.map(room => room.id).indexOf(dataDetail.id);
    showDeleteConfirmation(index);
  };

  return (
    <div className={clsx(classes.formContent, fullScreen && classes.fullWidth)}>
      <div className={classes.uploadContainer}>
        <Typography
          component="p"
          variant="body1"
        >
          Gambar*
        </Typography>
        <div className={clsx(classes.uploadArea, fullScreen && classes.fullWidthRel)}>
          {
            fileHandler ? (
              <img
                alt="content"
                className={classes.userImage}
                src={URL.createObjectURL(fileHandler[0])}
              />
            ) : (
              <img
                alt="content"
                className={classes.userImage}
                src={dataDetail?.image?.url}
              />
            )
          }
        </div>
        {
          isEdit && (
            <>
              <div className={classes.sizeCaption}>
                <Typography
                  component="p"
                  variant="caption"
                >
                  Foto (Maks. ukuran 2MB)
                </Typography>
              </div>
              <TextTriggerFile
                component="h5"
                varian="body1"
              >
                <input
                  accept="image/*"
                  className={classes.fileEmbed}
                  defaultValue={''}
                  name="image"
                  onChange={setPhoto}
                  ref={register()}
                  type="file"
                />
                Ubah Gambar
              </TextTriggerFile>
            </>
          )
        }
      </div>
      {
        isEdit ? (
          <RoundedInput 
            defaultValue={dataDetail.nama}
            errorMessage={errors?.nama}
            errors={errors.nama}
            label="Nama"
            name="nama"
            register={register(validation.nama)}
          />
        ) : (
          <FieldDisplay
            data={
              <Link
                color="secondary"
                href={dataDetail.nama}
                target="_blank"
                underline="always"
                variant="subtitle1"
              >
                {dataDetail.nama}
              </Link>
            }
            label="Nama"
          />
        )
      }
      {
        isEdit ? (
          <RoundedInput 
            defaultValue={dataDetail.description}
            errorMessage={errors?.description?.message}
            errors={errors.description}
            label="Deskripsi"
            multiline={10}
            name="description"
            register={register(validation.description)}
          />
        ) : (
          <div className={classes.extraBottom}>
            <FieldDisplay 
              data={dataDetail.description} 
              label="Deskripsi"
              multiline={20}
            />
          </div>
        )
      }
      {
        isEdit && (
          <Typography
            component="p"
            variant="body1"
          >
            * Tidak boleh kosong
          </Typography>
        )
      }
      {
        isEdit ? (
          <div className={classes.buttonAction}>
            <Button
              classes={{
                root: clsx(classes.buttonRounded, classes.expandButton)
              }}
              color="secondary"
              onClick={closeModal}
              variant="outlined"
            >
              Batal
            </Button>
            <Button
              classes={{
                root: classes.buttonRounded
              }}
              color="secondary"
              disabled={loading}
              endIcon={
                loading ? (
                  <CircularProgress
                    color="secondary"
                    size={20}
                  />
                ) : null
              }
              onClick={handleSubmit(onSubmit)}
              variant="contained"
            >
              Simpan
            </Button>
          </div>
        ) : (
          <div className={classes.buttonAction}>
            <Button
              classes={{
                root: clsx(classes.buttonRounded, classes.expandButton)
              }}
              onClick={showConfirmation}
            >
              Hapus
            </Button>
            <Button
              classes={{
                root: classes.buttonRounded
              }}
              color="secondary"
              onClick={switchToEdit}
              variant="contained"
            >
              Ubah
            </Button>
          </div>
        )
      }
    </div>
  )
}

DataDetail.propTypes = {
  hideDetailModal: PropTypes.func,
  isEdit: PropTypes.bool,
  setIsEdit: PropTypes.func,
  showChangeStatusConfirmation: PropTypes.func,
  showDeleteConfirmation: PropTypes.func,
};

export default DataDetail;