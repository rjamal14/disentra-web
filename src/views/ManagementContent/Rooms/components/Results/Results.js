import React, { /*useState*/ } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import useStyles from './result-jss';
import { withStyles } from '@material-ui/core/styles';
import {
  Card,
  CardActions,
  CardContent,
  // Checkbox,
  Divider,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
  Button,
} from '@material-ui/core';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
// import { TableEditBar } from 'components';
import { useDispatch, useSelector } from 'react-redux';
import { setRoomDataDetail } from 'actions';
import { setRoomDataFasilitas } from 'actions';

const CardFooter = withStyles(() => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    padding: '30px 0',
  }
}))(CardActions);


const Results = props => {
  const { 
    className, 
    onChangePage,
    setShowDataDetail, 
    setShowDataFasilitas,
    setIsEdit, 
    showDeleteConfirmation, 
  } = props;

  const classes = useStyles();
  const dispatch = useDispatch();

  const { data, pagination } = useSelector(state => state.room);

  const showDataDetailModal = (index, noEdit) => {
    if (!noEdit) {
      setIsEdit(true); 
    }
    dispatch(setRoomDataDetail({
      payload: data[index]
    }))
    setShowDataDetail(true);
  };

  const showDataFasilitasModal = (index, noEdit) => {
    if (!noEdit) {
      setIsEdit(true); 
    }
    dispatch(setRoomDataFasilitas({
      payload: data[index]
    }))
    setShowDataFasilitas(true);
  };

  return (
    <div
      className={clsx(classes.root, className)}
    >
      <Card>
        <Divider />
        <CardContent className={classes.content}>
          <PerfectScrollbar>
            <div className={classes.inner}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell padding="checkbox">
                    </TableCell>
                    <TableCell>Gambar</TableCell>
                    <TableCell>Nama Ruangan</TableCell>
                    <TableCell>Description</TableCell>
                    <TableCell>Aksi</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {data.map((content, index) => (
                    <TableRow
                      hover
                      key={index}
                      // selected={selectedCustomers.indexOf(content.id) !== -1}
                    >
                      <TableCell padding="checkbox">
                      </TableCell>
                      <TableCell>
                        <div className={classes.nameCell}>
                          <div>
                            <img   
                              alt="banner"
                              className={classes.banner}
                              onClick={() => showDataDetailModal(index, 'noEdit')} 
                              src={content?.image?.url} 
                            />
                          </div>
                        </div>
                      </TableCell>
                      <TableCell>
                        {content.nama}
                      </TableCell>
                      <TableCell>
                        {content.description}
                      </TableCell>
                      <TableCell>
                        <div className={classes.actionWrapper}>
                          <img 
                            alt="edit"
                            onClick={() => showDataDetailModal(index, 'noEdit')}
                            src="/svg/pencil.svg" 
                          />
                          <img 
                            alt="fasilitas"
                            onClick={() => showDataFasilitasModal(index, 'noEdit')}
                            src="/svg/x.svg" 
                          />
                          <img 
                            alt="delete"
                            onClick={() => showDeleteConfirmation(index)}
                            src="/svg/trash.svg" 
                          />
                        </div>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </div>
          </PerfectScrollbar>
        </CardContent>
        <CardFooter className={classes.actions}>
          {
            /* eslint-disable-next-line no-extra-boolean-cast */
            Boolean(data?.length) ? (
              <>
                <div className={classes.paginationButton}>
                  <Button
                    color="secondary"
                    onClick={() => onChangePage('prev')}
                    variant="contained"
                  >
                    <ChevronLeftIcon />
                  </Button>
                  <Typography
                    component="h1"
                    variant="subtitle1"
                  >
                    {pagination?.current_page || 1}
                  </Typography>
                  <Button
                    color="secondary"
                    onClick={() => onChangePage('next')}
                    variant="contained"
                  >
                    <ChevronRightIcon />
                  </Button>
                </div>
                <div className={classes.paginationInfo}>
                  <Typography
                    color="secondary"
                  >
                    Halaman {pagination?.current_page || 1} dari {pagination?.total_pages || 1}
                  </Typography>
                </div>
              </>
            ) : (
              <Typography
                component="h2"
                varian="body1"
              >
                Data Tidak Ditemukan
              </Typography>
            )
          }
        </CardFooter>
      </Card>
      {/* <TableEditBar selected={selectedCustomers} /> */}
    </div>
  );
};

Results.propTypes = {
  className: PropTypes.string,
  onChangePage: PropTypes.func,
  setIsEdit: PropTypes.func,
  setShowDataDetail: PropTypes.func,
  setShowDataFasilitas: PropTypes.func,
  showChangeStatusConfirmation: PropTypes.func,
  showDeleteConfirmation: PropTypes.func,
};

Results.defaultProps = {
  contents: []
};

export default Results;