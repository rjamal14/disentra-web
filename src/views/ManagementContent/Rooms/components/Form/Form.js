import React, { useState, useRef, useEffect } from 'react'
import { useForm } from 'react-hook-form';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {
  FormControl,
  Typography,
  useMediaQuery,
  Button,
  CircularProgress,
} from '@material-ui/core';
import { useTheme, withStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import { addRoom, refreshStateBanner } from 'actions';
import validation from '../../validation';
import useStyles from './form-jss';
import palette from 'theme/palette';
import { RoundedInput } from 'components';
import RadioGroup from '../RadioGroup';
import { RoundedSelect } from 'components';
import { Fragment } from 'react';


function Form(props) {
  const { toggleModal } = props;
  const classes = useStyles();
  const dispatch = useDispatch();
  const { success, error, loading } = useSelector(state => state.room);
  const theme = useTheme();
  const { register, handleSubmit, control, errors, watch } = useForm();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const [fileHandler, setFileHandler] = useState(null);
  const initialization = useRef(true);
  // const [category, setCategory] = useState('homepage');

  const PhotoErrorText = withStyles(() => ({
    root: {
      color: palette.error.main,
      marginBottom: 10,
      marginLeft: 10,
      marginTop: 10,
    }
  }))(Typography);

  // useEffect(() => {
  //   setCategory(watch('category_type'))
  // }, [watch('category_type')]);

  // useEffect(() => {
  //   dispatch(refreshStateBanner());
  // }, []);

  const onSubmit = (data) => {
    dispatch(addRoom({
      payload: data
    }));
  }; 

  const setImage = (event) => {
    setFileHandler(event.target.files);
  };

  const closeModal = () => {
    toggleModal(false);
  };

  useEffect(() => {
    if (initialization.current) {
      initialization.current = false;
    } else {
      if (success && !loading) {
        toggleModal(false);
        dispatch(refreshStateBanner());
      }
      if (error) {
        dispatch(refreshStateBanner());
      }
    }
  }, [success, error, loading]);
  return (
    <div className={clsx(classes.formContent, fullScreen && classes.fullWidth)}>
      <div className={classes.uploadContainer}>
        <Typography
          component="p"
          variant="body1"
        >
          Gambar*
        </Typography>
        <div className={clsx(classes.uploadArea, fullScreen && classes.fullWidthRel)}>
          <div className={classes.uploadInfo}>
            {
              fileHandler &&
              <img
                alt="user"
                className={classes.userImage}
                src={URL.createObjectURL(fileHandler[0])}
              />
            }
            <img
              alt="camera"
              src="/svg/camera.svg"
            />
            <Typography
              component="p"
              variant="caption"
            >
              Tambah Foto (Maks. ukuran 2MB)
            </Typography>
          </div>
          <input
            accept="image/*"
            className={classes.file}
            name="image"
            onChange={setImage}
            ref={register(validation.image)}
            type="file"
          />
        </div>
        {
          errors.image &&
          <PhotoErrorText
            component="p"
            variant="caption"
          >
            {errors.image.message}
          </PhotoErrorText>
        }
      </div>
      <Typography
        component="p"
        variant="body1"
      >
        * Tidak boleh kosong
      </Typography>

      <RoundedInput 
          defaultValue=""
          errorMessage={errors?.link?.message}
          errors={errors.link}
          label="Nama"
          name="nama"
          register={register(validation.nama)}
        />

      <RoundedInput 
          defaultValue=""
          errorMessage={errors?.description?.message}
          errors={errors.description}
          label="Deskripsi"
          multiline={13}
          name="description"
          register={register(validation.description)}
        />
      <div className={classes.buttonAction}>
        <Button
          classes={{
            root: clsx(classes.buttonRounded, classes.expandButton)
          }}
          color="secondary"
          onClick={closeModal}
          variant="outlined"
        >
          Batal
        </Button>
        <Button
          classes={{
            root: classes.buttonRounded
          }}
          color="secondary"
          disabled={loading}
          endIcon={
            loading ? (
              <CircularProgress
                color="secondary"
                size={20}
              />
            ) : null
          }
          onClick={handleSubmit(onSubmit)}
          variant="contained"
        >
          Simpan
        </Button>
      </div>
    </div>
  )
}

Form.propTypes = {
  toggleModal: PropTypes.func
};

export default Form;