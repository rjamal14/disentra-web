/*eslint-disable react/jsx-boolean-value*/
import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';

import { Page, Breadcrumb, Modal } from 'components';
import { Header, Results, Form, DataDetail, DataFasilitas } from './components';
import { Card } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { getRoom, deleteRoom } from 'actions';
import swal from 'sweetalert';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  results: {
    marginTop: theme.spacing(3)
  },
  breadcrumb:{
    margin: theme.spacing(3),
    marginBottom: -theme.spacing(6),
  }
}));

const path = [
  ['Manajemen Konten', '/management-content/room'],
  ['Room', '/management-content/room']
];
const RoomManagementList = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  
  const [showModal, setShowModal] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [isFasilitasEdit, setIsEditFasilitas] = useState(false);
  const [showDataDetail, setShowDataDetail] = useState(false);
  const [showDataFasilitas, setShowDataFasilitas] = useState(false);
  const [dataPerPage, setDataPerPage] = useState(10);
  const [status_eq, setStatus] = useState('$');
  const { data, pagination } = useSelector(state => state.room);

  useEffect(() => {
    dispatch(getRoom({
      payload: {
        dataPerPage
      }
    }));
  }, []);

  useEffect(() => {
    dispatch(getRoom({
      payload: {
        dataPerPage
      }
    })); 
  }, [dataPerPage]);

  const refetchContent = (perPage, additionalFilter) => {
    dispatch(getRoom({
      payload: {
        dataPerPage: perPage || dataPerPage,
        params: additionalFilter,
      }
    }))
  };

  const showDeleteConfirmation = async (index) => {
    const confirmation = await swal({
      title: 'Hapus Room',
      text: 'Setelah dihapus, Anda tidak akan dapat memulihkan data ini!',
      icon: 'warning',
      buttons: true,
      dangerMode: true,
    })
    if (confirmation) {
      deleteExecution(index);
    }
  };

  const deleteExecution = (index) => {
    dispatch(deleteRoom({
      payload: {
        id: data[index].id
      }
    }))
  };

  const hideDetailModal = () => {
    setShowDataDetail(false);
    setIsEdit(false);
  };

  const hideFasilitasModal = () => {
    setShowDataFasilitas(false);
    setIsEditFasilitas(false);
  };

  const onChangePage = (direction) => {
    let currentPage = pagination?.current_page ?? 1;
    const totalPages = pagination?.total_pages ?? 1;
    if (direction === 'next') {
      if (currentPage < totalPages) {
        dispatch(getRoom({
          payload: {
            dataPerPage,
            page: pagination?.current_page + 1 ?? 1
          }
        }))
      }
    } else {
      if (currentPage > 1) {
        dispatch(getRoom({
          payload: {
            dataPerPage,
            page: pagination?.current_page - 1 ?? 1
          }
        }))
      }
    }
  }

  const executeFilter = () => {
    refetchContent(null, {
      status_eq,
    })
  };

  const resetFilter = () => {
    setStatus('$');
    // setBannerType('$');
    refetchContent()
  }

  return (
    <Page
      className={classes.root}
      title="BJB DiSentra - Room"
    >
      <Modal 
        children={<Form toggleModal={setShowModal} />}
        isOpen={showModal}
        title="Tambah Room"
        toggleModal={setShowModal}
      />
      <Modal
        children={
          <DataDetail 
            hideDetailModal={hideDetailModal}
            isEdit={isEdit}
            maxWidth="lg"
            setIsEdit={setIsEdit}
            showDeleteConfirmation={showDeleteConfirmation}
          />
        }
        hideDetailModal={hideDetailModal}
        isOpen={showDataFasilitas}
        title="Ubah Room"
        toggleModal={setShowDataFasilitas}
      />

      <Modal
        children={
          <DataFasilitas 
          hideFasilitasModal={hideFasilitasModal}
          isFasilitasEdit={isFasilitasEdit}
            maxWidth="lg"
            setIsEditFasilitas={setIsEditFasilitas}
            showDeleteConfirmation={showDeleteConfirmation}
          />
        }
        hideFasilitasModal={hideFasilitasModal}
        isOpen={showDataFasilitas}
        title="Fasilitas Room"
        toggleModal={setShowDataFasilitas}
      />
      <Card>
        <div className={classes.breadcrumb}>
          <Breadcrumb route={path} />
        </div>
        <Header 
          // businessField={banner_type}
          dataPerPage={dataPerPage}
          // executeFilter={executeFilter}
          // resetFilter={resetFilter}
          // setBannerType={setBannerType}
          setDataPerPage={setDataPerPage}
          setShowModal={setShowModal} 
          setStatus={setStatus}
          showModal={showModal}
          status={status_eq}

        />
        <Results
          className={classes.results}
          onChangePage={onChangePage}
          setIsEdit={setIsEdit}
          setShowDataDetail={setShowDataDetail}
          setShowDataFasilitas={setShowDataFasilitas}
          showDeleteConfirmation={showDeleteConfirmation}
        />
      </Card>
    </Page>
  );
};

export default RoomManagementList;