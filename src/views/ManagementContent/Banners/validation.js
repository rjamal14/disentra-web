const defaultMessage = 'Tidak boleh kosong';

const validation = {
  image: {
    required: defaultMessage
  },
  status: {
    required: defaultMessage
  },
}

export default validation;