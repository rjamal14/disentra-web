/*eslint-disable react/jsx-boolean-value*/
import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';

import { Page, Breadcrumb, Modal } from 'components';
import { Header, Results, Form, DataDetail } from './components';
import { Card } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { getMarket, deleteMarket, changeStatusMarket, getAllBusinessField } from 'actions';
import swal from 'sweetalert';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(5, 4)
  },
  results: {
    marginTop: theme.spacing(3)
  },
  breadcrumb:{
    margin: theme.spacing(3),
    marginBottom: -theme.spacing(6),
  }
}));

const path = [
  ['Manajemen Konten', '/management-content/umkm-market'],
  ['Pasar UMKM', '/management-content/umkm-market']
];

const MarketList = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const [name_cont, setName] = useState('');
  const [business_field_id_eq, setBusinessType] = useState('$');
  
  const [showModal, setShowModal] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [showDataDetail, setShowDataDetail] = useState(false);
  const [dataPerPage, setDataPerPage] = useState(10);
  const { data, pagination } = useSelector(state => state.market);

  useEffect(() => {
    dispatch(getMarket({
      payload: {
        dataPerPage
      }
    }));
    dispatch(getAllBusinessField());
  }, []);

  useEffect(() => {
    refetchContent(dataPerPage, {
      business_field_id_eq,
      name_cont,
    })
  }, [dataPerPage]);

  const refetchContent = (perPage, additionalFilter) => {
    dispatch(getMarket({
      payload: {
        dataPerPage: perPage || dataPerPage,
        params: additionalFilter,
      }
    }))
  };

  const showDeleteConfirmation = async (index) => {
    const confirmation = await swal({
      title: 'Hapus Konten',
      text: 'Setelah dihapus, Anda tidak akan dapat memulihkan data ini!',
      icon: 'warning',
      buttons: true,
      dangerMode: true,
    })
    if (confirmation) {
      deleteExecution(index);
    }
  };

  const deleteExecution = (index) => {
    dispatch(deleteMarket({
      payload: {
        id: data[index].id
      }
    }))
  };

  const showChangeStatusConfirmation = async (index, status) => {
    const confirmation = await swal({
      title: status === 'show' ? 'Perlihatkan Konten' : 'Sembunyikan Konten',
      text: `Apakah Anda yakin akan ${status === 'show' ? 'memperlihatkan' : 'menyembunyikan'} konten ini?`,
      icon: 'warning',
      buttons: true,
      dangerMode: true,
    })
    if (confirmation) {
      suspendExecution(index, status);
    }
  };

  const suspendExecution = (index, status) => {
    dispatch(changeStatusMarket({
      payload: {
        id: data[index].id,
        status
      }
    }))
  };

  const hideDetailModal = () => {
    setShowDataDetail(false);
    setIsEdit(false);
  };

  const onChangePage = (direction) => {
    let currentPage = pagination?.current_page ?? 1;
    const totalPages = pagination?.total_pages ?? 1;
    if (direction === 'next') {
      if (currentPage < totalPages) {
        dispatch(getMarket({
          payload: {
            dataPerPage,
            page: pagination?.current_page + 1 ?? 1
          }
        }))
      }
    } else {
      if (currentPage > 1) {
        dispatch(getMarket({
          payload: {
            dataPerPage,
            page: pagination?.current_page - 1 ?? 1
          }
        }))
      }
    }
  }

  const executeFilter = () => {
    refetchContent(null, {
      business_field_id_eq,
      name_cont,
    })
  };

  const resetFilter = () => {
    setBusinessType('$');
    setName('');
    refetchContent(dataPerPage)
  };
  return (
    <Page
      className={classes.root}
      title="BJB DiSentra - Market UMKM"
    >
      <Modal 
        children={<Form toggleModal={setShowModal} />}
        fullWidth={true}
        isOpen={showModal}
        maxWidth="lg"
        title="Tambah Pasar UMKM"
        toggleModal={setShowModal}
      />
      <Modal
        children={
          <DataDetail 
            hideDetailModal={hideDetailModal}
            isEdit={isEdit}
            setIsEdit={setIsEdit}
            showChangeStatusConfirmation={showChangeStatusConfirmation}
            showDeleteConfirmation={showDeleteConfirmation}
          />
        }
        fullWidth={true}
        hideDetailModal={hideDetailModal}
        isOpen={showDataDetail}
        maxWidth="lg"
        title="Detail Pasar UMKM"
        toggleModal={setShowDataDetail}
      />
      <Card>
        <div className={classes.breadcrumb}>
          <Breadcrumb route={path} />
        </div>
        <Header 
          businessName={name_cont}
          businessType={business_field_id_eq}
          dataPerPage={dataPerPage}
          executeFilter={executeFilter}
          resetFilter={resetFilter}
          setBusinessType={setBusinessType}
          setDataPerPage={setDataPerPage}
          setName={setName}
          setShowModal={setShowModal} 
          showModal={showModal}
        />
        <Results
          className={classes.results}
          onChangePage={onChangePage}
          setIsEdit={setIsEdit}
          setShowDataDetail={setShowDataDetail}
          showChangeStatusConfirmation={showChangeStatusConfirmation}
          showDeleteConfirmation={showDeleteConfirmation}
        />
      </Card>
    </Page>
  );
};

export default MarketList;