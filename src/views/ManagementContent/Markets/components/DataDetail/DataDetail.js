/* eslint-disable no-extra-boolean-cast */
/* eslint-disable react/jsx-boolean-value */
import React, { useState, useEffect ,useRef } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import { useForm } from 'react-hook-form';
import axios from 'axios';
import { 
  Typography, 
  useMediaQuery,
  Button,
  CircularProgress,
  FormControl,
  Box,
  Grid,
  ClickAwayListener,
} from '@material-ui/core';
import { GoogleApiWrapper } from 'google-maps-react';
import env from 'config/env';
import { useTheme, withStyles } from '@material-ui/core/styles';
import useStyles from './data-detail-jss';
import palette from 'theme/palette';
import { editMarket, refreshStateMarket, getCities } from 'actions';
import validation from '../../validation';
import { RoundedInput } from 'components';
import { RoundedSelect } from 'components';
import FieldDisplay from '../FieldDisplay';
import UploadBox from '../UploadBox';
import { googlePlace, googlePlaceDetail } from 'services/googleapis';
import { debounce } from 'throttle-debounce';
import Map from './Map';

const PhotoErrorText = withStyles(() => ({
  root: {
    color: palette.error.main,
    marginBottom: 10,
    marginLeft: 10,
    marginTop: 10
  }
}))(Typography);

const TextTriggerFile = withStyles(() => ({
  root: {
    position: 'relative',
    cursor: 'pointer',
    fontWeight: 900,
    marginTop: -10,
    marginBottom: 20,
  }
}))(Typography);

function DataDetail(props) {
  const triggerThumbnail = useRef(null);
  const triggerImage1 = useRef(null);
  const triggerImage2 = useRef(null);
  const triggerImage3 = useRef(null);
  const triggerImage4 = useRef(null);
  const { 
    isEdit, 
    setIsEdit, 
    hideDetailModal, 
    showDeleteConfirmation,
    google,
  } = props;
  const classes = useStyles();
  const theme = useTheme();
  const dispatch = useDispatch();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const { data: dataMarket, dataDetail, success, loading } = useSelector(state => state.market);
  const { data: dataBusinessField } = useSelector(state => state.businessField);
  const { data: dataCities } = useSelector(state => state.cities);
  const { register, handleSubmit, control, errors, watch, setValue, clearErrors } = useForm();
  const [imageDisplay, setImageDisplay] = useState([])
  const [thumbnailDisplay, setThumbnailDisplay] = useState([]);
  const [thumbnail, setThumbnail] = useState(null);
  const [fileHandler1, setFileHandler1] = useState(null);
  const [fileHandler2, setFileHandler2] = useState(null);
  const [fileHandler3, setFileHandler3] = useState(null);
  const [fileHandler4, setFileHandler4] = useState(null);
  const [showSuggestion, setShowSuggestion] = useState(false);
  const [marker, setMarker] = useState([]);
  const [expandMap, setExpandMap] = useState(false);
  const [suggestedLocation, setSuggestedLocation] = useState([]);
  const [value, setValueX] = useState('');
  const [centerMap, setCenterMap] = useState(null);
  const initialization = useRef(true);

  useEffect(() => {
    if (initialization.current) {
      initialization.current = false;
    } else {
      if (success && !loading) {
        closeModal(false);
        dispatch(refreshStateMarket());
      }
    }
  }, [success, loading]);

  useEffect(() => {
    setThumbnailDisplay(dataDetail.images.filter(img => img.is_thumbnail));
    setImageDisplay(dataDetail.images.filter(img => !img.is_thumbnail));
    setDefaultMapProps();
  }, []);
  
  const setDefaultMapProps = () => {
    setMarker([
      {
        position: {
          lat: dataDetail.latitude || 0, 
          lng: dataDetail.longitude || 0
        },
        name: 'Lokasi Pasar UMKM'
      }
    ]);
    setCenterMap({ lat: dataDetail.latitude, lng: dataDetail.longitude });
  };

  const closeModal = () => {
    hideDetailModal();
  };

  const switchToEdit = () => {
    setIsEdit(true);
  };

  const onSubmit = (data) => {
    let modifiedData = Object.assign({}, data, { image: []});
    thumbnail?.length && (modifiedData.thumbnail = thumbnail);
    fileHandler1?.length && modifiedData.image.push(fileHandler1);
    fileHandler2?.length && modifiedData.image.push(fileHandler2);
    fileHandler3?.length && modifiedData.image.push(fileHandler3);
    fileHandler4?.length && modifiedData.image.push(fileHandler4);
    modifiedData.latitude = marker[0].position.lat;
    modifiedData.longitude = marker[0].position.lng;
    dispatch(editMarket({ 
      payload: modifiedData ,
      targetId: dataDetail.id
    }));
  };

  const showConfirmation = () => {
    const index = dataMarket.map(user => user.id).indexOf(dataDetail.id);
    showDeleteConfirmation(index);
  };

  const triggerInput = (type) => {
    eval(type).current.click();
  };

  useEffect(() => {
    if (value) {
      getPlaceDetail(value.place_id);
    }
  }, [value])

  const onChangeSearch = (event) => {
    if (event) {
      event.persist()
      executeDebounce(event);
    }
  };

  /* eslint-disable-next-line no-unused-vars */
  const relocateMap = (lat, lng) => {
    setCenterMap({lat, lng});
  };

  const executeDebounce = debounce(500, (e) => {
    getPlaceSuggestion(e.target.value);
  });

  /* eslint-disable-next-line no-unused-vars */
  const addMarker = (mapProps, map, clickEvent) => {
    setMarker([{
      position: {
        lat: clickEvent.latLng.lat(), 
        lng: clickEvent.latLng.lng()
      },
      name: 'Lokasi Pasar UMKM'
    }]);
    setValue('latitude', clickEvent.latLng.lat());
    setValue('longitude', clickEvent.latLng.lng());
  };

  const getPlaceSuggestion = async (query) => {
    const config = {
      method: 'GET',
      url: `${googlePlace}${query}&key=${env.google_maps_apikey}`
    }
    try {
      const response = await axios(config);
      setSuggestedLocation(response.data.predictions);
    } catch(error) {
      console.log(error);
    }
  };

  const getPlaceDetail = async (place_id) => {
    const config = {
      method: 'GET',
      url: `${googlePlaceDetail}${place_id}&key=${env.google_maps_apikey}`
    }
    try {
      const response = await axios(config);
      const { location } = response.data.result.geometry;
      relocateMap(location.lat, location.lng)
      setValue('latitude', location.lat);
      setValue('longitude', location.lng);
      setMarker([{
        position: {
          lat: location.lat,
          lng: location.lng
        },
        name: 'Lokasi Pasar UMKM'
      }])
    } catch(error) {
      console.log(error);
    }
  }

  const onCloseMap = () => {
    setExpandMap(false)
    if (marker.length) {
      setCenterMap({lat: marker[0].position.lat, lng: marker[0].position.lng});
      clearErrors('latitude');
      clearErrors('longitude');
    }
  }

  const onChangeQuerySearch = (e) => {
    e.persist();
    if (e.target.value.length > 2) {
      setShowSuggestion(true);
      searchCity(e.target.value);
    }
  }

  const searchCity = debounce(500, (query) => {
    dispatch(getCities({ query }));
  });

  const onSelectedCity = (index) => {
    setValue('city_id', dataCities[index].id);
    setShowSuggestion(false);
    clearErrors('city_id');
    document.getElementById('linked-city').value = dataCities[index].name;
  };

  return(
    <div className={clsx(classes.root, !fullScreen && classes.desktop)}>
      <Box>
        {
          isEdit ? (
            <>
              <UploadBox 
                disableInput={true}
                errors={errors.thumbnail}
                fileHandler={thumbnail?.length ? thumbnail : thumbnailDisplay[0]?.image?.url}
                forceUrl={!Boolean(thumbnail?.length)}
                fullScreen={fullScreen}
                label="Thumbnail"
                name="thumbnail"
                noBorder={true}
                nodeErrorText={
                  <PhotoErrorText
                    classes={classes.photoErrorText}
                    component="p"
                    variant="caption"
                  >
                    {errors?.thumbnail?.message}
                  </PhotoErrorText>
                }
              />
              <input 
                accept="image/*"
                hidden
                name="thumbnail"
                onChange={(e) => setThumbnail(e.target.files)}
                ref={triggerThumbnail}
                type="file"
              />
              <div className={classes.sizeCaption}>
                <Typography
                  component="p"
                  variant="caption"
                >
                  Foto (Maks. ukuran 2MB)
                </Typography>
              </div>
              <TextTriggerFile
                component="p"
                onClick={() => triggerInput('triggerThumbnail')}
                variant="body1"
              >
                Ubah Gambar
              </TextTriggerFile>
            </>
          ) : (
            <UploadBox 
              disableInput={true}
              errors={errors.thumbnail}
              fileHandler={thumbnailDisplay[0]?.image?.url}
              forceUrl={true}
              fullScreen={fullScreen}
              label="Thumbnail"
              name="thumbnail"
              noBorder={true}
              nodeErrorText={<div />}
            />
          )
        }
        {
          isEdit ? (
            <>
              <Box
                alignItems="center"
                display="flex"
                justifyContent="space-between"
                width="16vw"
              >
                {
                  imageDisplay[0] ? (
                    <Box>
                      <UploadBox 
                        disableInput={true}
                        errors={{}}
                        fileHandler={fileHandler1?.length ? fileHandler1 : imageDisplay[0]?.image?.url}
                        forceUrl={!Boolean(fileHandler1?.length)}
                        fullScreen={fullScreen}
                        label="Thumbnail"
                        name="thumbnail"
                        noBorder={true}
                        nodeErrorText={<div />}
                      />
                      <input 
                        accept="image/*"
                        hidden
                        name="thumbnail"
                        onChange={(e) => setFileHandler1(e.target.files)}
                        ref={triggerImage1}
                        type="file"
                      />
                      <div className={classes.sizeCaption}>
                        <Typography
                          component="p"
                          variant="caption"
                        >
                          Foto (Maks. ukuran 2MB)
                        </Typography>
                      </div>
                      <TextTriggerFile
                        component="p"
                        onClick={() => triggerInput('triggerImage1')}
                        variant="body1"
                      >
                        Ubah Gambar
                      </TextTriggerFile>
                    </Box>
                  ) : (
                    <div className={classes.ctSize}>
                      <Box>
                        <UploadBox 
                          errors={{}}
                          fileHandler={fileHandler1}
                          fullScreen={fullScreen}
                          label="&nbsp;"
                          name="thumbnail"
                          nodeErrorText={<div />}
                          setPhoto={(e) => setFileHandler1(e.target.files)}
                        />
                        <TextTriggerFile
                          component="p"
                          onClick={() => triggerInput('triggerImage1')}
                          variant="body1"
                        >
                          &nbsp;
                        </TextTriggerFile>
                      </Box>
                      <div className={classes.sizeCaptionEmpty}>
                        <Typography
                          component="p"
                          variant="caption"
                        >
                          Foto (Maks. ukuran 2MB)
                        </Typography>
                      </div>
                    </div>
                  )
                }
                {
                  imageDisplay[1] ? (
                    <Box>
                      <UploadBox 
                        disableInput={true}
                        errors={{}}
                        fileHandler={fileHandler2?.length ? fileHandler2 : imageDisplay[1]?.image?.url}
                        forceUrl={!Boolean(fileHandler2?.length)}
                        fullScreen={fullScreen}
                        label="&nbsp;"
                        name="thumbnail"
                        noBorder={true}
                        nodeErrorText={<div />}
                      />
                      <input 
                        accept="image/*"
                        hidden
                        name="thumbnail"
                        onChange={(e) => setFileHandler2(e.target.files)}
                        ref={triggerImage2}
                        type="file"
                      />
                      <div className={classes.sizeCaption}>
                        <Typography
                          component="p"
                          variant="caption"
                        >
                          Foto (Maks. ukuran 2MB)
                        </Typography>
                      </div>
                      <TextTriggerFile
                        component="p"
                        onClick={() => triggerInput('triggerImage2')}
                        variant="body1"
                      >
                        Ubah Gambar
                      </TextTriggerFile>
                    </Box>
                  ) : (
                    <div className={classes.ctSize}>
                      <Box
                        marginTop={imageDisplay[0] ? '-25px' : '0'}
                      >
                        <UploadBox 
                          errors={{}}
                          fileHandler={fileHandler2}
                          fullScreen={fullScreen}
                          label="&nbsp;"
                          name="thumbnail"
                          nodeErrorText={<div />}
                          setPhoto={(e) => setFileHandler2(e.target.files)}
                        />
                        <TextTriggerFile
                          component="p"
                          onClick={() => triggerInput('triggerImage2')}
                          variant="body1"
                        >
                          &nbsp;
                        </TextTriggerFile>
                      </Box>
                      <div className={classes.sizeCaptionEmpty}>
                        <Typography
                          component="p"
                          variant="caption"
                        >
                          Foto (Maks. ukuran 2MB)
                        </Typography>
                      </div>
                    </div>
                  )
                }
              </Box>
              <Box
                alignItems="center"
                display="flex"
                justifyContent="space-between"
                width="16vw"
              >
                {
                  imageDisplay[2] ? (
                    <Box>
                      <UploadBox 
                        disableInput={true}
                        errors={{}}
                        fileHandler={fileHandler3?.length ? fileHandler3 : imageDisplay[2]?.image?.url}
                        forceUrl={!Boolean(fileHandler3?.length)}
                        fullScreen={fullScreen}
                        label="&nbsp;"
                        name="thumbnail"
                        noBorder={true}
                        nodeErrorText={<div />}
                      />
                      <input 
                        accept="image/*"
                        hidden
                        name="thumbnail"
                        onChange={(e) => setFileHandler3(e.target.files)}
                        ref={triggerImage3}
                        type="file"
                      />
                      <div className={classes.sizeCaption}>
                        <Typography
                          component="p"
                          variant="caption"
                        >
                          Foto (Maks. ukuran 2MB)
                        </Typography>
                      </div>
                      <TextTriggerFile
                        component="p"
                        onClick={() => triggerInput('triggerImage3')}
                        variant="body1"
                      >
                        Ubah Gambar
                      </TextTriggerFile>
                    </Box>
                  ) : (
                    <div className={classes.ctSize}>
                      <Box>
                        <UploadBox 
                          errors={{}}
                          fileHandler={fileHandler3}
                          fullScreen={fullScreen}
                          label="&nbsp;"
                          name="thumbnail"
                          nodeErrorText={<div />}
                          setPhoto={(e) => setFileHandler3(e.target.files)}
                        />
                        <TextTriggerFile
                          component="p"
                          onClick={() => triggerInput('triggerImage3')}
                          variant="body1"
                        >
                          &nbsp;
                        </TextTriggerFile>
                      </Box>
                      <div className={classes.sizeCaptionEmpty}>
                        <Typography
                          component="p"
                          variant="caption"
                        >
                          Foto (Maks. ukuran 2MB)
                        </Typography>
                      </div>
                    </div>
                  )
                }
                {
                  imageDisplay[3] ? (
                    <Box>
                      <UploadBox 
                        disableInput={true}
                        errors={{}}
                        fileHandler={fileHandler4?.length ? fileHandler4 : imageDisplay[3]?.image?.url}
                        forceUrl={!Boolean(fileHandler4?.length)}
                        fullScreen={fullScreen}
                        label="&nbsp;"
                        name="thumbnail"
                        noBorder={true}
                        nodeErrorText={<div />}
                      />
                      <input 
                        accept="image/*"
                        hidden
                        name="thumbnail"
                        onChange={(e) => setFileHandler4(e.target.files)}
                        ref={triggerImage4}
                        type="file"
                      />
                      <div className={classes.sizeCaption}>
                        <Typography
                          component="p"
                          variant="caption"
                        >
                          Foto (Maks. ukuran 2MB)
                        </Typography>
                      </div>
                      <TextTriggerFile
                        component="p"
                        onClick={() => triggerInput('triggerImage4')}
                        variant="body1"
                      >
                        Ubah Gambar
                      </TextTriggerFile>
                    </Box>
                  ) : (
                    <div className={classes.ctSize}>
                      <Box
                        marginTop={imageDisplay[2] ? '-25px' : '0'}
                      >
                        <UploadBox 
                          errors={{}}
                          fileHandler={fileHandler4}
                          fullScreen={fullScreen}
                          label="&nbsp;"
                          name="thumbnail"
                          nodeErrorText={<div />}
                          setPhoto={(e) => setFileHandler4(e.target.files)}
                        />
                        <TextTriggerFile
                          component="p"
                          onClick={() => triggerInput('triggerImage4')}
                          variant="body1"
                        >
                          &nbsp;
                        </TextTriggerFile>
                      </Box>
                      <div className={classes.sizeCaptionEmpty}>
                        <Typography
                          component="p"
                          variant="caption"
                        >
                          Foto (Maks. ukuran 2MB)
                        </Typography>
                      </div>
                    </div>
                  )
                }
              </Box>
            </>
          ) : (
            <>
              <Typography
                component="p"
                variant="body1"
              >
                Gambar Tambahan
              </Typography>
              <Grid
                container
                spacing={1}
              >
                {
                  imageDisplay?.map((img, index) => (
                    <Grid 
                      item
                      key={index} 
                    >
                      <img
                        alt="photo"
                        className={classes.gridPhoto}
                        key={index}
                        src={img.image.url}
                      />
                    </Grid>
                  ))
                }
              </Grid>
            </>
          )
        }
      </Box>
      <div className={clsx(classes.formContent, fullScreen && classes.fullWidth)}>
        {
          isEdit ? (
            <RoundedInput 
              defaultValue={dataDetail.name}
              errorMessage={errors?.name?.message}
              errors={errors.name}
              label="Nama Usaha"
              name="name"
              register={register(validation.name)}
            />
          ) : (
            <FieldDisplay 
              data={dataDetail.name} 
              label="Nama Usaha"
            />
          )
        }
        {
          isEdit ? (
            <div className={classes.selectWrapper}>
              <FormControl fullWidth>
                <RoundedSelect
                  control={control}
                  defaultValue={dataDetail.business_field?.id}
                  errorMessage={errors?.business_field?.message}
                  errors={errors.business_field}
                  label="Kategori"
                  name="business_field_id"
                  options={dataBusinessField}
                  validation={validation.business_field}
                />
              </FormControl>
            </div>
          ) : (
            <FieldDisplay 
              data={dataDetail?.business_field?.name} 
              label="Kategori"
            />
          )
        }
        {
          isEdit ? (
            <div className={classes.selectWrapper}>
              <FormControl fullWidth>
                <RoundedInput
                  defaultValue={dataDetail?.city?.name}
                  errorMessage={errors?.city_id?.message}
                  errors={errors.city_id}
                  id="linked-city"
                  label="Asal Kota/Kab."
                  onChange={onChangeQuerySearch}
                  placeholder="Cari nama kota.."
                  type="text"
                  validation={validation.city_id}
                />
                <input
                  defaultValue={dataDetail?.city?.id}
                  name="city_id"
                  ref={register(validation.city_id)} 
                  type="hidden"
                />
                {
                  showSuggestion &&
                  <ClickAwayListener onClickAway={() => setShowSuggestion(false)}>
                    <div className={classes.suggestionBox}>
                      {
                        dataCities.map((city, index) => (
                          <div 
                            className={classes.suggestionList}
                            key={index} 
                            onClick={() => onSelectedCity(index)}
                          >
                            {city.name}
                          </div>
                        ))
                      }
                    </div>
                  </ClickAwayListener>
                }
              </FormControl>
            </div>
          ) : (
            <FieldDisplay 
              data={dataDetail?.city?.name} 
              label="Asal Kota/Kab."
            />
          )
        }
        {
          isEdit ? (
            <RoundedInput 
              defaultValue={dataDetail.address}
              errorMessage={errors?.address?.message}
              errors={errors.address}
              label="Alamat"
              multiline={3}
              name="address"
              register={register(validation.address)}
            />
          ) : (
            <FieldDisplay 
              data={dataDetail.address} 
              label="Alamat"
              multiline={3}
            />
          )
        }
        <Map
          addMarker={addMarker}
          centerMap={centerMap}
          expandMap={expandMap}
          google={google}
          hideButton={!isEdit}
          marker={marker}
          onChangeSearch={onChangeSearch}
          onCloseMap={onCloseMap}
          setExpandMap={setExpandMap}
          setValueX={setValueX}
          suggestedLocation={suggestedLocation}
          value={value}
        />
        <Box
          marginTop={1}
        >
          {
            Boolean(marker.length) &&
            <Typography
              component="p"
              variant="caption"
            >
              {`${watch('latitude') || marker[0].position.lat}, 
              ${watch('longitude') || marker[0].position.lng}`}
            </Typography>
          }
          {
            errors.latitude &&
            <PhotoErrorText
              classes={classes.photoErrorText}
              component="p"
              variant="caption"
            >
              {errors?.latitude?.message}
            </PhotoErrorText>
          }
        </Box>
        <input
          defaultValue={dataDetail.latitude}
          name="latitude"
          ref={register(validation.latitude)}
          type="hidden"
        />
        <input
          defaultValue={dataDetail.longitude}
          name="longitude"
          ref={register(validation.longitude)}
          type="hidden"
        />
      </div>
      <div className={clsx(classes.formContent, fullScreen && classes.fullWidth)}>
        {
          isEdit ? (
            <RoundedInput 
              defaultValue={dataDetail.link}
              errorMessage={errors?.link?.message}
              errors={errors.link}
              label="Website"
              name="link"
              register={register(validation.link)}
            />
          ) : (
            <FieldDisplay 
              data={dataDetail.link} 
              label="Website"
            />
          )
        }
        {
          isEdit ? (
            <RoundedInput 
              defaultValue={dataDetail.description}
              errorMessage={errors?.description?.message}
              errors={errors.description}
              label="Deskripsi"
              multiline={20}
              name="description"
              register={register(validation.description)}
            />
          ) : (
            <div className={classes.extraBottom}>
              <FieldDisplay 
                data={dataDetail.description} 
                label="Deskripsi"
                multiline={20}
              />
            </div>
          )
        }
        {
          isEdit ? (
            <div className={classes.buttonAction}>
              <Button
                classes={{
                  root: clsx(classes.buttonRounded, classes.expandButton)
                }}
                color="secondary"
                onClick={closeModal}
                variant="outlined"
              >
                Batal
              </Button>
              <Button 
                classes={{
                  root: classes.buttonRounded
                }}
                color="secondary"
                disabled={loading}
                endIcon={
                  loading ? (
                    <CircularProgress
                      color="secondary"
                      size={20} 
                    /> 
                  ) : null
                }
                onClick={handleSubmit(onSubmit)}
                variant="contained"
              >
                Simpan
              </Button>
            </div>
          ) : (
            <div className={classes.buttonAction}>
              <Button
                classes={{
                  root: clsx(classes.buttonRounded, classes.expandButton)
                }}
                onClick={showConfirmation}
              >
                Hapus
              </Button>
              <Button 
                classes={{
                  root: classes.buttonRounded
                }}
                color="secondary"
                onClick={switchToEdit}
                variant="contained"
              >
                Ubah
              </Button>
            </div>
          )
        }
      </div>
    </div>
  )
}

DataDetail.propTypes = {
  google: PropTypes.object,
  hideDetailModal: PropTypes.func,
  isEdit: PropTypes.bool,
  setIsEdit: PropTypes.func,
  showChangeStatusConfirmation: PropTypes.func,
  showDeleteConfirmation: PropTypes.func,
};

export default GoogleApiWrapper({
  apiKey: env.google_maps_apikey
})(DataDetail);