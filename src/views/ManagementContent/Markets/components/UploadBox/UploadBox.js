import React from 'react';
import { 
  Typography,
} from '@material-ui/core';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import useStyles from '../Form/form-jss';

const UploadBox = ({
  disableInput,
  errors,
  forceUrl,
  fullScreen,
  fileHandler,
  setPhoto,
  register,
  nodeErrorText,
  label,
  name,
  noBorder,
}) => {
  const classes = useStyles();
  return(
    <div className={classes.uploadContainer}>
      <Typography
        component="p"
        variant="body1"
      >
        {label}
      </Typography>
      <div className={clsx(classes.uploadArea, fullScreen && classes.fullWidthRel, !noBorder && classes.withBorder)}>
        <div className={classes.uploadInfo}>
          {
            fileHandler &&
            Boolean(fileHandler.length) &&
            <img
              alt="user"
              className={classes.userImage}
              src={
                forceUrl ? (
                  fileHandler
                ) : (
                  URL.createObjectURL(fileHandler[0])
                )
              }
            />
          }
          <img 
            alt="camera"
            src="/svg/camera.svg"
          />
          {errors && nodeErrorText}
        </div>
        {
          !disableInput &&
          <input 
            accept="image/*" 
            className={classes.file} 
            name={name}
            onChange={setPhoto}
            ref={register}
            type="file" 
          />
        }
      </div>
    </div>
  )
}

UploadBox.propTypes = {
  disableInput: PropTypes.bool,
  errors: PropTypes.object,
  fileHandler: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object
  ]),
  forceUrl: PropTypes.bool,
  fullScreen: PropTypes.bool,
  label: PropTypes.string,
  name: PropTypes.string,
  noBorder: PropTypes.bool,
  nodeErrorText: PropTypes.node,
  register: PropTypes.func,
  setPhoto: PropTypes.func,
}

export default UploadBox;