import React from 'react';
import PropTypes from 'prop-types';
import { 
  Button,
  Box,
  TextField
} from '@material-ui/core';
import { withStyles } from '@material-ui/styles';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { Map, Marker } from 'google-maps-react';

const FloatingButton = withStyles(() => ({
  root: {
    position: 'absolute',
    bottom: 10,
    right: 10
  }
}))(Button);

const Maps = ({
  google,
  centerMap,
  expandMap,
  addMarker,
  setExpandMap,
  onChangeSearch,
  setValueX,
  suggestedLocation,
  onCloseMap,
  value,
  marker
}) => {
  return(
    <Map 
      center={centerMap}
      containerStyle={{
        position: expandMap ? 'absolute' : 'relative',  
        width: expandMap ? '100%' : 320,
        height: expandMap ? '100%' : 150,
        top: 0,
        left: 0,
        zIndex: 10
      }}
      /* eslint-disable-next-line react/jsx-boolean-value */
      disableDefaultUI={true}
      google={google} 
      initialCenter={centerMap}
      onClick={addMarker}
      zoom={14} 
    >
      {
        !expandMap &&
        <Box
          alignItems="center"
          display="flex"
          height="100%"
          justifyContent="center"
          position="absolute"
          width="100%"
        >
          <Button
            color="secondary"
            onClick={() => setExpandMap(true)}
            variant="contained"
          >
            Pilih Lokasi
          </Button>
        </Box>
      }
      {
        expandMap &&             
        <>
          <Autocomplete
            autoComplete
            clearOnBlur={false}
            filterOptions={(x) => x}
            filterSelectedOptions
            getOptionLabel={(opt) => opt.description}
            includeInputInList
            noOptionsText={'Lokasi tidak ditemukan'}
            onChange={(e,v) => setValueX(v)}
            onInputChange={onChangeSearch}
            options={suggestedLocation}
            renderInput={
              (params) => 
                <TextField 
                  {...params} 
                  color="secondary"
                  label="Cari lokasi" 
                  variant="outlined" 
                />
            }
            style={{ width: 300, position: 'absolute', top: 10, left: 10, backgroundColor: 'white' }}
            value={value}
          />
          <FloatingButton
            color="secondary"
            onClick={onCloseMap}
            variant="contained"
          >
            Selesai Pilih Lokasi
          </FloatingButton>
        </>
      }
      {
        marker.map((mark, index) => (
          <Marker 
            key={index}
            name={mark.name}
            position={mark.position}
          />
        ))
      }
    </Map>
  )
};

Maps.propTypes = {
  addMarker: PropTypes.func,
  centerMap: PropTypes.object,
  expandMap: PropTypes.bool,
  google: PropTypes.object,
  marker: PropTypes.array,
  onChangeSearch: PropTypes.func,
  onCloseMap: PropTypes.func,
  setExpandMap: PropTypes.func,
  setValueX: PropTypes.func,
  suggestedLocation: PropTypes.array,
  value: PropTypes.string,
}

export default Maps;