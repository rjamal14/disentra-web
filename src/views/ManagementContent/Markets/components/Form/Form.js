import React, { useState, useRef, useEffect} from 'react'
import { useForm } from 'react-hook-form';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import axios from 'axios';
import { 
  Typography, 
  useMediaQuery,
  Button,
  CircularProgress,
  FormControl,
  Box,
  ClickAwayListener,
} from '@material-ui/core';
import { GoogleApiWrapper } from 'google-maps-react';
import env from 'config/env';
import { useTheme, withStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import { addMarket, refreshStateMarket, getCities } from 'actions';
import validation from '../../validation';
import useStyles from './form-jss';
import palette from 'theme/palette';
import { RoundedInput } from 'components';
import { RoundedSelect } from 'components';
import UploadBox from '../UploadBox';
import { googlePlace, googlePlaceDetail } from 'services/googleapis';
import { debounce } from 'throttle-debounce';
import Map from './Map';

const PhotoErrorText = withStyles(() => ({
  root: {
    color: palette.error.main,
    marginBottom: 10,
    marginLeft: 10,
    marginTop: 10
  }
}))(Typography);

function Form(props) {
  const { google, toggleModal } = props;
  const classes = useStyles();
  const dispatch = useDispatch();
  const { success, error, loading } = useSelector(state => state.market);
  const { data: dataBusinessField } = useSelector(state => state.businessField);
  const { data: dataCities } = useSelector(state => state.cities);
  const theme = useTheme();
  const { register, handleSubmit, control, errors, setValue, clearErrors, watch } = useForm();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const [thumbnail, setThumbnail] = useState(null);
  const [fileHandler1, setFileHandler1] = useState(null);
  const [fileHandler2, setFileHandler2] = useState(null);
  const [fileHandler3, setFileHandler3] = useState(null);
  const [fileHandler4, setFileHandler4] = useState(null);
  const [marker, setMarker] = useState([]);
  const [showSuggestion, setShowSuggestion] = useState(false);
  const [expandMap, setExpandMap] = useState(false);
  const [suggestedLocation, setSuggestedLocation] = useState([]);
  const [value, setValueX] = useState('');
  const [centerMap, setCenterMap] = useState({lat: -6.926617343390605, lng: 107.5987395482266});

  const initialization = useRef(true);
  useEffect(() => {
    dispatch(refreshStateMarket());
  }, []);

  const onSubmit = (data) => {
    dispatch(addMarket({ 
      payload: data 
    }));
  };

  useEffect(() => {
    if (value) {
      getPlaceDetail(value.place_id);
    }
  }, [value])

  const setMainPhoto = (event) => {
    setThumbnail(event.target.files);
  }

  const closeModal = () => {
    toggleModal(false);
  };

  const onChangeSearch = (event) => {
    if (event) {
      event.persist()
      executeDebounce(event);
    }
  };

  /* eslint-disable-next-line no-unused-vars */
  const relocateMap = (lat, lng) => {
    setCenterMap({lat, lng});
  };

  const executeDebounce = debounce(500, (e) => {
    getPlaceSuggestion(e.target.value);
  });

  useEffect(() => {
    if (initialization.current) {
      initialization.current = false;

    } else {
      if (success && !loading) {
        toggleModal(false);
        dispatch(refreshStateMarket());
      }
      if (error) {
        dispatch(refreshStateMarket());
      }
    }
  }, [success, error, loading]);

  /* eslint-disable-next-line no-unused-vars */
  const addMarker = (mapProps, map, clickEvent) => {
    setMarker([{
      position: {
        lat: clickEvent.latLng.lat(), 
        lng: clickEvent.latLng.lng()
      },
      name: 'Lokasi Pasar UMKM'
    }]);
    setValue('latitude', clickEvent.latLng.lat());
    setValue('longitude', clickEvent.latLng.lng());
  };

  const getPlaceSuggestion = async (query) => {
    const config = {
      method: 'GET',
      url: `${googlePlace}${query}&key=${env.google_maps_apikey}`
    }
    try {
      const response = await axios(config);
      setSuggestedLocation(response.data.predictions);
    } catch(error) {
      console.log(error);
    }
  };

  const getPlaceDetail = async (place_id) => {
    const config = {
      method: 'GET',
      url: `${googlePlaceDetail}${place_id}&key=${env.google_maps_apikey}`
    }
    try {
      const response = await axios(config);
      const { location } = response.data.result.geometry;
      relocateMap(location.lat, location.lng)
      setValue('latitude', location.lat);
      setValue('longitude', location.lng);
      setMarker([{
        position: {
          lat: location.lat,
          lng: location.lng
        },
        name: 'Lokasi Pasar UMKM'
      }])
    } catch(error) {
      console.log(error);
    }
  }

  const onCloseMap = () => {
    setExpandMap(false)
    if (marker.length) {
      setCenterMap({lat: marker[0].position.lat, lng: marker[0].position.lng});
      clearErrors('latitude');
      clearErrors('longitude');
    }
  }

  const onChangeQuerySearch = (e) => {
    e.persist();
    if (e.target.value.length > 2) {
      setShowSuggestion(true);
      searchCity(e.target.value);
    }
  }

  const searchCity = debounce(500, (query) => {
    dispatch(getCities({ query }));
  });

  const onSelectedCity = (index) => {
    setValue('city_id', dataCities[index].id);
    setShowSuggestion(false);
    clearErrors('city_id');
    document.getElementById('linked-city').value = dataCities[index].name;
  };
  
  return(
    <div className={clsx(classes.root, !fullScreen && classes.desktop)}>
      <Box>
        <UploadBox 
          errors={errors.thumbnail}
          fileHandler={thumbnail}
          fullScreen={fullScreen}
          label="Thumbnail"
          name="thumbnail"
          nodeErrorText={
            <PhotoErrorText
              classes={classes.photoErrorText}
              component="p"
              variant="caption"
            >
              {errors?.thumbnail?.message}
            </PhotoErrorText>
          }
          register={register(validation.thumbnail)}
          setPhoto={setMainPhoto}
        />
        <Box
          alignItems="center"
          display="flex"
          justifyContent="space-between"
          width="16vw"
        >
          <UploadBox 
            errors={{}}
            fileHandler={fileHandler1}
            fullScreen={fullScreen}
            label="Gambar Tambahan"
            name="image[0]"
            nodeErrorText={<div />}
            register={register()}
            setPhoto={(e) => setFileHandler1(e.target.files)}
          />
          <UploadBox 
            errors={{}}
            fileHandler={fileHandler2}
            fullScreen={fullScreen}
            label="&nbsp;"
            name="image[1]"
            nodeErrorText={<div />}
            register={register(validation.photo)}
            setPhoto={(e) => setFileHandler2(e.target.files)}
          />
        </Box>
        <Box
          alignItems="center"
          display="flex"
          justifyContent="space-between"
          width="16vw"
        >
          <UploadBox 
            errors={{}}
            fileHandler={fileHandler3}
            fullScreen={fullScreen}
            label="&nbsp;"
            name="image[2]"
            nodeErrorText={<div />}
            register={register()}
            setPhoto={(e) => setFileHandler3(e.target.files)}
          />
          <UploadBox 
            errors={{}}
            fileHandler={fileHandler4}
            fullScreen={fullScreen}
            label="&nbsp;"
            name="image[3]"
            nodeErrorText={<div />}
            register={register(validation.photo)}
            setPhoto={(e) => setFileHandler4(e.target.files)}
          />
        </Box>
      </Box>
      <div className={clsx(classes.formContent, fullScreen && classes.fullWidth)}>
        <RoundedInput
          defaultValue=""
          errorMessage={errors?.name?.message}
          errors={errors.name}
          label="Nama Usaha"
          name="name"
          register={register(validation.name)}
          type="text"
        />
        <div className={classes.selectWrapper}>
          <FormControl fullWidth>
            <RoundedSelect
              control={control}
              defaultValue={''}
              errorMessage={errors?.business_field_id?.message}
              errors={errors.business_field_id}
              label="Kategori"
              name="business_field_id"
              options={dataBusinessField}
              validation={validation.business_field_id}
            />
          </FormControl>
        </div>
        <div className={classes.selectWrapper}>
          <FormControl fullWidth>
            <RoundedInput
              defaultValue={''}
              errorMessage={errors?.city_id?.message}
              errors={errors.city_id}
              id="linked-city"
              label="Asal Kota/Kab."
              onChange={onChangeQuerySearch}
              placeholder="Cari nama kota.."
              type="text"
              validation={validation.city_id}
            />
            <input
              defaultValue={watch('city_id')}
              name="city_id"
              ref={register(validation.city_id)} 
              type="hidden"
            />
            {
              showSuggestion &&
              <ClickAwayListener onClickAway={() => setShowSuggestion(false)}>
                <div className={classes.suggestionBox}>
                  {
                    dataCities.map((city, index) => (
                      <div 
                        className={classes.suggestionList}
                        key={index} 
                        onClick={() => onSelectedCity(index)}
                      >
                        {city.name}
                      </div>
                    ))
                  }
                </div>
              </ClickAwayListener>
            }
          </FormControl>
        </div>
        <RoundedInput
          defaultValue=""
          errorMessage={errors?.address?.message}
          errors={errors.address}
          label="Alamat"
          multiline={3}
          name="address"
          register={register(validation.address)}
          type="text"
        />
        <Map
          addMarker={addMarker}
          centerMap={centerMap}
          expandMap={expandMap}
          google={google}
          marker={marker}
          onChangeSearch={onChangeSearch}
          onCloseMap={onCloseMap}
          setExpandMap={setExpandMap}
          setValueX={setValueX}
          suggestedLocation={suggestedLocation}
          value={value}
        />
        <Box
          marginTop={1}
        >
          {
            Boolean(marker.length) &&
            <Typography
              component="p"
              variant="caption"
            >
              {`${watch('latitude') || marker[0].position.lat}, 
              ${watch('longitude') || marker[0].position.lng}`}
            </Typography>
          }
          {
            errors.latitude &&
            <PhotoErrorText
              classes={classes.photoErrorText}
              component="p"
              variant="caption"
            >
              {errors?.latitude?.message}
            </PhotoErrorText>
          }
        </Box>
        <input
          defaultValue={watch('latitude')}
          name="latitude"
          ref={register(validation.latitude)}
          type="hidden"
        />
        <input
          defaultValue={watch('longitude')}
          name="longitude"
          ref={register(validation.longitude)}
          type="hidden"
        />
      </div>
      <div className={clsx(classes.formContent, fullScreen && classes.fullWidth)}>
        <RoundedInput 
          defaultValue=""
          errorMessage={errors?.link?.message}
          errors={errors.link}
          label="Website"
          name="link"
          register={register(validation.link)}
        />
        <RoundedInput 
          defaultValue=""
          errorMessage={errors?.description?.message}
          errors={errors.description}
          label="Deskripsi"
          multiline={23}
          name="description"
          register={register(validation.description)}
        />
        <div className={classes.buttonAction}>
          <Button
            classes={{
              root: clsx(classes.buttonRounded, classes.expandButton)
            }}
            color="secondary"
            onClick={closeModal}
            variant="outlined"
          >
            Batal
          </Button>
          <Button 
            classes={{
              root: classes.buttonRounded
            }}
            color="secondary"
            disabled={loading}
            endIcon={
              loading ? (
                <CircularProgress
                  color="secondary"
                  size={20} 
                /> 
              ) : null
            }
            onClick={handleSubmit(onSubmit)}
            variant="contained"
          >
            Simpan
          </Button>
        </div>
      </div>
    </div>
  )
}

Form.propTypes = {
  google: PropTypes.object,
  toggleModal: PropTypes.func
};

export default GoogleApiWrapper({
  apiKey: env.google_maps_apikey
})(Form);