import { makeStyles } from '@material-ui/styles';
import palette from 'theme/palette';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    justifyContent: 'space-between',
    width: '100%',
    position: 'relative'
  },
  input: {
    marginBottom: 15,
  },
  select:{
    marginBottom: 15,
    marginTop: 5,
    height: '40px',
    borderRadius: 50,
    '&.MuiInputBase-root': {
      fontSize: 14
    }
  },
  uploadContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    marginBottom: 20,

  },
  uploadArea: {
    position: 'relative',
    borderRadius: 5,
    width: '15vh',
    height: '15vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
  },
  withBorder: {
    borderColor: palette.secondary.dark,
    border: '2px dashed',
  },
  uploadInfo: {
    display: 'flex',
    flexDirection: 'column',
  },
  userImage: {
    position: 'absolute',
    zIndex: 1,
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    objectFit: 'cover',
    borderRadius: 5,
  },
  file: {
    height: '100%',
    width: '100%',
    cursor: 'pointer',
    position: 'absolute',
    opacity: 0,
    top: 0,
    left: 0,
    zIndex: 2
  },
  desktop: {
    display: 'flex',
    width: '100%',
    justifyContent: 'space-between'
  },
  formContent: {
    width: '30%',
    justifyContent: 'flex-end',
  },
  fullWidth: {
    width: '100%'
  },
  fullWidthRel: {
    width: '100vw',
  },
  buttonRounded: {
    borderRadius: 20,
    textTransform: 'capitalize',
    marginLeft: 10,
    padding: '8px 32px'
  },
  buttonAction: {
    marginTop: 150,
    display: 'flex',
    justifyContent: 'flex-end',
    padding: '20px 0 10px 0'
  },
  box:{
    padding: theme.spacing(0.5,2),
    [theme.breakpoints.down('md')]: {
      left:50
    }
  },
  selectWrapper: {
    width: '100%',
    position: 'relative',
  },
  suggestionBox: {
    margin: '-10px 0 10px',
    position: 'absolute',
    backgroundColor: '#fff',
    width: '100%',
    borderRadius: 5,
    left: 0,
    top: 80,
    maxHeight: 200,
    zIndex: 100,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    overflowY: 'auto',
    boxShadow: '0px 5px 5px -3px rgba(0,0,0,0.2), 0px 8px 10px 1px rgba(0,0,0,0.14), 0px 3px 14px 2px rgba(0,0,0,0.12)'
  },
  suggestionList: {
    padding: '8px 18px',
    cursor: 'pointer',
    width: '100%',
    '&:hover': {
      backgroundColor: '#d8d8d8' 
    }
  }
}));

export default useStyles;