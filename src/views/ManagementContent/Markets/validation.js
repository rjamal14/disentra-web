const defaultMessage = 'Tidak boleh kosong';

const validation = {
  thumbnail: {
    required: defaultMessage
  },
  image: {
    required: defaultMessage
  },
  name: {
    required: defaultMessage
  },
  business_field_id: {
    required: defaultMessage
  },
  city_id: {
    required: defaultMessage
  },
  address: {
    required: defaultMessage
  },
  link: {
    required: defaultMessage
  },
  description: {
    required: defaultMessage
  },
  latitude: {
    required: defaultMessage
  },
  longitude: {
    required: defaultMessage
  }
}

export default validation;