/*eslint-disable react/jsx-boolean-value*/
import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';

import { Page, Breadcrumb, Modal } from 'components';
import { Header, Results, Form, DataDetail } from './components';
import { Card } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { getLearningCenter, deleteLearningCenter, changeStatusLearningCenter } from 'actions';
import swal from 'sweetalert';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(5, 4)
  },
  results: {
    marginTop: theme.spacing(3)
  },
  breadcrumb:{
    margin: theme.spacing(3),
    marginBottom: -theme.spacing(6),
  }
}));

const path = [
  ['Manajemen Konten', '/management-content/learning-center'],
  ['Sentra Belajar', '/management-content/learning-center']
];

const CustomerManagementList = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const [title_cont, setTitle] = useState('');
  const [content_type_cont, setContentType] = useState('$');
  
  const [showModal, setShowModal] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [showDataDetail, setShowDataDetail] = useState(false);
  const [dataPerPage, setDataPerPage] = useState(10);
  const { data, pagination } = useSelector(state => state.learningCenter);

  useEffect(() => {
    dispatch(getLearningCenter({
      payload: {
        dataPerPage
      }
    }));
  }, []);

  useEffect(() => {
    refetchContent(dataPerPage, {
      content_type_cont,
      title_cont,
    })
  }, [dataPerPage]);

  const refetchContent = (perPage, additionalFilter) => {
    dispatch(getLearningCenter({
      payload: {
        dataPerPage: perPage || dataPerPage,
        params: additionalFilter,
      }
    }))
  };

  const showDeleteConfirmation = async (index) => {
    const confirmation = await swal({
      title: 'Hapus Konten',
      text: 'Setelah dihapus, Anda tidak akan dapat memulihkan data ini!',
      icon: 'warning',
      buttons: true,
      dangerMode: true,
    })
    if (confirmation) {
      deleteExecution(index);
    }
  };

  const deleteExecution = (index) => {
    dispatch(deleteLearningCenter({
      payload: {
        id: data[index].id
      }
    }))
  };

  const showChangeStatusConfirmation = async (index, status) => {
    const confirmation = await swal({
      title: status === 'show' ? 'Perlihatkan Konten' : 'Sembunyikan Konten',
      text: `Apakah Anda yakin akan ${status === 'show' ? 'memperlihatkan' : 'menyembunyikan'} konten ini?`,
      icon: 'warning',
      buttons: true,
      dangerMode: true,
    })
    if (confirmation) {
      suspendExecution(index, status);
    }
  };

  const suspendExecution = (index, status) => {
    dispatch(changeStatusLearningCenter({
      payload: {
        id: data[index].id,
        status
      }
    }))
  };

  const hideDetailModal = () => {
    setShowDataDetail(false);
    setIsEdit(false);
  };

  const onChangePage = (direction) => {
    let currentPage = pagination?.current_page ?? 1;
    const totalPages = pagination?.total_pages ?? 1;
    if (direction === 'next') {
      if (currentPage < totalPages) {
        dispatch(getLearningCenter({
          payload: {
            dataPerPage,
            page: pagination?.current_page + 1 ?? 1
          }
        }))
      }
    } else {
      if (currentPage > 1) {
        dispatch(getLearningCenter({
          payload: {
            dataPerPage,
            page: pagination?.current_page - 1 ?? 1
          }
        }))
      }
    }
  }

  const executeFilter = () => {
    refetchContent(null, {
      content_type_cont,
      title_cont,
    })
  };

  const resetFilter = () => {
    setContentType('$');
    setTitle('');
    refetchContent(dataPerPage)
  };
  return (
    <Page
      className={classes.root}
      title="BJB DiSentra - Learning Center"
    >
      <Modal 
        children={<Form toggleModal={setShowModal} />}
        isOpen={showModal}
        title="Tambah Sentra Belajar"
        toggleModal={setShowModal}
      />
      <Modal
        children={
          <DataDetail 
            hideDetailModal={hideDetailModal}
            isEdit={isEdit}
            maxWidth="lg"
            setIsEdit={setIsEdit}
            showChangeStatusConfirmation={showChangeStatusConfirmation}
            showDeleteConfirmation={showDeleteConfirmation}
          />
        }
        hideDetailModal={hideDetailModal}
        isOpen={showDataDetail}
        title="Detail Sentra Belajar"
        toggleModal={setShowDataDetail}
      />
      <Card>
        <div className={classes.breadcrumb}>
          <Breadcrumb route={path} />
        </div>
        <Header 
          content_type={content_type_cont}
          dataPerPage={dataPerPage}
          executeFilter={executeFilter}
          resetFilter={resetFilter}
          setContentType={setContentType}
          setDataPerPage={setDataPerPage}
          setShowModal={setShowModal} 
          setTitle={setTitle}
          showModal={showModal}
          title={title_cont}
        />
        <Results
          className={classes.results}
          onChangePage={onChangePage}
          setIsEdit={setIsEdit}
          setShowDataDetail={setShowDataDetail}
          showChangeStatusConfirmation={showChangeStatusConfirmation}
          showDeleteConfirmation={showDeleteConfirmation}
        />
      </Card>
    </Page>
  );
};

export default CustomerManagementList;