import React, { useState, useRef, useEffect} from 'react'
import { useForm } from 'react-hook-form';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { 
  Typography, 
  useMediaQuery,
  Button,
  CircularProgress,
} from '@material-ui/core';
import { useTheme, withStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import { addLearningCenter, refreshStateLearningCenter } from 'actions';
import validation from '../../validation';
import useStyles from './form-jss';
import palette from 'theme/palette';
import { RoundedInput } from 'components';
import RadioGroup from '../RadioGroup';

function Form(props) {
  const { toggleModal } = props;
  const classes = useStyles();
  const dispatch = useDispatch();
  const { success, error, loading } = useSelector(state => state.learningCenter);
  const theme = useTheme();
  const { register, handleSubmit, control, errors } = useForm();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const [fileHandler, setFileHandler] = useState(null);
  const initialization = useRef(true);

  const PhotoErrorText = withStyles(() => ({
    root: {
      color: palette.error.main,
      marginBottom: 10,
      marginLeft: 10,
      marginTop: 10
    }
  }))(Typography);

  useEffect(() => {
    dispatch(refreshStateLearningCenter());
  }, []);

  const onSubmit = (data) => {
    dispatch(addLearningCenter({ 
      payload: data 
    }));
  };

  const setPhoto = (event) => {
    setFileHandler(event.target.files);
  };

  const closeModal = () => {
    toggleModal(false);
  };

  useEffect(() => {
    if (initialization.current) {
      initialization.current = false;
    } else {
      if (success && !loading) {
        toggleModal(false);
        dispatch(refreshStateLearningCenter());
      }
      if (error) {
        dispatch(refreshStateLearningCenter());
      }
    }
  }, [success, error, loading]);
  
  return(
    <div className={clsx(classes.root, !fullScreen && classes.desktop)}>
      <div className={classes.uploadContainer}>
        <Typography
          component="p"
          variant="body1"
        >
          Thumbnail
        </Typography>
        <div className={clsx(classes.uploadArea, fullScreen && classes.fullWidthRel)}>
          <div className={classes.uploadInfo}>
            {
              fileHandler &&
              Boolean(fileHandler?.length) &&
              <img
                alt="user"
                className={classes.userImage}
                src={URL.createObjectURL(fileHandler[0])}
              />
            }
            <img 
              alt="camera"
              src="/svg/camera.svg"
            />
            <Typography
              component="p"
              variant="caption"
            >
              Tambah Foto (Maks. ukuran 2MB)
            </Typography>
          </div>
          <input 
            accept="image/*" 
            className={classes.file} 
            name="image"
            onChange={setPhoto}
            ref={register(validation.photo)}
            type="file" 
          />
        </div>
        {
          errors.photo &&
          <PhotoErrorText
            classes={classes.photoErrorText}
            component="p"
            variant="caption"
          >
            {errors.photo.message}
          </PhotoErrorText>
        }
      </div>
      <div className={clsx(classes.formContent, fullScreen && classes.fullWidth)}>
        <RoundedInput 
          defaultValue=""
          errorMessage={errors?.title?.message}
          errors={errors.title}
          label="Judul"
          name="title"
          register={register(validation.title)}
        />
        <RoundedInput 
          defaultValue=""
          errorMessage={errors?.description?.message}
          errors={errors.description}
          label="Deskripsi"
          multiline={5}
          name="description"
          register={register(validation.description)}
        />
        <RadioGroup 
          control={control}
          errorMessage={errors?.content_type?.message}
          errors={errors.content_type}
          label="Kategori"
          name="content_type"
          options={[
            {label: 'Video', value: 'video'},
            {label: 'E-Book', value: 'ebook'},
            {label: 'Berita', value: 'news'}
          ]}
          validation={validation.content_type}
          value={'video'}
        />
        <RoundedInput
          defaultValue=""
          errorMessage={errors?.author_name?.message}
          errors={errors.author_name}
          label="Penulis"
          name="author_name"
          register={register(validation.author_name)}
          type="text"
        />
        <RoundedInput
          defaultValue=""
          errorMessage="Nomor telepon hanya terdiri dari 10 sampai 14 nomor"
          errors={errors.link}
          label="URL"
          name="link"
          register={register(validation.link)}
          type="text"
        />
        <div className={classes.buttonAction}>
          <Button
            classes={{
              root: clsx(classes.buttonRounded, classes.expandButton)
            }}
            color="secondary"
            onClick={closeModal}
            variant="outlined"
          >
            Batal
          </Button>
          <Button 
            classes={{
              root: classes.buttonRounded
            }}
            color="secondary"
            disabled={loading}
            endIcon={
              loading ? (
                <CircularProgress
                  color="secondary"
                  size={20} 
                /> 
              ) : null
            }
            onClick={handleSubmit(onSubmit)}
            variant="contained"
          >
            Simpan
          </Button>
        </div>
      </div>
    </div>
  )
}

Form.propTypes = {
  toggleModal: PropTypes.func
};

export default Form;