const defaultMessage = 'Tidak boleh kosong';

const validation = {
  title: {
    required: defaultMessage
  },
  author_name: {
    required: defaultMessage
  },
  content_type: {
    required: defaultMessage
  },
  image: {
    required: defaultMessage
  },
  link: {
    required: defaultMessage
  },
  description: {
    required: defaultMessage
  }
}

export default validation;