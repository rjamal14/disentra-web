import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(() => ({
  root: {
    justifyContent: 'space-between',
    width: '100%'
  },
  formContent: {
    height: 400,
    maxHeight: 400,
    overflowY: 'auto',
    overflowX: 'hidden'
  },
  columnExtend: {
    width: '10vw'
  }
}));

export default useStyles;