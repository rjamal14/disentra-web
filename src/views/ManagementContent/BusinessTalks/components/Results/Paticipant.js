import React from 'react';
import { 
  Typography,
  Grid,
  useMediaQuery,
} from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import useStyles from './participant-jss';

const Participant = ({
  participant
}) => {
  const classes = useStyles();
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  return(
    <div className={clsx(classes.root, !fullScreen && classes.desktop)}>
      <div className={clsx(classes.formContent, fullScreen && classes.fullWidth)}>
        <Grid
          container
          spacing={3}
        >
          <Grid item>
            <div className={classes.columnExtend}>
              <Typography 
                color="secondary"
                variant="body1"
              >
                Nama Peserta
              </Typography>
            </div>
          </Grid>
          <Grid item>
            <div className={classes.columnExtend}>
              <Typography 
                color="secondary"
                variant="body1"
              >
                Asal Kota/Kab
              </Typography>
            </div>
          </Grid>
        </Grid>
        {
          participant?.map((item, index) => (
            <Grid
              container
              key={index}
              spacing={3}
            >
              <Grid item>
                <div className={classes.columnExtend}>
                  <Typography 
                    color="textPrimary"
                    variant="body2"
                  >
                    {item?.user?.name}
                  </Typography>
                </div>
              </Grid>
              <Grid item>
                <div className={classes.columnExtend}>
                  <Typography 
                    color="textPrimary"
                    variant="body2"
                  >
                    {item?.city?.name || '-'}
                  </Typography>
                </div>
              </Grid>
            </Grid>
          ))
        }
      </div>
    </div>
  )
}

Participant.propTypes = {
  participant: PropTypes.array
}

export default Participant;