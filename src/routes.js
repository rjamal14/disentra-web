/* eslint-disable react/no-multi-comp */
/* eslint-disable react/display-name */
import React, { lazy } from 'react';
import { Redirect } from 'react-router-dom';
import loadable from '@loadable/component';

import AuthLayout from './layouts/Auth';
import ErrorLayout from './layouts/Error';
import DashboardLayout from './layouts/Dashboard';
import DashboardDefaultView from './views/DashboardDefault';

const routes = [
  {
    path: '/',
    exact: true,
    component: () => <Redirect to="/auth/login" />
  },
  {
    path: '/auth',
    component: AuthLayout,
    routes: [
      {
        path: '/auth/login',
        exact: true,
        component: lazy(() => import('views/Login'))
      },
      {
        path: '/auth/forgot-password',
        exact: true,
        component: lazy(() => import('views/Forgot'))
      },
      {
        path: '/auth/reset-password',
        exact: true,
        component: lazy(() => import('views/Reset'))
      },
      {
        path: '/auth/confirmation',
        exact: true,
        component: lazy(() => import('views/Confirmation'))
      },
      {
        component: () => <Redirect to="/errors/error-404" />
      }
    ]
  },
  {
    path: '/errors',
    component: ErrorLayout,
    routes: [
      {
        path: '/errors/error-401',
        exact: true,
        component: lazy(() => import('views/Error401'))
      },
      {
        path: '/errors/error-404',
        exact: true,
        component: lazy(() => import('views/Error404'))
      },
      {
        path: '/errors/error-500',
        exact: true,
        component: lazy(() => import('views/Error500'))
      },
      {
        component: () => <Redirect to="/errors/error-404" />
      }
    ]
  },
  {
    route: '*',
    component: DashboardLayout,
    routes: [
      {
        path: '/dashboard',
        exact: true,
        component: DashboardDefaultView
      },
      {
        path: '/management-user/user',
        exact: true,
        component: lazy(() => import('views/ManagementUser/User'))
      },
      {
        path: '/management-user/umkm',
        exact: true,
        component: lazy(() => import('views/ManagementUser/UMKM'))
      },
      {
        path: '/management-user/role',
        exact: true,
        component: lazy(() => import('views/ManagementUser/Role'))
      },
      {
        path: '/master-data/province',
        exact: true,
        component: lazy(() => import('views/MasterData/Province'))
      },
      {
        path: '/master-data/city',
        exact: true,
        component: lazy(() => import('views/MasterData/City'))
      },
      {
        path: '/master-data/district',
        exact: true,
        component: lazy(() => import('views/MasterData/District'))
      },
      {
        path: '/master-data/village',
        exact: true,
        component: lazy(() => import('views/MasterData/Village'))
      },
      {
        path: '/master-data/business-field',
        exact: true,
        component: lazy(() => import('views/MasterData/BusinessField'))
      },
      {
        path: '/master-data/job-position',
        exact: true,
        component: lazy(() => import('views/MasterData/JobPosition'))
      },
      {
        path: '/management-content/business-talk',
        exact: true,
        component: lazy(() => import('views/ManagementContent/BusinessTalks'))
      },
      {
        path: '/management-content/learning-center',
        exact: true,
        component: lazy(() => import('views/ManagementContent/LearningCenters'))
      },
      {
        path: '/management-content/banner',
        exact: true,
        component: lazy(() => import('views/ManagementContent/Banners'))
      },
      {
        path: '/management-content/room',
        exact: true,
        component: lazy(() => import('views/ManagementContent/Rooms'))
      },
      {
        path: '/management-content/umkm-market',
        exact: true,
        component: lazy(() => import('views/ManagementContent/Markets'))
      },
      {
        path: '/profile/setting',
        exact: true,
        component: loadable(() => import('views/Profile/Setting')),
      },
      {
        path: '/profile/change-password',
        exact: true,
        component: lazy(() => import('views/Profile/ChangePassword')),
      },
      {
        component: () => <Redirect to="/errors/error-404" />
      },
      
    ]
  },
];

export default routes;
