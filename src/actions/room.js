import swal from 'sweetalert';
import axios from 'axios';
import env from 'config/env';
import { apiWithToken } from 'config/apiWithToken';
export const GET_ROOM_REQUEST = 'GET_ROOM_REQUEST';
export const GET_ROOM_SUCCESS = 'GET_ROOM_SUCCESS';
export const GET_ROOM_FAILED = 'GET_ROOM_FAILED';

export const ADD_ROOM_REQUEST = 'ADD_ROOM_REQUEST';
export const ADD_ROOM_SUCCESS = 'ADD_ROOM_SUCCESS';
export const ADD_ROOM_FAILED = 'ADD_ROOM_FAILED';

export const EDIT_ROOM_REQUEST = 'EDIT_ROOM_REQUEST';
export const EDIT_ROOM_SUCCESS = 'EDIT_ROOM_SUCCESS';
export const EDIT_ROOM_FAILED = 'EDIT_ROOM_FAILED';

export const DELETE_ROOM_REQUEST = 'DELETE_ROOM_REQUEST';
export const DELETE_ROOM_SUCCESS = 'DELETE_ROOM_SUCCESS';
export const DELETE_ROOM_FAILED = 'DELETE_ROOM_FAILED';

export const CHANGE_STATUS_ROOM_REQUEST = 'CHANGE_STATUS_ROOM_REQUEST';
export const CHANGE_STATUS_ROOM_SUCCESS = 'CHANGE_STATUS_ROOM_SUCCESS';
export const CHANGE_STATUS_ROOM_FAILED = 'CHANGE_STATUS_ROOM_FAILED';

export const SET_ROOM_DATA_DETAIL = 'SET_ROOM_DATA_DETAIL';
export const SET_ROOM_DATA_FASILITAS = 'SET_ROOM_DATA_FASILITAS';
export const REFRESH_STATE_ROOM = 'REFRESH_STATE_ROOM';

const API_URL = `${env.baseUrl}api/v1`;

export const refreshStateRoom = () => (dispatch) => {
  dispatch({ type: REFRESH_STATE_ROOM });
};

export const getRoom = ({ payload }) => async (dispatch) => {
  dispatch({ type: GET_ROOM_REQUEST });
  const url = `/rooms?search=${payload?.search}&per_page=${payload?.dataPerPage}&order_by=created_at&page=${payload?.page || 1}`

  try {
    const response = await apiWithToken.get(url, { payload });
    dispatch({ type: GET_ROOM_SUCCESS, payload: response.data });
  } catch(error) {
    dispatch({ type: GET_ROOM_FAILED });
    errorHandler(error);
  }
};

export const getAllRoom = () => async (dispatch) => {
  dispatch({ type: GET_ROOM_REQUEST });
  const url = '/rooms'

  try {
    const response = await apiWithToken.get(url);
    dispatch({ type: GET_ROOM_SUCCESS, payload: response.data });
  } catch(error) {
    dispatch({ type: GET_ROOM_FAILED });
    errorHandler(error);
  }
};

export const addRoom = ({ payload }) => async (dispatch) => {
  dispatch({ type: ADD_ROOM_REQUEST });
  const freshToken = localStorage.getItem('token');
  let formData = new FormData();
  for (let [key, value] of Object.entries(payload)) {
    if (key === 'image') {
      Array.from(payload[key]).map(file => formData.append(`room[${key}]`, file));
    } else {
      formData.append(`room[${key}]`, value);
    }
  }
  const config = {
    url: API_URL + '/rooms',
    method: 'POST',
    headers: {
      Authorization: `Bearer ${freshToken}`
    },
    data: formData
  };
  try {
    const response = await axios(config);
    swal('Berhasil!', 'Anda berhasil menambah data!', 'success');
    dispatch({ 
      type: ADD_ROOM_SUCCESS, 
      payload: response.data.data
    });
  } catch(error) {
    dispatch({ type: ADD_ROOM_FAILED });
    errorHandler(error);
  }
};

export const editRoom = ({ payload, targetId }) => async (dispatch) => {
  dispatch({ type: EDIT_ROOM_REQUEST });
  const freshToken = localStorage.getItem('token');
  let formData = new FormData();
  for (let [key, value] of Object.entries(payload)) {
    if (key === 'image' && value) {
      Array.from(payload[key]).map(file => formData.append(`room[${key}]`, file));
    } else {
      formData.append(`room[${key}]`, value);
    }
  }
  const config = {
    url: API_URL + '/rooms/' + targetId,
    method: 'PATCH',
    headers: {
      Authorization: `Bearer ${freshToken}`
    },
    data: formData
  };
  try {
    const response = await axios(config);
    swal('Berhasil!', 'Anda berhasil mengubah data!', 'success');
    dispatch({ type: EDIT_ROOM_SUCCESS, payload: response.data.data, id: targetId });
  } catch(error) {
    dispatch({ type: EDIT_ROOM_FAILED });
    errorHandler(error);
  }
};

export const deleteRoom = ({ payload }) => async (dispatch) => {
  dispatch({ type: DELETE_ROOM_REQUEST });
  const url = `/rooms/${payload.id}`

  try {
    await apiWithToken.delete(url);
    swal('Berhasil!', 'Anda berhasil menghapus data!', 'success')
      .then(() => window.location.reload());
    dispatch({ type: DELETE_ROOM_SUCCESS, id: payload.id });
  } catch(error) {
    dispatch({ type: DELETE_ROOM_FAILED });
    errorHandler(error);
  }
};

export const setRoomDataDetail = ({ payload }) => (dispatch) => {
  dispatch({ type: SET_ROOM_DATA_DETAIL, payload });
};

export const setRoomDataFasilitas = ({ payload }) => (dispatch) => {
  dispatch({ type: SET_ROOM_DATA_FASILITAS, payload });
};

export const refreshState = () => (dispatch) => {
  dispatch({ type: REFRESH_STATE_ROOM });
};

const errorHandler = (error) => {
  if (error?.response?.status === 401){
    localStorage.clear();
    window.location = '/auth/login';
  } else {
    swal(
      '', 
      'Terjadi kesalahan pada server',
      'error'
    )
  }
};