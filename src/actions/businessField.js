import swal from 'sweetalert';
import { apiWithToken } from 'config/apiWithToken';

export const GET_BUSINESS_REQUEST = 'GET_BUSINESS_REQUEST';
export const GET_BUSINESS_SUCCESS = 'GET_BUSINESS_SUCCESS';
export const GET_BUSINESS_FAILED = 'GET_BUSINESS_FAILED';

export const ADD_BUSINESS_REQUEST = 'ADD_BUSINESS_REQUEST';
export const ADD_BUSINESS_SUCCESS = 'ADD_BUSINESS_SUCCESS';
export const ADD_BUSINESS_FAILED = 'ADD_BUSINESS_FAILED';

export const EDIT_BUSINESS_REQUEST = 'EDIT_BUSINESS_REQUEST';
export const EDIT_BUSINESS_SUCCESS = 'EDIT_BUSINESS_SUCCESS';
export const EDIT_BUSINESS_FAILED = 'EDIT_BUSINESS_FAILED';

export const DELETE_BUSINESS_REQUEST = 'DELETE_BUSINESS_REQUEST';
export const DELETE_BUSINESS_SUCCESS = 'DELETE_BUSINESS_SUCCESS';
export const DELETE_BUSINESS_FAILED = 'DELETE_BUSINESS_FAILED';

export const CHANGE_STATUS_BUSINESS_REQUEST = 'CHANGE_STATUS_BUSINESS_REQUEST';
export const CHANGE_STATUS_BUSINESS_SUCCESS = 'CHANGE_STATUS_BUSINESS_SUCCESS';
export const CHANGE_STATUS_BUSINESS_FAILED = 'CHANGE_STATUS_BUSINESS_FAILED';

export const SET_BUSINESS_DATA_DETAIL = 'SET_BUSINESS_DATA_DETAIL';
export const REFRESH_STATE_BUSINESS = 'REFRESH_STATE_BUSINESS';

export const getBusinessField = ({ payload }) => async (dispatch) => {
  dispatch({ type: GET_BUSINESS_REQUEST });
  const url = `/business_fields?search=${payload?.search}&per_page=${payload.dataPerPage}&order_by=created_at&page=${payload.page || 1}`

  try {
    const response = await apiWithToken.get(url, { payload });
    dispatch({ type: GET_BUSINESS_SUCCESS, payload: response.data });
  } catch(error) {
    dispatch({ type: GET_BUSINESS_FAILED });
    errorHandler(error);
  }
};

export const getAllBusinessField = () => async (dispatch) => {
  dispatch({ type: GET_BUSINESS_REQUEST });
  const url = '/business_fields/all_business_field';
  try {
    const response = await apiWithToken.get(url);
    dispatch({ type: GET_BUSINESS_SUCCESS, payload: response.data });
  } catch(error) {
    dispatch({ type: GET_BUSINESS_FAILED });
  }
}

export const addBusinessField = ({ payload }) => async (dispatch) => {
  dispatch({ type: ADD_BUSINESS_REQUEST });
  const url = '/business_fields'

  try {
    const response = await apiWithToken.post(url, payload );
    dispatch({ 
      type: ADD_BUSINESS_SUCCESS, 
      payload: response.data.data
    })
    swal('Berhasil!', 'Anda berhasil menambah data!', 'success')
      .then(() => window.location.reload());
  } catch(error) {
    dispatch({ type: ADD_BUSINESS_FAILED });
    errorHandler(error);
  }
};

export const editBusinessField = ({ payload, targetId }) => async (dispatch) => {
  dispatch({ type: EDIT_BUSINESS_REQUEST });
  const url = `/business_fields/${targetId}`
  
  try {
    const response = await apiWithToken.put(url, payload);
    swal('Berhasil!', 'Anda berhasil mengubah data!', 'success')
      .then(() => window.location.reload());
    dispatch({ type: EDIT_BUSINESS_SUCCESS, payload: response.data.data, id: targetId });
  } catch(error) {
    dispatch({ type: EDIT_BUSINESS_FAILED });
    errorHandler(error);
  }
};

export const deleteBusinessField = ({ payload }) => async (dispatch) => {
  dispatch({ type: DELETE_BUSINESS_REQUEST });
  const url = `/business_fields/${payload.id}`

  try {
    await apiWithToken.delete(url);
    swal('Berhasil!', 'Anda berhasil menghapus data!', 'success')
      .then(() => window.location.reload());
    dispatch({ type: DELETE_BUSINESS_SUCCESS, id: payload.id });
  } catch(error) {
    dispatch({ type: DELETE_BUSINESS_FAILED });
    errorHandler(error);
  }
};

export const refreshState = () => (dispatch) => {
  dispatch({ type: REFRESH_STATE_BUSINESS });
};

const errorHandler = (error) => {
  if (error?.response?.status === 401){
    localStorage.clear();
    window.location = '/auth/login';
  } else {
    swal(
      '', 
      'Terjadi kesalahan pada server',
      'error'
    )
  }
};
