import swal from 'sweetalert';
import { apiWithToken } from 'config/apiWithToken';

export const GET_JOB_POSITION_REQUEST = 'GET_JOB_POSITION_REQUEST';
export const GET_JOB_POSITION_SUCCESS = 'GET_JOB_POSITION_SUCCESS';
export const GET_JOB_POSITION_FAILED = 'GET_JOB_POSITION_FAILED';

export const ADD_JOB_POSITION_REQUEST = 'ADD_JOB_POSITION_REQUEST';
export const ADD_JOB_POSITION_SUCCESS = 'ADD_JOB_POSITION_SUCCESS';
export const ADD_JOB_POSITION_FAILED = 'ADD_JOB_POSITION_FAILED';

export const EDIT_JOB_POSITION_REQUEST = 'EDIT_JOB_POSITION_REQUEST';
export const EDIT_JOB_POSITION_SUCCESS = 'EDIT_JOB_POSITION_SUCCESS';
export const EDIT_JOB_POSITION_FAILED = 'EDIT_JOB_POSITION_FAILED';

export const DELETE_JOB_POSITION_REQUEST = 'DELETE_JOB_POSITION_REQUEST';
export const DELETE_JOB_POSITION_SUCCESS = 'DELETE_JOB_POSITION_SUCCESS';
export const DELETE_JOB_POSITION_FAILED = 'DELETE_JOB_POSITION_FAILED';

export const CHANGE_STATUS_JOB_POSITION_REQUEST = 'CHANGE_STATUS_JOB_POSITION_REQUEST';
export const CHANGE_STATUS_JOB_POSITION_SUCCESS = 'CHANGE_STATUS_JOB_POSITION_SUCCESS';
export const CHANGE_STATUS_JOB_POSITION_FAILED = 'CHANGE_STATUS_JOB_POSITION_FAILED';

export const SET_JOB_POSITION_DATA_DETAIL = 'SET_JOB_POSITION_DATA_DETAIL';
export const REFRESH_STATE_JOB_POSITION = 'REFRESH_STATE_JOB_POSITION';

export const getJobPositions = () => async (dispatch) => {
  dispatch({ type: GET_JOB_POSITION_REQUEST });
  const url = '/job_positions/all_job_position'

  try {
    const response = await apiWithToken(url);
    dispatch({ type: GET_JOB_POSITION_SUCCESS, payload: response.data.data });
  } catch(error) {
    dispatch({ type: GET_JOB_POSITION_FAILED });
  }
}

export const getJobPositionPerPage = ({ payload }) => async (dispatch) => {
  dispatch({ type: GET_JOB_POSITION_REQUEST });
  const url = `/job_positions?search=${payload?.search}&per_page=${payload.dataPerPage}&order_by=created_at&page=${payload.page || 1}`

  try {
    const response = await apiWithToken.get(url, { payload });
    dispatch({ type: GET_JOB_POSITION_SUCCESS, payload: response.data.data });
  } catch(error) {
    dispatch({ type: GET_JOB_POSITION_FAILED });
    errorHandler(error);
  }
};

export const addJobPosition = ({ payload }) => async (dispatch) => {
  dispatch({ type: ADD_JOB_POSITION_REQUEST });
  const url = '/job_positions'

  try {
    const response = await apiWithToken.post(url, payload );
    dispatch({ 
      type: ADD_JOB_POSITION_SUCCESS, 
      payload: response.data.data
    })
    swal('Berhasil!', 'Anda berhasil menambah data!', 'success')
      .then(() => window.location.reload());
  } catch(error) {
    dispatch({ type: ADD_JOB_POSITION_FAILED });
    errorHandler(error);
  }
};

export const editJobPosition = ({ payload, targetId }) => async (dispatch) => {
  dispatch({ type: EDIT_JOB_POSITION_REQUEST });
  const url = `/job_positions/${targetId}`
  
  try {
    const response = await apiWithToken.put(url, payload);
    swal('Berhasil!', 'Anda berhasil mengubah data!', 'success')
      .then(() => window.location.reload());
    dispatch({ type: EDIT_JOB_POSITION_SUCCESS, payload: response.data.data, id: targetId });
  } catch(error) {
    dispatch({ type: EDIT_JOB_POSITION_FAILED });
    errorHandler(error);
  }
};

export const deleteJobPosition = ({ payload }) => async (dispatch) => {
  dispatch({ type: DELETE_JOB_POSITION_REQUEST });
  const url = `/job_positions/${payload.id}`

  try {
    await apiWithToken.delete(url);
    swal('Berhasil!', 'Anda berhasil menghapus data!', 'success')
      .then(() => window.location.reload());
    dispatch({ type: DELETE_JOB_POSITION_SUCCESS, id: payload.id });
  } catch(error) {
    dispatch({ type: DELETE_JOB_POSITION_FAILED });
    errorHandler(error);
  }
};

export const refreshState = () => (dispatch) => {
  dispatch({ type: REFRESH_STATE_JOB_POSITION });
};

const errorHandler = (error) => {
  if (error?.response?.status === 401){
    localStorage.clear();
    window.location = '/auth/login';
  } else {
    swal(
      '', 
      'Terjadi kesalahan pada server',
      'error'
    )
  }
};