import swal from 'sweetalert';
import { apiWithToken } from 'config/apiWithToken';

export const GET_VILLAGE_REQUEST = 'GET_VILLAGE_REQUEST';
export const GET_VILLAGE_SUCCESS = 'GET_VILLAGE_SUCCESS';
export const GET_VILLAGE_FAILED = 'GET_VILLAGE_FAILED';

export const ADD_VILLAGE_REQUEST = 'ADD_VILLAGE_REQUEST';
export const ADD_VILLAGE_SUCCESS = 'ADD_VILLAGE_SUCCESS';
export const ADD_VILLAGE_FAILED = 'ADD_VILLAGE_FAILED';

export const EDIT_VILLAGE_REQUEST = 'EDIT_VILLAGE_REQUEST';
export const EDIT_VILLAGE_SUCCESS = 'EDIT_VILLAGE_SUCCESS';
export const EDIT_VILLAGE_FAILED = 'EDIT_VILLAGE_FAILED';

export const DELETE_VILLAGE_REQUEST = 'DELETE_VILLAGE_REQUEST';
export const DELETE_VILLAGE_SUCCESS = 'DELETE_VILLAGE_SUCCESS';
export const DELETE_VILLAGE_FAILED = 'DELETE_VILLAGE_FAILED';

export const CHANGE_STATUS_VILLAGE_REQUEST = 'CHANGE_STATUS_VILLAGE_REQUEST';
export const CHANGE_STATUS_VILLAGE_SUCCESS = 'CHANGE_STATUS_VILLAGE_SUCCESS';
export const CHANGE_STATUS_VILLAGE_FAILED = 'CHANGE_STATUS_VILLAGE_FAILED';

export const SET_VILLAGE_DATA_DETAIL = 'SET_VILLAGE_DATA_DETAIL';
export const REFRESH_STATE_VILLAGE = 'REFRESH_STATE_VILLAGE';

export const getVillage = ({ payload }) => async (dispatch) => {
  dispatch({ type: GET_VILLAGE_REQUEST });
  const url = `/villages?search=${payload?.search}&per_page=${payload.dataPerPage}&order_by=created_at&page=${payload.page || 1}`

  try {
    const response = await apiWithToken.get(url, { payload });
    dispatch({ type: GET_VILLAGE_SUCCESS, payload: response.data });
  } catch(error) {
    dispatch({ type: GET_VILLAGE_FAILED });
    errorHandler(error);
  }
};

export const addVillage = ({ payload }) => async (dispatch) => {
  dispatch({ type: ADD_VILLAGE_REQUEST });
  const url = '/villages'
  delete payload.city_id;
  const params = {
    district_id: payload.district_id.id,
    name: payload.name,
  }

  try {
    const response = await apiWithToken.post(url, params );
    dispatch({ 
      type: ADD_VILLAGE_SUCCESS, 
      payload: response.data.data
    })
    swal('Berhasil!', 'Anda berhasil menambah data!', 'success')
      .then(() => window.location.reload());
  } catch(error) {
    dispatch({ type: ADD_VILLAGE_FAILED });
    errorHandler(error);
  }
};

export const editVillage = ({ payload, targetId }) => async (dispatch) => {
  dispatch({ type: EDIT_VILLAGE_REQUEST });
  const url = `/villages/${targetId}`
  const params = {
    district_id: payload.district_id.id,
    name: payload.name,
  }
  
  try {
    const response = await apiWithToken.put(url, params);
    swal('Berhasil!', 'Anda berhasil mengubah data!', 'success')
      .then(() => window.location.reload());
    dispatch({ type: EDIT_VILLAGE_SUCCESS, payload: response.data.data, id: targetId });
  } catch(error) {
    dispatch({ type: EDIT_VILLAGE_FAILED });
    errorHandler(error);
  }
};

export const deleteVillage = ({ payload }) => async (dispatch) => {
  dispatch({ type: DELETE_VILLAGE_REQUEST });
  const url = `/villages/${payload.id}`

  try {
    await apiWithToken.delete(url);
    swal('Berhasil!', 'Anda berhasil menghapus data!', 'success')
      .then(() => window.location.reload());
    dispatch({ type: DELETE_VILLAGE_SUCCESS, id: payload.id });
  } catch(error) {
    dispatch({ type: DELETE_VILLAGE_FAILED });
    errorHandler(error);
  }
};

export const refreshState = () => (dispatch) => {
  dispatch({ type: REFRESH_STATE_VILLAGE });
};

const errorHandler = (error) => {
  if (error?.response?.status === 401){
    localStorage.clear();
    window.location = '/auth/login';
  } else {
    swal(
      '', 
      'Terjadi kesalahan pada server',
      'error'
    )
  }
};