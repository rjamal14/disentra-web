import axios from 'axios';
import env from 'config/env';

export const GET_CITIES_REQUEST = 'GET_CITIES_REQUEST';
export const GET_CITIES_SUCCESS = 'GET_CITIES_SUCCESS';
export const GET_CITIES_FAILED = 'GET_CITIES_FAILED';

const API_URL = `${env.baseUrl}api/v1`;

export const getCities = (payload) => async (dispatch) => {
  dispatch({ type: GET_CITIES_REQUEST });
  const freshToken = localStorage.getItem('token');
  const config = {
    url: API_URL + `/cities?search=${payload.query}`,
    method: 'GET',
    headers: {
      Authorization: `Bearer ${freshToken}`
    }
  }
  try {
    const response = await axios(config);
    dispatch({ type: GET_CITIES_SUCCESS, payload: response.data.data });
  } catch(error) {
    dispatch({ type: GET_CITIES_FAILED });
  }
}