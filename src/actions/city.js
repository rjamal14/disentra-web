import swal from 'sweetalert';
import { apiWithToken } from 'config/apiWithToken';

export const GET_CITY_REQUEST = 'GET_CITY_REQUEST';
export const GET_CITY_SUCCESS = 'GET_CITY_SUCCESS';
export const GET_CITY_FAILED = 'GET_CITY_FAILED';

export const ADD_CITY_REQUEST = 'ADD_CITY_REQUEST';
export const ADD_CITY_SUCCESS = 'ADD_CITY_SUCCESS';
export const ADD_CITY_FAILED = 'ADD_CITY_FAILED';

export const EDIT_CITY_REQUEST = 'EDIT_CITY_REQUEST';
export const EDIT_CITY_SUCCESS = 'EDIT_CITY_SUCCESS';
export const EDIT_CITY_FAILED = 'EDIT_CITY_FAILED';

export const DELETE_CITY_REQUEST = 'DELETE_CITY_REQUEST';
export const DELETE_CITY_SUCCESS = 'DELETE_CITY_SUCCESS';
export const DELETE_CITY_FAILED = 'DELETE_CITY_FAILED';

export const CHANGE_STATUS_CITY_REQUEST = 'CHANGE_STATUS_CITY_REQUEST';
export const CHANGE_STATUS_CITY_SUCCESS = 'CHANGE_STATUS_CITY_SUCCESS';
export const CHANGE_STATUS_CITY_FAILED = 'CHANGE_STATUS_CITY_FAILED';

export const SET_CITY_DATA_DETAIL = 'SET_CITY_DATA_DETAIL';
export const REFRESH_STATE_CITY = 'REFRESH_STATE_CITY';

export const getCity = ({ payload }) => async (dispatch) => {
  dispatch({ type: GET_CITY_REQUEST });
  const url = `/cities?search=${payload?.search}&per_page=${payload?.dataPerPage}&order_by=created_at&page=${payload?.page || 1}`

  try {
    const response = await apiWithToken.get(url, { payload });
    dispatch({ type: GET_CITY_SUCCESS, payload: response.data });
  } catch(error) {
    dispatch({ type: GET_CITY_FAILED });
    errorHandler(error);
  }
};

export const getAllCity = () => async (dispatch) => {
  dispatch({ type: GET_CITY_REQUEST });
  const url = '/cities'

  try {
    const response = await apiWithToken.get(url);
    dispatch({ type: GET_CITY_SUCCESS, payload: response.data });
  } catch(error) {
    dispatch({ type: GET_CITY_FAILED });
    errorHandler(error);
  }
};

export const addCity = ({ payload }) => async (dispatch) => {
  dispatch({ type: ADD_CITY_REQUEST });
  const url = '/cities'
  const params = {
    city_type: payload.city_type,
    name: payload.name,
    province_id: payload.province_id.id
  }

  try {
    const response = await apiWithToken.post(url, params );
    dispatch({ 
      type: ADD_CITY_SUCCESS, 
      payload: response.data.data
    })
    swal('Berhasil!', 'Anda berhasil menambah data!', 'success')
      .then(() => window.location.reload());
  } catch(error) {
    dispatch({ type: ADD_CITY_FAILED });
    errorHandler(error);
  }
};

export const editCity = ({ payload, targetId }) => async (dispatch) => {
  dispatch({ type: EDIT_CITY_REQUEST });
  const url = `/cities/${targetId}`
  const params = {
    city_type: payload.city_type,
    name: payload.name,
    province_id: payload.province_id.id
  }
  
  try {
    const response = await apiWithToken.put(url, params);
    swal('Berhasil!', 'Anda berhasil mengubah data!', 'success')
      .then(() => window.location.reload());
    dispatch({ type: EDIT_CITY_SUCCESS, payload: response.data.data, id: targetId });
  } catch(error) {
    dispatch({ type: EDIT_CITY_FAILED });
    errorHandler(error);
  }
};

export const deleteCity = ({ payload }) => async (dispatch) => {
  dispatch({ type: DELETE_CITY_REQUEST });
  const url = `/cities/${payload.id}`

  try {
    await apiWithToken.delete(url);
    swal('Berhasil!', 'Anda berhasil menghapus data!', 'success')
      .then(() => window.location.reload());
    dispatch({ type: DELETE_CITY_SUCCESS, id: payload.id });
  } catch(error) {
    dispatch({ type: DELETE_CITY_FAILED });
    errorHandler(error);
  }
};

export const refreshState = () => (dispatch) => {
  dispatch({ type: REFRESH_STATE_CITY });
};

const errorHandler = (error) => {
  if (error?.response?.status === 401){
    localStorage.clear();
    window.location = '/auth/login';
  } else {
    swal(
      '', 
      'Terjadi kesalahan pada server',
      'error'
    )
  }
};