import axios from 'axios';
import swal from 'sweetalert';
import env from 'config/env';

export const GET_MARKET_REQUEST = 'GET_MARKET_REQUEST';
export const GET_MARKET_SUCCESS = 'GET_MARKET_SUCCESS';
export const GET_MARKET_FAILED = 'GET_MARKET_FAILED';

export const ADD_MARKET_REQUEST = 'ADD_MARKET_REQUEST';
export const ADD_MARKET_SUCCESS = 'ADD_MARKET_SUCCESS';
export const ADD_MARKET_FAILED = 'ADD_MARKET_FAILED';

export const EDIT_MARKET_REQUEST = 'EDIT_MARKET_REQUEST';
export const EDIT_MARKET_SUCCESS = 'EDIT_MARKET_SUCCESS';
export const EDIT_MARKET_FAILED = 'EDIT_MARKET_FAILED';

export const DELETE_MARKET_REQUEST = 'DELETE_MARKET_REQUEST';
export const DELETE_MARKET_SUCCESS = 'DELETE_MARKET_SUCCESS';
export const DELETE_MARKET_FAILED = 'DELETE_MARKET_FAILED';

export const CHANGE_STATUS_MARKET_REQUEST = 'CHANGE_STATUS_MARKET_REQUEST';
export const CHANGE_STATUS_MARKET_SUCCESS = 'CHANGE_STATUS_MARKET_SUCCESS';
export const CHANGE_STATUS_MARKET_FAILED = 'CHANGE_STATUS_MARKET_FAILED';

export const SET_MARKET_DATA_DETAIL = 'SET_MARKET_DATA_DETAIL';
export const REFRESH_STATE_MARKET = 'REFRESH_STATE_MARKET';

const API_URL = `${env.baseUrl}api/v1`;

const queryBuilder = (array) => {
  /* eslint-disable no-unused-vars */
  const filtered = Object.entries(array).filter(([field, value]) => value)
  const convert = filtered.map(field => `q[${field[0]}]=${('' + field[1]).replace('$','')}`)
  return convert.join('&')
}

export const getMarket = ({ payload }) => async (dispatch) => {
  dispatch({ type: GET_MARKET_REQUEST });
  const freshToken = localStorage.getItem('token');
  const config = {
    url: API_URL + `/umkm_markets?per_page=${payload.dataPerPage}&page=${payload.page || 1}&${payload.params ? queryBuilder(payload.params) : ''}`,
    method: 'GET',
    headers: {
      Authorization: `Bearer ${freshToken}`
    }
  };
  try {
    const response = await axios(config);
    dispatch({ type: GET_MARKET_SUCCESS, payload: response.data });
  } catch(error) {
    dispatch({ type: GET_MARKET_FAILED });
    errorHandler(error);
  }
};

export const addMarket = ({ payload }) => async (dispatch) => {
  dispatch({ type: ADD_MARKET_REQUEST });
  const filterImage = payload.image.filter(file => file.length);
  const freshToken = localStorage.getItem('token');
  let formData = new FormData();
  for (let [key, value] of Object.entries(payload)) {
    if (key === 'image') {
      filterImage.map((img, index) => {
        Array.from(img).map(file => {
          formData.append(`umkm_market[umkm_market_images_attributes][${index}][${key}]`, file);
          formData.append(`umkm_market[umkm_market_images_attributes][${index}][is_thumbnail]`, false);
        });
      })
    } else {
      if (key === 'thumbnail') {
        Array.from(payload[key]).map(file => {
          formData.append(`umkm_market[umkm_market_images_attributes][${filterImage.length}][image]`, file);
          formData.append(`umkm_market[umkm_market_images_attributes][${filterImage.length}][is_thumbnail]`, true);
        })
      } else {
        formData.append(`umkm_market[${key}]`, value);
      }
    }
  }
  const config = {
    url: API_URL + '/umkm_markets',
    method: 'POST',
    headers: {
      Authorization: `Bearer ${freshToken}`
    },
    data: formData
  };
  try {
    const response = await axios(config);
    swal('Berhasil!', 'Anda berhasil menambah data!', 'success');
    dispatch({ 
      type: ADD_MARKET_SUCCESS, 
      payload: response.data,
    });
  } catch(error) {
    dispatch({ type: ADD_MARKET_FAILED });
    errorHandler(error);
  }
};

export const editMarket = ({ payload, targetId }) => async (dispatch) => {
  dispatch({ type: EDIT_MARKET_REQUEST });
  const filterImage = payload.image.filter(file => file.length);
  const freshToken = localStorage.getItem('token');
  let formData = new FormData();
  for (let [key, value] of Object.entries(payload)) {
    if (key === 'image') {
      filterImage.map((img, index) => {
        Array.from(img).map(file => {
          formData.append(`umkm_market[umkm_market_images_attributes][${index}][${key}]`, file);
          formData.append(`umkm_market[umkm_market_images_attributes][${index}][is_thumbnail]`, false);
        });
      })
    } else {
      if (key === 'thumbnail') {
        Array.from(payload[key]).map(file => {
          formData.append(`umkm_market[umkm_market_images_attributes][${filterImage.length}][image]`, file);
          formData.append(`umkm_market[umkm_market_images_attributes][${filterImage.length}][is_thumbnail]`, true);
        })
      } else {
        formData.append(`umkm_market[${key}]`, value);
      }
    }
  }
  const config = {
    url: API_URL + '/umkm_markets/' + targetId,
    method: 'PATCH',
    headers: {
      Authorization: `Bearer ${freshToken}`
    },
    data: formData
  };
  try {
    const response = await axios(config);
    swal('Berhasil!', 'Anda berhasil mengubah data!', 'success');
    dispatch({ type: EDIT_MARKET_SUCCESS, payload: response.data.data, id: targetId });
  } catch(error) {
    dispatch({ type: EDIT_MARKET_FAILED });
    errorHandler(error);
  }
};

export const deleteMarket = ({ payload }) => async (dispatch) => {
  dispatch({ type: DELETE_MARKET_REQUEST });
  const freshToken = localStorage.getItem('token');
  const config = { 
    url: API_URL + '/umkm_markets/' + payload.id,
    method: 'DELETE',
    headers: {
      Authorization: `Bearer ${freshToken}`
    }
  }
  try {
    await axios(config);
    swal('Berhasil!', 'Anda berhasil menghapus data!', 'success');
    dispatch({ type: DELETE_MARKET_SUCCESS, id: payload.id });
  } catch(error) {
    dispatch({ type: DELETE_MARKET_FAILED });
    errorHandler(error);
  }
};

export const changeStatusMarket = ({ payload }) => async (dispatch) => {
  dispatch({ type: CHANGE_STATUS_MARKET_REQUEST });
  const freshToken = localStorage.getItem('token');
  let formData = new FormData();
  formData.append('umkm_market[status]', payload.status);
  const config = {
    url: API_URL + '/umkm_markets/' + payload.id,
    method: 'PUT',
    headers: {
      Authorization: `Bearer ${freshToken}`
    },
    data: formData
  }
  try {
    await axios(config);
    if (payload.status === 'show') {
      swal('Berhasil!', 'Konten berhasil diperlihatkan!', 'success'); 
    } else {
      swal('Berhasil!', 'Konten berhasil disembunyikan!', 'success');
    }
    dispatch({ type: CHANGE_STATUS_MARKET_SUCCESS, id: payload.id, status: payload.status });
  } catch(error) {
    dispatch({ type: CHANGE_STATUS_MARKET_FAILED });
    errorHandler(error);
  }
};

export const setMarketDataDetail = ({ payload }) => (dispatch) => {
  dispatch({ type: SET_MARKET_DATA_DETAIL, payload });
};

export const refreshStateMarket = () => (dispatch) => {
  dispatch({ type: REFRESH_STATE_MARKET });
};

const errorHandler = (error) => {
  if (error?.response?.status === 401){
    localStorage.clear();
    window.location = '/auth/login';
  } else {
    swal(
      '', 
      'Terjadi kesalahan pada server',
      'error'
    )
  }
};