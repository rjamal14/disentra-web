import swal from 'sweetalert';
import { apiWithToken } from 'config/apiWithToken';

export const GET_PROVINCE_REQUEST = 'GET_PROVINCE_REQUEST';
export const GET_PROVINCE_SUCCESS = 'GET_PROVINCE_SUCCESS';
export const GET_PROVINCE_FAILED = 'GET_PROVINCE_FAILED';

export const ADD_PROVINCE_REQUEST = 'ADD_PROVINCE_REQUEST';
export const ADD_PROVINCE_SUCCESS = 'ADD_PROVINCE_SUCCESS';
export const ADD_PROVINCE_FAILED = 'ADD_PROVINCE_FAILED';

export const EDIT_PROVINCE_REQUEST = 'EDIT_PROVINCE_REQUEST';
export const EDIT_PROVINCE_SUCCESS = 'EDIT_PROVINCE_SUCCESS';
export const EDIT_PROVINCE_FAILED = 'EDIT_PROVINCE_FAILED';

export const DELETE_PROVINCE_REQUEST = 'DELETE_PROVINCE_REQUEST';
export const DELETE_PROVINCE_SUCCESS = 'DELETE_PROVINCE_SUCCESS';
export const DELETE_PROVINCE_FAILED = 'DELETE_PROVINCE_FAILED';

export const CHANGE_STATUS_PROVINCE_REQUEST = 'CHANGE_STATUS_PROVINCE_REQUEST';
export const CHANGE_STATUS_PROVINCE_SUCCESS = 'CHANGE_STATUS_PROVINCE_SUCCESS';
export const CHANGE_STATUS_PROVINCE_FAILED = 'CHANGE_STATUS_PROVINCE_FAILED';

export const SET_PROVINCE_DATA_DETAIL = 'SET_PROVINCE_DATA_DETAIL';
export const REFRESH_STATE_PROVINCE = 'REFRESH_STATE_PROVINCE';

export const getProvince = ({ payload }) => async (dispatch) => {
  dispatch({ type: GET_PROVINCE_REQUEST });
  const url = `/provinces?search=${payload?.search}&per_page=${payload?.dataPerPage}&order_by=created_at&page=${payload?.page || 1}`

  try {
    const response = await apiWithToken.get(url, { payload });
    dispatch({ type: GET_PROVINCE_SUCCESS, payload: response.data });
  } catch(error) {
    dispatch({ type: GET_PROVINCE_FAILED });
    errorHandler(error);
  }
};

export const getAllProvince = () => async (dispatch) => {
  dispatch({ type: GET_PROVINCE_REQUEST });
  const url = '/provinces'

  try {
    const response = await apiWithToken.get(url);
    dispatch({ type: GET_PROVINCE_SUCCESS, payload: response.data });
  } catch(error) {
    dispatch({ type: GET_PROVINCE_FAILED });
    errorHandler(error);
  }
};

export const addProvince = ({ payload }) => async (dispatch) => {
  dispatch({ type: ADD_PROVINCE_REQUEST });
  const url = '/provinces'

  try {
    const response = await apiWithToken.post(url, payload );
    dispatch({ 
      type: ADD_PROVINCE_SUCCESS, 
      payload: response.data.data
    })
    swal('Berhasil!', 'Anda berhasil menambah data!', 'success')
      .then(() => window.location.reload());
  } catch(error) {
    dispatch({ type: ADD_PROVINCE_FAILED });
    errorHandler(error);
  }
};

export const editProvince = ({ payload, targetId }) => async (dispatch) => {
  dispatch({ type: EDIT_PROVINCE_REQUEST });
  const url = `/provinces/${targetId}`
  
  try {
    const response = await apiWithToken.put(url, payload);
    swal('Berhasil!', 'Anda berhasil mengubah data!', 'success')
      .then(() => window.location.reload());
    dispatch({ type: EDIT_PROVINCE_SUCCESS, payload: response.data.data, id: targetId });
  } catch(error) {
    dispatch({ type: EDIT_PROVINCE_FAILED });
    errorHandler(error);
  }
};

export const deleteProvince = ({ payload }) => async (dispatch) => {
  dispatch({ type: DELETE_PROVINCE_REQUEST });
  const url = `/provinces/${payload.id}`

  try {
    await apiWithToken.delete(url);
    swal('Berhasil!', 'Anda berhasil menghapus data!', 'success')
      .then(() => window.location.reload());
    dispatch({ type: DELETE_PROVINCE_SUCCESS, id: payload.id });
  } catch(error) {
    dispatch({ type: DELETE_PROVINCE_FAILED });
    errorHandler(error);
  }
};

export const refreshState = () => (dispatch) => {
  dispatch({ type: REFRESH_STATE_PROVINCE });
};

const errorHandler = (error) => {
  if (error?.response?.status === 401){
    localStorage.clear();
    window.location = '/auth/login';
  } else {
    swal(
      '', 
      'Terjadi kesalahan pada server',
      'error'
    )
  }
};