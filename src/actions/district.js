import swal from 'sweetalert';
import { apiWithToken } from 'config/apiWithToken';

export const GET_DISTRICT_REQUEST = 'GET_DISTRICT_REQUEST';
export const GET_DISTRICT_SUCCESS = 'GET_DISTRICT_SUCCESS';
export const GET_DISTRICT_FAILED = 'GET_DISTRICT_FAILED';

export const ADD_DISTRICT_REQUEST = 'ADD_DISTRICT_REQUEST';
export const ADD_DISTRICT_SUCCESS = 'ADD_DISTRICT_SUCCESS';
export const ADD_DISTRICT_FAILED = 'ADD_DISTRICT_FAILED';

export const EDIT_DISTRICT_REQUEST = 'EDIT_DISTRICT_REQUEST';
export const EDIT_DISTRICT_SUCCESS = 'EDIT_DISTRICT_SUCCESS';
export const EDIT_DISTRICT_FAILED = 'EDIT_DISTRICT_FAILED';

export const DELETE_DISTRICT_REQUEST = 'DELETE_DISTRICT_REQUEST';
export const DELETE_DISTRICT_SUCCESS = 'DELETE_DISTRICT_SUCCESS';
export const DELETE_DISTRICT_FAILED = 'DELETE_DISTRICT_FAILED';

export const CHANGE_STATUS_DISTRICT_REQUEST = 'CHANGE_STATUS_DISTRICT_REQUEST';
export const CHANGE_STATUS_DISTRICT_SUCCESS = 'CHANGE_STATUS_DISTRICT_SUCCESS';
export const CHANGE_STATUS_DISTRICT_FAILED = 'CHANGE_STATUS_DISTRICT_FAILED';

export const SET_DISTRICT_DATA_DETAIL = 'SET_DISTRICT_DATA_DETAIL';
export const REFRESH_STATE_DISTRICT = 'REFRESH_STATE_DISTRICT';

export const getDistrict = ({ payload }) => async (dispatch) => {
  dispatch({ type: GET_DISTRICT_REQUEST });
  const url = `/districts?search=${payload?.search}&per_page=${payload.dataPerPage}&order_by=created_at&page=${payload.page || 1}`

  try {
    const response = await apiWithToken.get(url, { payload });
    dispatch({ type: GET_DISTRICT_SUCCESS, payload: response.data });
  } catch(error) {
    dispatch({ type: GET_DISTRICT_FAILED });
    errorHandler(error);
  }
};

export const getAllDistrict = () => async (dispatch) => {
  dispatch({ type: GET_DISTRICT_REQUEST });
  const url = '/districts?'

  try {
    const response = await apiWithToken.get(url);
    dispatch({ type: GET_DISTRICT_SUCCESS, payload: response.data });
  } catch(error) {
    dispatch({ type: GET_DISTRICT_FAILED });
    errorHandler(error);
  }
};

export const addDistrict = ({ payload }) => async (dispatch) => {
  dispatch({ type: ADD_DISTRICT_REQUEST });
  const url = '/districts'
  const params = {
    city_id: payload.city_id.id,
    name: payload.name,
  }

  try {
    const response = await apiWithToken.post(url, params );
    dispatch({ 
      type: ADD_DISTRICT_SUCCESS, 
      payload: response.data.data
    })
    swal('Berhasil!', 'Anda berhasil menambah data!', 'success')
      .then(() => window.location.reload());
  } catch(error) {
    dispatch({ type: ADD_DISTRICT_FAILED });
    errorHandler(error);
  }
};

export const editDistrict = ({ payload, targetId }) => async (dispatch) => {
  dispatch({ type: EDIT_DISTRICT_REQUEST });
  const url = `/districts/${targetId}`
  const params = {
    city_id: payload.city_id.id,
    name: payload.name,
  }
  
  try {
    const response = await apiWithToken.put(url, params);
    swal('Berhasil!', 'Anda berhasil mengubah data!', 'success')
      .then(() => window.location.reload());
    dispatch({ type: EDIT_DISTRICT_SUCCESS, payload: response.data.data, id: targetId });
  } catch(error) {
    dispatch({ type: EDIT_DISTRICT_FAILED });
    errorHandler(error);
  }
};

export const deleteDistrict = ({ payload }) => async (dispatch) => {
  dispatch({ type: DELETE_DISTRICT_REQUEST });
  const url = `/districts/${payload.id}`

  try {
    await apiWithToken.delete(url);
    swal('Berhasil!', 'Anda berhasil menghapus data!', 'success')
      .then(() => window.location.reload());
    dispatch({ type: DELETE_DISTRICT_SUCCESS, id: payload.id });
  } catch(error) {
    dispatch({ type: DELETE_DISTRICT_FAILED });
    errorHandler(error);
  }
};

export const refreshState = () => (dispatch) => {
  dispatch({ type: REFRESH_STATE_DISTRICT });
};

const errorHandler = (error) => {
  if (error?.response?.status === 401){
    localStorage.clear();
    window.location = '/auth/login';
  } else {
    swal(
      '', 
      'Terjadi kesalahan pada server',
      'error'
    )
  }
};