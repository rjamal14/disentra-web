/* eslint-disable no-case-declarations */

import * as actionTypes from 'actions';

const initialState = {
  data: [],
  dataDetail: null,
  pagination: null,
  error: false,
  loading: false,
  success: false,
};

const jobPositionReducer = (state = initialState, action) => {
  switch (action.type) {

    case actionTypes.GET_JOB_POSITION_REQUEST:
    case actionTypes.ADD_JOB_POSITION_REQUEST:
    case actionTypes.EDIT_JOB_POSITION_REQUEST:
    case actionTypes.DELETE_JOB_POSITION_REQUEST:
    case actionTypes.CHANGE_STATUS_JOB_POSITION_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case actionTypes.GET_JOB_POSITION_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
        pagination: action.payload.pagination,
        error: false,
        success: true,
      }

    case actionTypes.GET_JOB_POSITION_FAILED:
    case actionTypes.ADD_JOB_POSITION_FAILED:
    case actionTypes.EDIT_JOB_POSITION_FAILED:
    case actionTypes.DELETE_JOB_POSITION_FAILED:
    case actionTypes.CHANGE_STATUS_JOB_POSITION_FAILED:
      return {
        ...state,
        loading: false,
        error: true,
        success: false,
      }

    case actionTypes.ADD_JOB_POSITION_SUCCESS:
      const provinceCopy = [...state.data];
      provinceCopy.push(action.payload);
      return {
        ...state,
        data: provinceCopy,
        error: false,
        loading: false,
        success: true,
      }
  
    case actionTypes.EDIT_JOB_POSITION_SUCCESS:
      const provinceCopy2 = [...state.data];
      const dataIndex = provinceCopy2.map(item => item.id).indexOf(action.id);
      provinceCopy2[dataIndex] = action.payload;
      return {
        ...state,
        data: provinceCopy2,
        error: false,
        loading: false,
        success: true,
      }
  
    case actionTypes.DELETE_JOB_POSITION_SUCCESS:
      return {
        ...state,
        data: state.data.filter(item => item.id !== action.id),
        error: false,
        loading: false,
        success: true,
      }
  
    case actionTypes.SET_JOB_POSITION_DATA_DETAIL:
      return {
        ...state,
        dataDetail: action.payload,
      }
      
    case actionTypes.REFRESH_STATE_JOB_POSITION:
      return {
        ...state,
        loading: false,
        success: false,
        error: false,
      }

    default:
      return state;
  }
};

export default jobPositionReducer;