/* eslint-disable no-case-declarations */

import * as actionTypes from 'actions';

const initialState = {
  data: [],
  dataDetail: null,
  pagination: null,
  error: false,
  loading: false,
  success: false,
};

const roomReducer = (state = initialState, action) => {
  switch (action.type) {

    case actionTypes.GET_ROOM_REQUEST: 
    case actionTypes.ADD_ROOM_REQUEST:
    case actionTypes.EDIT_ROOM_REQUEST:
    case actionTypes.DELETE_ROOM_REQUEST:
    case actionTypes.CHANGE_STATUS_ROOM_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case actionTypes.GET_ROOM_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        error: false,
        loading: false,
        pagination: action.payload.pagination,
        success: true,
      }

    case actionTypes.GET_ROOM_FAILED:
    case actionTypes.ADD_ROOM_FAILED:
    case actionTypes.EDIT_ROOM_FAILED:
    case actionTypes.DELETE_ROOM_FAILED:
    case actionTypes.CHANGE_STATUS_ROOM_FAILED:
      return {
        ...state,
        error: true,
        loading: false,
        success: false,
      }

    case actionTypes.ADD_ROOM_SUCCESS:
      const RoomCopy = [...state.data];
      RoomCopy.unshift(action.payload);
      return {
        ...state,
        data: RoomCopy,
        error: false,
        loading: false,
        success: true,
      }

    case actionTypes.EDIT_ROOM_SUCCESS:
      const RoomCopy2 = [...state.data];
      const dataIndex = RoomCopy2.map(item => item.id).indexOf(action.id);
      RoomCopy2[dataIndex] = action.payload;
      return {
        ...state,
        data: RoomCopy2,
        error: false,
        loading: false,
        success: true,
      }

    case actionTypes.DELETE_ROOM_SUCCESS:
      return {
        ...state,
        data: state.data.filter(item => item.id !== action.id),
        error: false,
        loading: false,
        success: true,
      }

    case actionTypes.CHANGE_STATUS_ROOM_SUCCESS:
      const RoomCopy1 = [...state.data];
      const suspendedUserIndex = RoomCopy1.map(user => user.id).indexOf(action.id);
      RoomCopy1[suspendedUserIndex].status = action.status;
      return {
        ...state,
        data: RoomCopy1,
        error: false,
        loading: false,
        success: true,
      }

    case actionTypes.SET_ROOM_DATA_DETAIL:
      return {
        ...state,
        dataDetail: action.payload,
      }

    case actionTypes.REFRESH_STATE_ROOM:
      return {
        ...state,
        loading: false,
        success: false,
        error: false,
      }

    default:
      return state;
  }
};

export default roomReducer; 