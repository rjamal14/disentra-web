import { combineReducers } from 'redux';

import sessionReducer from './sessionReducer';
import userReducer from './user';
import userUmkmReducer from './userUmkm';
import roleReducer from './role';
import jobPositionReducer from './jobPosition';
import branchOfficeReducer from './branchOffice';
import provinceReducer from './province';
import cityReducer from './city';
import districtReducer from './district';
import villageReducer from './village';
import businessFieldReducer from './businessField';
import businessTalkReducer from './businessTalk';
import learningCenterReducer from './learningCenter';
import marketReducer from './market';
import citiesReducer from './cities';
import bannerReducer from './banner';
import roomReducer from './room';

const rootReducer = combineReducers({
  session: sessionReducer,
  user: userReducer,
  userUmkm: userUmkmReducer,
  role: roleReducer,
  jobPosition: jobPositionReducer,
  branchOffice: branchOfficeReducer,
  province: provinceReducer,
  city: cityReducer,
  district: districtReducer,
  village: villageReducer,
  businessField: businessFieldReducer,
  businessTalk: businessTalkReducer,
  learningCenter: learningCenterReducer,
  market: marketReducer,
  cities: citiesReducer,
  banner: bannerReducer,
  room: roomReducer,
});

export default rootReducer;
