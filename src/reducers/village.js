/* eslint-disable no-case-declarations */

import * as actionTypes from 'actions';

const initialState = {
  data: [],
  dataDetail: null,
  pagination: null,
  error: false,
  loading: false,
  success: false,
};

const villageReducer = (state = initialState, action) => {
  switch (action.type) {

    case actionTypes.GET_VILLAGE_REQUEST: 
    case actionTypes.ADD_VILLAGE_REQUEST:
    case actionTypes.EDIT_VILLAGE_REQUEST:
    case actionTypes.DELETE_VILLAGE_REQUEST:
    case actionTypes.CHANGE_STATUS_VILLAGE_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case actionTypes.GET_VILLAGE_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        error: false,
        loading: false,
        pagination: action.payload.pagination,
        success: true,
      }
    
    case actionTypes.GET_VILLAGE_FAILED:
    case actionTypes.ADD_VILLAGE_FAILED:
    case actionTypes.EDIT_VILLAGE_FAILED:
    case actionTypes.DELETE_VILLAGE_FAILED:
    case actionTypes.CHANGE_STATUS_VILLAGE_FAILED:
      return {
        ...state,
        error: true,
        loading: false,
        success: false,
      }

    case actionTypes.ADD_VILLAGE_SUCCESS:
      const provinceCopy = [...state.data];
      provinceCopy.push(action.payload);
      return {
        ...state,
        data: provinceCopy,
        error: false,
        loading: false,
        success: true,
      }

    case actionTypes.EDIT_VILLAGE_SUCCESS:
      const provinceCopy2 = [...state.data];
      const dataIndex = provinceCopy2.map(item => item.id).indexOf(action.id);
      provinceCopy2[dataIndex] = action.payload;
      return {
        ...state,
        data: provinceCopy2,
        error: false,
        loading: false,
        success: true,
      }

    case actionTypes.DELETE_VILLAGE_SUCCESS:
      return {
        ...state,
        data: state.data.filter(item => item.id !== action.id),
        error: false,
        loading: false,
        success: true,
      }

    case actionTypes.SET_VILLAGE_DATA_DETAIL:
      return {
        ...state,
        dataDetail: action.payload,
      }
    
    case actionTypes.REFRESH_STATE_VILLAGE:
      return {
        ...state,
        loading: false,
        success: false,
        error: false,
      }

    default:
      return state;
  }
};

export default villageReducer;