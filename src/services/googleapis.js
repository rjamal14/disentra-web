export const googlePoint = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=';
export const googlePlace = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=';
export const googlePlaceDetail = 'https://maps.googleapis.com/maps/api/place/details/json?place_id=';