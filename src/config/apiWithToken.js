import axios from 'axios'
import env from './env'
import { handleUnatuhorizedAndExpired, SetAuthTokenRequest } from './interceptor'

export const apiWithToken = axios.create({
  baseURL: `${env.baseUrl}api/v1`,
})

apiWithToken.interceptors.request.use(SetAuthTokenRequest, (err) => err)
apiWithToken.interceptors.response.use((res) => res, handleUnatuhorizedAndExpired)
