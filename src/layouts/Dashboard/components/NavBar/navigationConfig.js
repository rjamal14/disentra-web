export default [
  {
    pages: [
      {
        title: 'Dashboard',
        href: '/dashboard',
        icon: '/icon/home_icon.png'
      },
      {
        title: 'Manajemen User',
        href: '/management',
        icon: '/icon/user_icon.png',
        children: [
          {
            title: 'Admin',
            href: '/management-user/user'
          },
          {
            title: 'UMKM',
            href: '/management-user/umkm'
          },
          {
            title: 'Role',
            href: '/management-user/role'
          }
        ]
      },
      {
        title: 'Master Data',
        href: '/master-data',
        icon: '/icon/master_icon.png',
        children: [
          {
            title: 'Provinsi',
            href: '/master-data/province'
          },
          {
            title: 'Kota/Kabupaten',
            href: '/master-data/city'
          },
          {
            title: 'Kecamatan',
            href: '/master-data/district'
          },
          {
            title: 'Kelurahan',
            href: '/master-data/village'
          },
          {
            title: 'Bidang Usaha',
            href: '/master-data/business-field'
          },
          {
            title: 'Jabatan',
            href: '/master-data/job-position'
          }
        ]
      },
      {
        title: 'Manajemen Konten',
        href: '/management-content/content',
        icon: '/icon/management_content.png',
        children: [
          {
            title: 'Bincang Bisnis',
            href: '/management-content/business-talk'
          },
          {
            title: 'Sentra Belajar',
            href: '/management-content/learning-center'
          },
          {
            title: 'Pasar UMKM',
            href: '/management-content/umkm-market'
          },
          {
            title: 'Banner',
            href: '/management-content/banner'
          },
          {
            title: 'Room',
            href: '/management-content/room'
          },
        ]
      }
    ]
  }
];
