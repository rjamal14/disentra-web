/* eslint-disable react/prop-types */
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { makeStyles, withStyles } from '@material-ui/styles';
import { Controller } from 'react-hook-form';
import {  
  Typography,
  TextField,
  CircularProgress,
  Box,
} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import palette from 'theme/palette';

const ErrorText = withStyles(() => ({
  root: {
    color: palette.error.main,
    marginBottom: 10,
    marginLeft: 10,
    marginTop: -10,
  }
}))(Typography);

const useStyles = makeStyles((theme) => ({
  box: {
    padding: theme.spacing(1),
    [theme.breakpoints.down('md')]: {
      left:50
    },
    borderRadius: 50,
    '&.MuiInputBase-root': {
      fontSize: 14
    },
  },
}));

const RoundedAutocomplete = ({
  name,
  label,
  defaultValue,
  errorMessage,
  errors,
  control,
  validation,
  searchApi,
  setValue,
  options,
}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [open, setOpen] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [value, setSelectedValue] = React.useState({ id: '', name: '' });
  let timeout = 0;

  useEffect(() => {
    setSelectedValue(defaultValue)
    setValue(name, defaultValue)
  }, [])

  useEffect(() => {
    setLoading(false)
  }, [options])

  const onChange = (evt) => {
    const search = evt.target.value
    setLoading(true)
    if (timeout) clearTimeout(timeout)
    timeout = setTimeout(async () => {
      if (search) dispatch(searchApi({ payload: { search } }))
    }, 2000)
  }

  return(
    <>
      <Typography
        component="p"
        variant="body1"
      >
        {label}
      </Typography>
      <Box
        border={1}
        borderColor="#C4C4C4"
        borderRadius={20}
        className={classes.box}
        marginBottom={2}
      >
        <Controller
          control={control}
          defaultValue={value}
          name={name}
          render={() => (
            <Autocomplete
              getOptionLabel={(option) => option.name}
              loading={loading}
              onChange={(e, value, reason) => {
                if (reason === 'select-option') {
                  setValue(name, value, { shouldValidate: true })
                  setSelectedValue(value)
                }}
              }
              onClose={() => {setOpen(false);}}
              onOpen={() => {setOpen(true);}}
              open={open}
              options={options.map(val => ({ id: val.id, name: val.name }))}
              renderInput={(params) => (
                <TextField
                  className={classes.box}
                  {...params}
                  InputProps={{
                    ...params.InputProps,
                    endAdornment: (
                      <React.Fragment>
                        {loading ? <CircularProgress 
                          color="inherit"
                          size={20}
                        /> : null}
                        {params.InputProps.endAdornment}
                      </React.Fragment>
                    ),
                    disableUnderline: true,
                  }}
                  onChange={onChange}
                />
              )}
              value={value}
            />
          )}
          rules={validation}
        />
      </Box>
      {
        errors?.branch_office_id &&
          <ErrorText
            component="p"
            variant="caption"
          >
            {errorMessage}
          </ErrorText>
      }
    </>
  )
}

RoundedAutocomplete.propTypes = {
  control: PropTypes.object,
  defaultValue: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  errorMessage: PropTypes.string,
  errors: PropTypes.object,
  label : PropTypes.string,
  name: PropTypes.string,
  options: PropTypes.array,
  validation: PropTypes.object,
};

export default RoundedAutocomplete;