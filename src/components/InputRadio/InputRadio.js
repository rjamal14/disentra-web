import React from 'react';
import { Controller } from 'react-hook-form';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import {  
  Typography,
  FormControlLabel,
  Radio,
  RadioGroup
} from '@material-ui/core';
import palette from 'theme/palette';

const ErrorText = withStyles(() => ({
  root: {
    color: palette.error.main,
    marginBottom: 10,
    marginLeft: 10,
    marginTop: -10,
  }
}))(Typography);

const InputRadio = ({
  name,
  label,
  defaultValue,
  errorMessage,
  errors,
  options,
  control,
  validation,
}) => {
  return(
    <>
      <Typography
        component="p"
        variant="body1"
      >
        {label}
      </Typography>
      <Controller
        control={control}
        defaultValue={defaultValue || options[0].value}
        name={name}
        render={({ onChange, value }) => (
          <RadioGroup
            name={name}
            onChange={onChange}
            row
            value={value}
          >
            {options.map((option) =>
              <FormControlLabel
                control={<Radio color="default" />}
                label={option.label}
                value={option.value}
              />
            )}
          </RadioGroup>
        )}
        rules={validation}
      />
      {
        errors &&
          <ErrorText
            component="p"
            variant="caption"
          >
            {errorMessage}
          </ErrorText>
      }
    </>
  )
};

InputRadio.propTypes = {
  control: PropTypes.object,
  defaultValue: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  errorMessage: PropTypes.string,
  errors: PropTypes.object,
  label : PropTypes.string,
  name: PropTypes.string,
  options: PropTypes.array,
  validation: PropTypes.object,
};

export default InputRadio;