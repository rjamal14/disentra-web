import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles, withStyles } from '@material-ui/styles';
import {  
  Typography,
  Box,
  TextareaAutosize
} from '@material-ui/core';
import palette from 'theme/palette';

const ErrorText = withStyles(() => ({
  root: {
    color: palette.error.main,
    marginBottom: 10,
    marginLeft: 10,
    marginTop: -10,
  }
}))(Typography);

const useStyles = makeStyles((theme) => ({
  box: {
    marginTop: theme.spacing(0.6),
    padding: theme.spacing(0.5,2),
    [theme.breakpoints.down('md')]: {
      left:50
    }
  },
  textarea: {
    border: 0,
    resize: 'none',
    width: '100%',
    padding: '5px 0',
    '&:focus': {
      outline: 0
    }
  }
}));

const RoundedTextarea = ({
  label,
  defaultValue,
  errorMessage,
  errors,
  name,
  register,
  row
}) => {
  const classes = useStyles();
  return(
    <>
      <Typography
        component="p"
        variant="body1"
      >
        {label}
      </Typography>
      <Box
        border={1}
        borderColor="#C4C4C4"
        borderRadius={20}
        className={classes.box}
        marginBottom={2}
      >
        <TextareaAutosize
          aria-label={name}
          className={classes.textarea}
          defaultValue={defaultValue}
          name={name}
          ref ={register}
          rowsMin={row || 3}
        />
      </Box>
      {
        errors &&
          <ErrorText
            component="p"
            variant="caption"
          >
            {errorMessage}
          </ErrorText>
      }
    </>
  )
};

RoundedTextarea.propTypes = {
  defaultValue: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  errorMessage: PropTypes.string,
  errors: PropTypes.object,
  label : PropTypes.string,
  name: PropTypes.string,
  register: PropTypes.func,
  row: PropTypes.number,
};

RoundedTextarea.defaultProps = {
  defaultValue: '',
  type: 'text'
}

export default RoundedTextarea;